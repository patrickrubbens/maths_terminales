from turtle import *

hideturtle()

def koch(l,n):
    if n==0:
        forward(l)
    else: # construction sur 1 côté
        koch(l/3,n-1) 
        left(60)
        koch(l/3,n-1)
        right(120)
        koch(l/3,n-1)
        left(60)
        koch(l/3,n-1)
        
def flocon(t,e):
    koch(t,e)
    right(120)
    koch(t,e)
    right(120)
    koch(t,e)

def dessin(taille):
    for n in range(4):
        up()
        goto(-350+(taille+20)*n,0)
        down()
        flocon(taille,n)
        setheading(0)
           
speed(0)
dessin(150)
done()