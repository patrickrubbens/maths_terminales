import numpy as np
import matplotlib.pyplot as plt

x_min , x_max = 0 , 10
y_min, y_max = 0 , 100

# création de la fenêtre graphique 1
plt.figure("Courbe et Tangentes", figsize=(12,10), dpi=100) 
###########################################################################################################

# division de la fenêtre graphique en 1 ligne, 2 colonnes, graphique en position 1
plt.subplot(1,2,1)
# titre
plt.title("Parabole et 10 tangentes",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xlabel("x")
plt.ylabel("y")
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,10))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

# la parabole
x = np.arange(x_min, x_max, .01)
y = x**2

plt.plot(x,y,color='b', linestyle='-', linewidth = 2, label='y = x²')

# les tangentes
for a in range(10):
    m = a*2
    abs = np.array([0,a,10])
    t = np.array([m*(0-a)+a**2,a**2,m*(10-a)+a**2])
    plt.plot(abs,t,color='r', marker = '.',markersize = 12,linestyle='-.', linewidth = 1)

###########################################################################################################

# division de la fenêtre graphique en 2 lignes, 2 colonnes, graphique en position 4
plt.subplot(2,2,4)

# titre
plt.title("La parabole",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,10))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#la parabole
plt.plot(x,y,color='b', linestyle='-', linewidth = 2, label='y = x²')
plt.text(5,50,'y = x²',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

# division de la fenêtre graphique en 2 lignes, 2 colonnes, graphique en position 2
plt.subplot(2,2,2)

# titre
plt.title("Les 10 tangentes",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')

# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,10))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#les tangentes
for a in range(10):
    m = a*2
    abs = np.array([0,a,10])
    t = np.array([m*(0-a)+a**2,a**2,m*(10-a)+a**2])
    plt.plot(abs,t,color='r', marker = '.',markersize = 12,linestyle='-.', linewidth = 1)

###########################################################################################################

plt.show()