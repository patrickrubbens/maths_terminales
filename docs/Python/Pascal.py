

#n = int(input("pour n = "))
#k = int(input("pour k = "))

def coeff(n=6,k=3):
    lp = [1] # on initialise la première ligne
    for j in range(n):
        nl = lp + [1] # nouvelle ligne (nl) on recopie la ligne précédente (lp) et on rajoute 1 à la fin
        for i in range(len(lp) - 1):
            nl[i + 1] = lp[i] + lp[i + 1] # c'est ici que la magie opère: les deux coeff de la ligne précédente sont additionnés
        lp = nl #nouvelle ligne va maintenat jouer le rôle de ligne précédente pour recommencer
    print(f"le nombre de combinaisons pour n={n} et X={k} est égal à {nl[k]}")

def coeff_triangle(n=6,k=3):
    lp = [1]
    for j in range(n):
        print(lp)
        nl = lp + [1]
        for i in range(len(lp) - 1):
            nl[i + 1] = lp[i] + lp[i + 1]
        lp = nl
    
def trianglePascal(n=10):
    lp = [1]
    for j in range(n):
        print(f"{'-'*(len(lp)*5)}")
        nl = lp + [1]
        for i in range(len(lp) - 1):
            nl[i + 1] = lp[i] + lp[i + 1]
            print(f"{lp[i]:3} |",end="")
            
def trianglePascalv2(n=10):
    C=[]
    for i in range(n):
        ligne = []
        ligne.append(1) # pour i=0 ligne = [1]
        for j in range(1,i): # pour i=0 on ne rentre pas dans la boucle
            ligne.append(C[i-1][j-1]+C[i-1][j])
        if (i!=0): # pour i=0 ligne = [1]
            ligne.append(1)
        C.append(ligne)
        
    for ligne in C:
        for e in ligne:
            print(e,end='\t')
        print()
    return C


            

#trianglePascal()
C= trianglePascal()