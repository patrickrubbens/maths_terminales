import numpy as np
import matplotlib.pyplot as plt

# Sample data and plot
x = [1, 2, 3, 4, 5]
y = [1, 2, 3, 4, 5]

fig, ax = plt.subplots()
ax.scatter(x, y)

# Grid line width
ax.grid(which = "major", linewidth = 1)
ax.grid(which = "minor", linewidth = 0.2)
ax.minorticks_on()

plt.show()