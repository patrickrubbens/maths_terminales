import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
# https://pyspc.readthedocs.io/fr/latest/05-bases/10-graphiques_partie_1.html
I=[0,25e-3,50e-3,75e-3,100e-3,125e-3]
U=[0,1.8,3.3,5.12,6.8,8.5]
plt.figure("loi d'Ohm", figsize=(12,10), dpi=150)

# plus sur les couleurs : https://matplotlib.org/stable/users/explain/colors/colors.html
# https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html

plt.plot(I,U,color='b', marker = 'x',markersize = 12,
         linestyle='', linewidth = 2, label='U=f(I)')
plt.legend(loc=2)
plt.xlim(0,0.150)
plt.ylim(0,10)
plt.xlabel("intensité I (A)")
plt.ylabel("tension U (V)")
plt.gca().xaxis.set_major_locator(MultipleLocator(0.01))
plt.gca().xaxis.set_minor_locator(MultipleLocator(0.001))
plt.gca().yaxis.set_major_locator(MultipleLocator(1))
plt.gca().yaxis.set_minor_locator(MultipleLocator(0.1))
plt.grid(color='r', linestyle='--', linewidth=0.75)
plt.title("Caractéristique Intensité-Tension d’un dipôle ohmique",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
plt.text(0.100,5,'résistance du dipôle égale à 68 Ohms',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)
plt.show()