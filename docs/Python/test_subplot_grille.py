#import matplotlib.pyplot as plt
#
##https://courspython.com/tracer-plusieurs-courbes.html
#
##https://pyspc.readthedocs.io/fr/latest/05-bases/10-graphiques_partie_2.html
#
##plt.subplot(nombre de lignes, nombre de colonnes, index)
#
#plt.subplot(221)
#plt.xticks([])
#plt.yticks([])
#plt.text(0.5, 0.5, "subplot(221)", ha="center", va="center", size=24, alpha=.5)
#
#plt.subplot(222)
#plt.xticks([])
#plt.yticks([])
#plt.text(0.5, 0.5, "subplot(222)", ha="center", va="center", size=24, alpha=.5)
#
#plt.subplot(223)
#plt.xticks([])
#plt.yticks([])
#plt.text(0.5, 0.5, "subplot(223)", ha="center", va="center", size=24, alpha=.5)
#
#plt.subplot(224)
#plt.xticks([])
#plt.yticks([])
#plt.text(0.5, 0.5, "subplot(224)", ha="center", va="center", size=24, alpha=.5)
#
#plt.show()

import matplotlib.pyplot as plt
import numpy as np

# Create figure and subplots

fig, ax = plt.subplots()

# Define Data Coordinates

x = np.arange(0, 10, .01)
y = x**2

# Plot

plt.plot(x,y,color='b')


# Définition des axes

x_min, x_max = 0,10
y_min, y_max = 0,100

plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)

# Set ticks
#grid_x_ticks = np.arange(x_min, x_max, 0.2)
#grid_y_ticks = np.arange(y_min, y_max, 1)
#ax.set_xticks(grid_x_ticks , minor=True)
#ax.set_yticks(grid_y_ticks , minor=True)

# affichage grille et sous-grille

#plt.grid()
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on()

# Titre

plt.title("Parabole")

# Affichage

plt.xlabel("x")
plt.ylabel("y = x²")
plt.show()