import numpy as np
import matplotlib.pyplot as plt

x_min , x_max = 0 , 10
y_min, y_max = 0 , 10

# création de la fenêtre graphique 1
plt.figure("ASTROÏDE", figsize=(12,10), dpi=100)

###########################################################################################################

# division de la fenêtre graphique en 3 lignes, 2 colonnes, graphique en position 1
plt.subplot(3,2,1)

# titre
plt.title("Une droite",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,1))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#la première droite
plt.plot([0,1],[10,0],color='b', linestyle='-', linewidth = 2)
plt.text(2,5,'la première droite relie le point (0,10) et le point (1,0)',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

# division de la fenêtre graphique en 3 lignes, 2 colonnes, graphique en position 3
plt.subplot(3,2,3)

# titre
plt.title("Deux droites",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')

# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,1))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#les tangentes
plt.plot([0,1],[10,0],color='b', linestyle='-', linewidth = 2)
plt.plot([0,2],[9,0],color='b', linestyle='-', linewidth = 2)
plt.text(2,5,'la deuxième relie le point (0,9) et le point (2,0)',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

# division de la fenêtre graphique en  lignes, 2 colonnes, graphique en position 5
plt.subplot(3,2,5)

# titre
plt.title("Trois droites",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')

# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,1))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#les tangentes
plt.plot([0,1],[10,0],color='b', linestyle='-', linewidth = 2)
plt.plot([0,2],[9,0],color='b', linestyle='-', linewidth = 2)
plt.plot([0,3],[8,0],color='b', linestyle='-', linewidth = 2)
plt.text(2,5,'la troisième relie le point (0,8) et le point (3,0)',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

# division de la fenêtre graphique en 1 ligne, 2 colonnes, graphique en position 2
plt.subplot(1,2,2)

# titre
plt.title("1/4 d'astroïde",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
plt.text(5,5,'On a tracé 10 droites',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

# puis caractéristiques de ce graphique
plt.axis([0,x_max,0,y_max])
plt.xlabel("x")
plt.ylabel("y")
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(0,y_max,1))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

# les tangentes
for x in range(10):
    abs = np.array([0,10-x])
    ord = np.array([x+1,0])
    plt.plot(abs,ord,color='b', marker = '.',markersize = 12,linestyle='-', linewidth = 1)


###########################################################################################################

plt.show()
