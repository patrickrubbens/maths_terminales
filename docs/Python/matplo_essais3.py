import numpy as np
import matplotlib.pyplot as plt

plt.figure(1, figsize=(12,10), dpi=100)

x_min = 0
x_max = 10.0

y_min = 0
y_max = 100

x = np.arange(x_min, x_max, .01)
y = x**2


#a = 3
#m = a*2
#abs = np.array([0,a,10])
#t = np.array([m*(0-a)+a**2,a**2,m*(10-a)+a**2])

plt.axis([0,10,0,100])
plt.xlabel("x")
plt.ylabel("y")
plt.yticks(np.arange(0,100,10))
plt.xticks(np.arange(0,10,1))
plt.grid(color='r', linestyle='--', linewidth=0.75)

plt.plot(x,y,color='b', linestyle='-', linewidth = 2, label='y = x²')

for a in range(10):
    m = a*2
    abs = np.array([0,a,10])
    t = np.array([m*(0-a)+a**2,a**2,m*(10-a)+a**2])
    plt.plot(abs,t,color='r', marker = '.',markersize = 12,linestyle='-.', linewidth = 1)
    
plt.show()