import numpy as np
import matplotlib.pyplot as plt

x_min , x_max = 0 , 20
y_min, y_max = 7200 , 11000

# création de la fenêtre graphique 1
plt.figure("Comparaison suites arithmétiques et suites géométriques", figsize=(12,10), dpi=100) 
###########################################################################################################

# division de la fenêtre graphique en 1 ligne, 2 colonnes, graphique en position 1
plt.subplot(1,2,1)
# titre
plt.title("Les deux suites",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
# puis caractéristiques de ce graphique
plt.axis([0,x_max,7000,y_max])
plt.xlabel("x")
plt.ylabel("y")
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(7000,y_max,1000))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

# la suite géométrique
x = np.arange(x_min, x_max, 1)
y = 7200*1.02**x

plt.plot(x,y,color='b', linestyle='', marker = '.',markersize = 12, label='la suite géométrique')

# la suite arithmétique

y = 7200+160*x

plt.plot(x,y,color='r', linestyle='', marker = '.',markersize = 12, label='la suite arithmétique')


###########################################################################################################

# division de la fenêtre graphique en 2 lignes, 2 colonnes, graphique en position 4
plt.subplot(2,2,4)

# titre
plt.title("La suite géométrique",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')
# puis caractéristiques de ce graphique
plt.axis([0,x_max,7000,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(7000,y_max,1000))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#la suite géométrique
y = 7200*1.02**x
plt.plot(x,y,color='b', linestyle='',marker = '.',markersize = 12, linewidth = 2, label='la suite géométrique')
plt.text(5,50,'Terme initial = 1 ; raison q = 1,25',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

# division de la fenêtre graphique en 2 lignes, 2 colonnes, graphique en position 2
plt.subplot(2,2,2)

# titre
plt.title("La suite arithmétique",
          fontsize=12,family='monospace',fontweight='bold',
          style='italic',color='m', backgroundcolor='y',
          horizontalalignment='center')

# puis caractéristiques de ce graphique
plt.axis([0,x_max,7000,y_max])
plt.xticks(np.arange(0,x_max,1))
plt.yticks(np.arange(7000,y_max,1000))
plt.grid(which='major')
plt.grid(which='minor', linewidth = 0.2, linestyle='--')
plt.minorticks_on() # pour avoir les deux grilles

#la suite arithmétique
y = 7200+160*x
plt.plot(x,y,color='r', linestyle='',marker = '.',markersize = 12, linewidth = 2, label='la suite arithmétique')
plt.text(5,50,'Terme initial = 1 ; raison r = 3',
         fontsize=8, family='serif',fontweight='heavy',
         style='oblique',color='m',backgroundcolor='c', alpha=1)

###########################################################################################################

plt.show()