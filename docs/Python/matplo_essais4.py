import numpy as np
import matplotlib.pyplot as plt

plt.figure(1, figsize=(12,10), dpi=100)



x_min = 0
x_max = 10.0

y_min = 0
y_max = 10.0

plt.axis([0,10,0,10])
plt.xlabel("x")
plt.ylabel("y")
plt.xticks(np.arange(0,10,0.5))
plt.yticks(np.arange(0,10,1))

plt.grid(which='major',alpha = 1, linestyle='-', linewidth=0.75)
plt.grid(which='minor', linewidth = 0.75, linestyle='--')
plt.minorticks_on()

plt.show()