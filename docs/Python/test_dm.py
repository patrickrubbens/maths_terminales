# Deux nombres ont pour somme 42. Si on ajoute 5 à chacun d’eux, de combien augmente leur produit ?

x=2548 # on choisit x au hasard
y=42-x # ainsi x+y=42

P1=x*y
P2=(x+5)*(y+5)

D=P2-P1

print(D)  # on constate que la différence des deux produit est TOUJOURS 235.

#Démontrer que si x+y=42 alors xy+235 = (x+5)*(y+5)
