import pandas
import matplotlib.pyplot as plt
import numpy as np
from urllib.request import urlretrieve
from datetime import date

# une fonction pour formater les dates YYYYMM au format standard YYYY-MM-JJ
def formate(d: int) : #-> date YYYYMM
    s = str(d)
    return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01')

def collect_datas(lieu="Monde", an=1970):
    # récupération de l'année courante
    auj = date.today().year
    # on prend le fichier de l'année courante s'il est disponible (à partir de  mars)
    # sinon l'année précédente
    if date.today().month >= 3:
        an_deb = auj
    else: auj - 1
    # choix du fichier dans la base de données
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
        
    # récupération du fichier choisi
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    urlretrieve(url, filename="./temp_anomalies.csv")
    # on prend le tableau csv à partie de la ligne 5 : titre des colonnes Year Anomaly
    df = pandas.read_csv("temp_anomalies.csv", header=4)
    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ
    df['Year'] = df['Year'].map(formate)
     # renomme les colonnes en français
    df.columns = ["Année", "Anomalie"]
    return df

def convert_csv(filescsv="temp_anomalies.csv"):
    # on prend le tableau csv à partie de la ligne 5 : titre des colonnes Year Anomaly pour le data frame pandas
    df = pandas.read_csv(filescsv, header=4)
    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ
    df['Year'] = df['Year'].map(formate)
     # renomme les colonnes en français
    df.columns = ["Année", "Anomalie"]
    return df

def clim(lieu="Monde", an=1970):
    df = convert_csv()
    tit = "dans le monde"
    # on récupère le dernier mois fourni pour le titre
    dernier = df.iloc[-1,0] 
    titre = f"Évolution des températures {tit} depuis {an}\nDernière mesure le 1er {dernier.strftime('%B %Y')} "
    # Tracé du nuage    
    graph = df.plot.scatter(x='Année', y='Anomalie', s=1, c='Anomalie', colormap="viridis")
    plt.title(titre)
    graph.grid(True)
    plt.show()
    return graph


#df = collect_datas(lieu="Monde", an=1970)
# x = np.array(df['Year'])
#    x = df['Année'].tolist()
#    x = np.array([i for i in range(len(x))])
#    y = df['Anomalie'].tolist()
#    a, b = np.polyfit(x, y, 1)
#    plt.plot(x, a * x + b, 'r')

