# Chapitre 7 : Fonctions exponentielles

* Rappel : Calculer avec des exposants (faire avant avec automatismes).
* Définition avec représentation graphique.
* Sens de variation.
* Propriétés algébriques.
* Application : Retour sur le taux moyen (voir livre page 51)
