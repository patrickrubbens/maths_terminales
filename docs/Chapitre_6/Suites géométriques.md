# Chapitre 6 : Suites géométriques.

* Moyenne géométrique- croissance exponentielle.
* Expression du terme général d’une suite géométrique (attention, pas au programme de la
première).
* Somme des premiers termes d’une suite géométrique.