# Probabilités conditionnelles.

## Rappel : tableau croisé d’effectifs, construire et utiliser un arbre pondéré pour des épreuves indépendantes.

Un essai de laboratoire porte sur 35 cobayes et 15 rats. Après 5 jours, $40\%$ des cobayes sont malades ainsi que $20\%$
des rats. 

$\begin{array}{|l|c|c|c|}
 \hline
               & \text{Cobayes} &  \text{Rats}  & \text{Total}   \\\hline
 \text{Malade} &                &               &                 \\\hline                        
 \text{Sain }  &                &               &                  \\\hline 
 \text{Total}  &                &               &    100\%       \\\hline 
\end{array}$


1. Compléter le tableau par des pourcentages. 

2. Quel est, arrondi au dixième, le pourcentage de rats parmi les animaux sains ?

 

  On note les événements :

    * $M$: "l'animal est malade",
    * $C$: "l'animal est un cobaye". 

    Dans le contexte de cet exercice, que signifie $\overline{M}$ ? et $\overline{C}$ ?  

3. On choisit un animal au hasard. Quelle est la probabilité que ce soit un cobaye et qu'il soit malade ? Comment pourrait-on noter cette probabilité ?

4. On choisit un animal parmi les cobayes. Quelle est la probabilité qu'il soit malade ? Comment peut-on noter ce résultat ? $P(M)$ vous semble t'il une notation adéquate?

On notera $P_C(M)$ la probabilité qu'un animal choisit **parmi les cobayes** soit malades. 

5. Essayer de trouver une relation liant les probabilités $P(C \cap M)$, $P_C(M)$ et $P(C)$. 

## Conditionnement par un événement et Probabilité Conditionnelle.

### Définition

Soient $A$ et $B$ deux événements, avec $P(A)\not=0$. 

La probabilité conditionnelle de de l'événement $B$ sachant $A$, notée $P_A(B)$, est définie par :
  
$$P_A(B)=\frac{P\left( A \cap B \right)}{P(A)}$$


Soit $A$ et $B$ deux événements, alors $P\left( A \cap B \right) = P(A) \times P_A(B)$.



Remarque Importante: Il est évident que $P\left( A\cap B\right) = P\left( B \cap A\right)$. 

On a aussi de même, $P\left( B \cap A\right) = P(B) \times P_B(A)$.

Et donc finalement : $P(A) \times P_A(B) = P(B) \times P_B(A)$


### Arbre pondéré

![arbre_v1](arbre_v1.png)

![arbre_v2](arbre_v2.png)

* Formule des probabilités totales.
* Indépendance de deux événements.
* S'entrainer:
  * [Lire sur un arbre pondéré](http://bref.jeduque.net/zey1yk)
  * [Calculer sur un arbre pondéré](http://bref.jeduque.net/o3kgn6)
