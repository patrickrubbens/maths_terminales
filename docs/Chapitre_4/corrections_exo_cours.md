# Correction des exercices du cours

## exercice 1

### 26 page 172

1. Dans l'énoncé, il est bien spécifié qu'on répète le schéma de Bernouilli de manières **indépendantes**. On se trouve donc bien dans le cadre de la loi Binomiale. La probabilité d'un succés est $p(S)=0,8$

    Les paramètres de la loi Binomiale sont $n=3$ et $p=0,8$.

2. En observant l'arbre, on peut voir que $3$ chemins sont possibles pour qu'il y ait $2$ succés. 
    
    Ces chemins sont:
    
     $$S-S- \overline{S} \text{ ou bien } S-\overline{S}-S \text{ ou bien } \overline{S}-S-S$$
 
    $$P(X=2)= (0,8) \times (0,8) \times (0,2) + (0,8) \times (0,2) \times (0,8) + (0,2) \times (0,8) \times (0,8)$$

    Mais pour chaque termes, peu importe l'ordre dans lequel on fait les mutiplications, donc:

    $$P(X=2)= (0,8)^2 \times (0,2) + (0,8)^2 \times (0,2) + (0,8)^2 \times (0,2)$$

    Il est clair qu'on additionne $3$ fois le même nombre $(0,8)^2 \times (0,2)$

    Donc, finalement:

    $$P(X=2)=3 \times (0,8)^2 \times (0,2)^1$$

    Et le calcul donne alors: $P(X=2)=0.384$

## exercice 2

### 36 page 172

On cherche à nouveau à calculer $P(X=2)$. On reprends exactement les mêmes arguments que dans l'exercice précédent pour justifier que X suit une loi binomiale de paramètres $n=4$ et $p=0,9$

De nouveau, en observant l'arbre, $6$ chemins sont possibles pour obtenir $X=2$.

$$
S-S-\overline{S}-\overline{S} 
\text{ ou bien }
S-\overline{S}-S-\overline{S}
\text{ ou bien }
\overline{S}-S-S-\overline{S}
\text{ ou bien }
S-\overline{S}-\overline{S}-S
\text{ ou bien }
\overline{S}-S-\overline{S}-S
\text{ ou bien }
\overline{S}-\overline{S}-S-S
$$

Et chacune de ces **combinaisons** aboutit au même résultat : $(0,9) \times (0,9)\times (0,1) \times (0,1)$

Donc, finalement:

$$P(X=2)=6 \times (0,9)^2 \times (0,1)^2$$

$$P(X=2)=0,0486$$

## Triangle

|X=k |X=0 |X=1 |X=2 |X=3 |X=4 |X=5 |X=6 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|n=1 |  1 | 1  |    |    |    |    |    | 
|n=2 |  1 | 2  | 1  |    |    |    |    |   
|n=3 |  1 | 3  | 3  | 1  |    |    |    |
|n=4 |  1 | 4  | 6  | 4  |  1 |    |    |
|n=5 |  1 | 5  | 10 | 10 | 5  | 1  |    |
|n=6 |  1 | 6  | 15 | 20 | 15 | 6  |  1 |


## Python

```python
def triangle(n):
    lp = [1] # on initialise la première ligne
    for j in range(n+1):
        print(lp)
        nl = lp + [1]
        for i in range(len(lp) - 1):
            nl[i + 1] = lp[i] + lp[i + 1] # c'est ici que la magie opère: les deux coeff de la ligne précédente sont additionnés
        lp = nl
```

Collez ce code dans la fenêtre de gauche (script python):[basthon](https://console.basthon.fr/){target=_blank} et exécuter.

Tapez ensuite dans la fenêtre de droite (console) `triangle(6)`. Vous obtenez le triangle précédent.

Évidemment, vous pouvez taper  `triangle(7)` ou  `triangle(10)`,...

Une petite variante, pour obtenir directement le coefficient sans afficher tout le triangle:

```python
def coeff(n,k):
    lp = [1] # on initialise la première ligne
    for j in range(n):
        nl = lp + [1] # nouvelle ligne (nl) on recopie la ligne précédente (lp) et on rajoute 1 à la fin
        for i in range(len(lp) - 1):
            nl[i + 1] = lp[i] + lp[i + 1] # c'est ici que la magie opère: les deux coeff de la ligne précédente sont additionnés
        lp = nl #nouvelle ligne va maintenat jouer le rôle de ligne précédente pour recommencer
    print(f"le nombre de combinaisons pour n={n} et X={k} est égal à {nl[k]}")
```

Dans la fenêtre de droite (console) `coeff(5,2)` 

Vous retrouverez le résultat $10$ qui correspond donc à $n=5$ et $X=2$

C'est ainsi que votre calculatrice donne $\dbinom{5}{2}=10$

## exercice 3

41 page 173

1. L'événement $A$ comporte $5$ succès et donc $3$ échecs puisqu'on a lancé $8$ fois la pièces.

    En notant $X=$ "le nombre de fois où on obtient le côté face", il est clair que $P(A)=P(X=5)$

    Donc $X\hookrightarrow \mathcal B (8;0,5)$ car on lance $n=8$ fois la pièce non truquée $p=\frac{1}{2}$

2. Bien sûr, il n'est pas question de dessiner l'arbre, mais de l'imaginer. 
    $\dbinom {8}{5}$ correspond au nombre de combinaisons possibles pour obtenir $5$ fois le côté face en $8$ lancers.

    $\dbinom {8}{5}=56$ 

3. Ainsi $P(A)=P(X=5)=\dbinom {8}{5} \times (0,5)^5 \times (0,5)^3$. 

    Ici, comme la pièce est équilibrée, donc non truquée, $p=0,5$ et $1-p=1-0,5=0,5$

    et donc $P(A)=56 \times (0,5)^8=0,21875$

