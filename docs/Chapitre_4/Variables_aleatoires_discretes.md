# Chapitre 4 : Variables aléatoires discrètes.

## Variable aléatoire.

En théorie des probabilités, une variable aléatoire est une variable dont la valeur est déterminée après la réalisation d’un phénomène, expérience ou événement, aléatoire.

En voici des exemples : la valeur d’un dé entre 1 et 6 ; le côté de la pièce dans un pile ou face ; le nombre de voitures en attente dans la 2e file d’un télépéage autoroutier ; le jour de semaine de naissance de la prochaine personne que vous rencontrez ; le temps d’attente dans la queue du cinéma ; etc...

Ce furent les jeux de hasard qui amenèrent à concevoir les variables aléatoires, en associant à une éventualité (résultat du lancer d’un ou plusieurs dés, d’un tirage à pile ou face, d’une roulette, etc.) un gain. Cette association éventualité-gain a donné lieu par la suite à la conception d’une théorie bien plus générale dont nous n'aborderons que les principes de base.

## Loi de probabilité

La fonction mathématique décrivant les valeurs possibles d'une variable aléatoire et leur probabilité est connue sous le nom de **Loi de probabilité**


## Epreuve de Bernoulli.

Une épreuve de Bernoulli de paramètre p (réel compris entre 0 et 1) est une expérience aléatoire (c'est-à-dire soumise au hasard) comportant deux issues, le succès ($S$) ou l'échec ($\overline{S}$).

> remarque: Les termes « succès » et « échec » ne sont porteurs d'aucune valeur. Ils désignent simplement, de manière générique, les deux issues possibles. Le choix de ces termes est historiquement issu de la théorie des jeux. 

L'exemple typique est le lancer d'une pièce de monnaie possiblement truquée. On note alors p la probabilité d'obtenir pile (qui correspond disons à un succès) et 1-p d'obtenir face. 

Pour schématiser une épreuve de Bernoulli, on peut construire un arbre pondéré à $2$ branches.

Par exemple, si un sac contient $8$ jetons noirs et $2$ jetons blancs, et qu'on considère que l'on gagne (Succès) en tirant un jeton noir au hasard, alors on définit une épreuve de Bernouilli qu'on peut schématiser ainsi:

![bernouilli](bernouilli.png)

En effet $P(S)=\frac{8}{10}=0,8$ et donc $P(\overline{S})=1-P(S)=1-0,8=0,2$

## Loi Binomiale.

La répétition de plusieurs épreuves de Bernouilli sucessives et **indépendantes** mène à la **Loi Binomiale**. 

On dit qu'une variable aléatoire $X$ suit une **Loi Binomiale** lorque que $X$ est égale au nombre de succès obtenus lors de la répétition de plusieurs épreuves de Bernouilli **identiques et indépendantes**.

On dit alors que $X$ suit une **Loi Binomiale** de paramètres $n$ et $p$, où $n$ est le nombre de répétitions et $p$ la probabilité d'un Succès. 

On notera: $X\hookrightarrow \mathcal B (n;p)$

En reprenant l'exemple du sac contenant $8$ jetons noirs et $2$ jetons blancs. 

On choisit un jeton au hasard et on définit le Succès $S=$"on tire un jeton noir". Donc $P(S)=0,8$

On **remet le jeton dans le sac** une fois le tirage effectué, et on tire une deuxème fois. $X$ est la variable aléatoire correspondant au nombre de succès à l'issue des deux tirages, donc à l'issue de $2$ épreuves de Bernouilli **identiques et indépendantes**.

Donc $n=2$ et $p=0,8$ donc on notera :$X\hookrightarrow \mathcal B (2;0,8)$

Il est important de remarquer qu'on a ici un **tirage AVEC remise**, ce qui permet d'avoir des tirages **identiques et indépendants**

Exercice 30 page 172 du manuel:

![binomiale_manuel_30page172.jpg](binomiale_manuel_30page172.jpg)

Ici $Z$ ne suit pas une loi binomiale car il est dit dans l'énoncé que chaque **tirage est SANS remise**, donc l'épreuve de Bernouilli change à chaque fois.

Exercice 31 page 172 du manuel:

![binomiale_manuel_31page172.jpg](binomiale_manuel_31page172.jpg)

Cette fois $F$ suit une loi binomiale car il est dit dans l'énoncé que chaque **tirage est AVEC remise**, on remet la carte tirée dans le paquet. Donc $F\hookrightarrow \mathcal B (5;0,125)$ car $p=\frac{4}{32}=\frac{1}{8}=0,125$

Exercice 26 page 172 du manuel:

![ex26p172](ex26p172.png)

[correction](corrections_exo_cours.md/#exercice-1){target=_blank}

-------------------------------------------

Exercice 36 page 173 du manuel:

![ex36p173](ex36p173.png)

1. Justifier que $X$ suit une loi binomiale dont on déterminera les paramètres.
2. Calculer $P(X=2)$

[correction](corrections_exo_cours.md/#exercice-2){target=_blank}

--------------------------------------------

## Triangle de Pascal

Le problème qui va rapidement se poser est qu'en répétant un plus grand nombre de fois le schéma de Bernouilli, il deviendra impossible de construire l'arbre associé... 

Imaginez simplement sur l'arbre précédent, on a dessiné $16$ chemins en tout. 

En rajoutant une répétition, nous aurons le double de chemins, soit $32$.  Je ne sais pas vous, mais pas franchement envie de dessiner un tel arbre...

Quelles seraient alors les combinaisons pour $X=2$ ? $S-S-\overline{S}-\overline{S}-\overline{S}$ ou bien ...

Il va falloir réflèchir différemment ! On va faire "comme si" le premier tirage avait déjà eu lieu. Deux possibilités s'offre à nous:

* le premier tirage a aboutit à un succés:
    
    Pour le dire autrement, on fixe le premier $S$ dans $S-S-\overline{S}-\overline{S}-\overline{S}$, et il y a donc $4$ chemins $S-\overline{S}-\overline{S}-\overline{S}$ et donc $4$ chemins qui commencent par $S$.
    
    Donc il y aura **$4$ combinaisons commençant par $1$ succés**.

* le premier tirage a aboutit à un succés:
    Alors il reste à dénombrer les **combinaisons commençant par $\overline{S}$**: $\overline{S}-S-S-\overline{S}-\overline{S}$ ou bien ... cette fois on fixe le premier $\overline{S}$

    Ce qui signifie que pour avoir ensuite $2$ succés, il faut compter $S-S-\overline{S}-\overline{S}$

    On sait qu'il y a $6$ combinaisons pour $S-S-\overline{S}-\overline{S}$ d'après le dénombrement précédent.

* Finalement, on aura $4+6=10$ **combinaisons** pour $X=2$ si on répète $5$ fois le schéma de Bernouilli.

Et alors, si X suit la loi binomiale de paramètres $n=5$ et $p=0,9$

$$P(X=2)=10 \times (0,9)^2 \times (0,1)^3$$

Le raisonnement que nous venons de tenir doit amener à penser d'une manière plus "systèmatique". Si je suis capable de connaitre les combinaisons pour $n=4$ je pourrais en déduire les combinaisons pour $n=5$, et donc connaissant les combinaisons pour $n=5$ je pourrais en déduire les combinaisons pour $n=6$, et ainsi de suite.

**Si je connais les combinaisons pour $n$ je pourrais en déduire les combinaisons pour $n+1$**
    
On va tenter de mettre ce raisonnement par récurrence en place, c'est un algorithme qu'on appelle **triangle de Pascal** en hommage à cet illustre mathématicien philosophe (voir pour [la petite histoire](#Pour-la-petite-histoire))

Il est clair que pour $1$ répétition du schéma de Bernouilli, il y a $1$ seul chemin pour obtenir $0$ succés et $1$ seul chemin pour obtenir $1$ succés.

pour $2$ répétitions du schéma de Bernouilli, il y a $1$ seul chemin pour obtenir $0$ succés, $2$ chemins pour obtenir $1$ succés et $1$ seul chemin pour obtenir $2$ succés.

pour $3$ répétitions du schéma de Bernouilli, il y a $1$ seul chemin pour obtenir $0$ succés, $3$ chemins pour obtenir $1$ succés, $3$ chemins pour obtenir $2$ succés et $1$ seul chemin pour obtenir $2$ succés.

pour $4$ répétitions du schéma de Bernouilli, il y a $1$ seul chemin pour obtenir $0$ succés, $4$ chemins pour obtenir $1$ succés, $6$ chemins pour obtenir $2$ succés, $4$ chemins pour obtenir $3$ succés et $1$ seul chemin pour obtenir $4$ succés.

Traduisons ces résultats dans un tableau:

|X=k |X=0 |X=1 |X=2 |X=3 |X=4 |
|:--:|:--:|:--:|:--:|:--:|:--:|
|n=1 |  1 | 1  |    |    |    |
|n=2 |  1 | 2  | 1  |    |    |
|n=3 |  1 | 3  | 3  | 1  |    |
|n=4 |  1 | 4  | 6  | 4  |  1 |

On vient de voir que si $n=5$, pour $X=2$ on aura $10$ combianisons. 

Saurez-vous compléter la ligne suivante du tableau ?

-----------------------------------------------

|X=k |X=0 |X=1 |X=2 |X=3 |X=4 |X=5 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|n=1 |  1 | 1  |    |    |    |    |
|n=2 |  1 | 2  | 1  |    |    |    |  
|n=3 |  1 | 3  | 3  | 1  |    |    |
|n=4 |  1 | 4  | 6  | 4  |  1 |    |
|n=5 |    |    |    |    |    |    | 

----------------------------------------------------
Continuons ...

|X=k |X=0 |X=1 |X=2 |X=3 |X=4 |X=5 |X=6 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|n=1 |  1 | 1  |    |    |    |    |    | 
|n=2 |  1 | 2  | 1  |    |    |    |    |   
|n=3 |  1 | 3  | 3  | 1  |    |    |    |
|n=4 |  1 | 4  | 6  | 4  |  1 |    |    |
|n=5 |    |    |    |    |    |    |    |
|n=6 |    |    |    |    |    |    |    |

[correction](corrections_exo_cours.md/#triangle){target=_blank}

------------------------------------------------------

Ainsi, à la ligne n=5 et à la colonne X=2, on obtient $10$. C'est donc le nombre de combinaisons possibles pour obtenir 2 succès en 5 tirages identiques et indépendants.

Les coefficients qui apparaisent dans ce tableau sont appelés **coefficients binomiaux**.

Il existe une notation pratique pour désigner ces coefficients et ainsi éviter les longues phrases.

Le coefficient binomial correspondant à n=5 et à X=2 sera noté : $\dbinom {5}{2}$ 

Donc $\dbinom {5}{2}=10$. En vous basant sur le triangle de Pascal, $\dbinom {6}{3}=$... ?

$\dbinom {n}{k}$ est égal au nombre de combinaisons possibles pour $X=k$ succès en $n$ tirages identiques et indépendants.

On peut continuer le triangle de Pascal, évidemment, mais un tel algorithme se programme très facilement en machine.

### En langage Python par exemple: [coefficients binomiaux](https://console.basthon.fr/?script=eJx1Us2O0zAQviPxDqPtYRtRDgUtQpX6HkhVD44zqWbrjLN2XC1U-y4c6Z0bx7wYn-2uihDk4Phnvp_57I57sp77fqmrY7N5-4bwuZG2tFvvaUFeSVQmMU4ikzM0Bh5k_hGwkINyBfQ-0CMKKRg98FJfifKnDmRgfHdlVJ9O7NwVT0t1TVYJbP0oRaIejGG-2PnSsU6ocmNDPJVC8-gTttY0f8_VvehNLBuRmxHHWpDvaf2npWprJ_AES9ndTvZY5H_dW5C95ziRWKGnVFwN5gB7fsy9b8hxpI7Tcw0P0_8Yj14nMl2HCL3qfIk3FyVlpLP4K5FT1hKA1UyEXjlAjcL8E2PH_xIZfQolwGFgtRyqxhhAsuzvAFM_tKGgUdOKGoGvWHG6PetLzvbL9nzEBF3Pl4NxOd4zUjruX-6ayvg6LoifIY9b-EY2c4KYlayTp2TQb0zFcot7woXhpDXoO-MmMwLDGpNMSLG-vIfVh4Y6A0PI0MKYBxjqXfCoqsCTT8XvCfBWQBnnXwFmo_mKfLp7k6aAKzkZxylEKlHQpmKryqfVRzy0BHHrA183P68emt_pzv0-){target=_blank}

D'autres algorithmes sont possibles: [algorithme](corrections_exo_cours.md/#python){target=_blank}

### Sur la calculatrice Numworks: [Émulateur calculatrice](https://www.numworks.com/fr/simulateur/){target=_blank}

Vérifions que $\dbinom{5}{2}=10$

étape1: dans le menu `calcul`, appuyer sur la touche `paste`:

![numworks01](numworks01.png)

puis suivez ces étapes:

|étape 2|étape 3|étape 4|étape 5|
|:-----:|:-----:|:-----:|:-----:|
|![numworks02](numworks02.png)|![numworks03](numworks03.png)|![numworks04](numworks04.png)|![numworks05](numworks05.png)|

Vérifiez maintenat votre résulat pour $\dbinom {6}{3}$

### En pratique :

Activité page 169 du manuel:

![binomiale_manuelpage169](binomiale_manuelpage169.jpg)

Exercice 41 page 173 du manuel:

![binomiale_manuel_41page173.jpg](binomiale_manuel_41page173.jpg)

[correction](corrections_exo_cours.md/#exercice-3){target=_blank}

## Espérance mathématique.



Exercice 49 page 173 du manuel:

![binomiale_manuel_49page173.jpg](binomiale_manuel_49page173.jpg)


## Pour la petite histoire

### Le paradoxe du chevalier de Méré

Le chevalier de Méré était un noble de la cour de Louis XIV. Selon une lettre de Pascal à Fermat (datant du 29/07/1654), il "avait très bon esprit, mais n'était pas géomètre". 

* Est-il avantageux, lorsqu'on joue au dé, de parier sur l'apparition d'un 6 en lançant 4 fois le dé? 
* Est-il avantageux de parier sur l'apparition d'un double-six, quand on lance 24 fois deux dés?

Le chevalier de Méré, qui était un grand joueur, avait remarqué que le premier jeu était avantageux. Et en effet, la probabilité d'apparition d'un 6 en lançant 4 fois un dé est : $1−(\frac{5}{6})^4≃0,5177$.

Donc très légèrement supérieur à $\frac{1}{2}$: ce deuxième jeu est avantageux.

Se laissant abuser par un soi-disant argument d'homothétie, le chevalier considérait que le deuxième pari était aussi avantageux : en lançant un dé, il y a $6$ issues; en lançant $2$ dés, il y en a $36$, soit $6$ fois plus. Puisqu'il est avantageux de parier sur l'apparition d'un $6$ en lançant le dé $4$ fois de suite, il doit être avantageux de miser sur l'apparition d'un double-six en lançant un dé $24$ fois de suite, puisque $24= 4 \times 6$. 

Malheureusement pour le chevalier, les règles des probabilités sont plus complexes, et c'est Pascal qui calcula la vraie probabilité : $1−(\frac{35}{36})^{24}≃0,4914$

Elle est donc très légèrement inférieure à $\frac{1}{2}$: le deuxième jeu n'est pas avantageux!
