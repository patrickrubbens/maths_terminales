# 1. Nombre dérivé

## Partie 1 : Rappel sur les droites.
 
Depuis la classe de seconde, on sait que tout point de coordonées $(x;y)$ tel que $y=mx+p$ sont alignés.

Réciproquement, les coefficients $m$ et $p$ caractérisent une unique droite, et un point $M$ de coordonées $(x;y)$ choisit au hasard sur cette droite sera tel que $y=mx+p$.

  > Prenons par exemple l'équation $y=2x+5$ et les points $M(2;9)$ et $N(-1;3)$. </br>
    Pour $M(2;9)$ : $2x_M+5=2 \times 2+5=4+5=9$ et on vérifie bien que $y_M=2x_M+5$ </br>
    Pour $N(-1;3)$ : $2x_N+5=2 \times (-1)+5=(-2)+5=3$ et on vérifie bien que $y_N=2x_N+5$ </br>
    On peut alors dire que la droite $(MN)$ a pour équation $y=2x+5$

### **Équation générale d'une droite :**

$$y=mx+p$$

* m est le **coefficient directeur** de la droite
* p est l' **ordonnée à l'origine** de la droite

### Coefficient directeur d'une droite : 

$$m=\frac{dy}{dx}$$
  
  Dans cette notation, le $d$ signifie **"_différence_"**.
  
  Le coefficient directeur de la droite (AB) est égal à : 
  
$$m=\frac{y_B-y_A}{x_B-x_A}$$

  > C'est à dire *"la différence entre les y des deux points divisée par la différence entre les x des deux points"*



## Partie 2 : Tangente à une courbe

### Des droites mais une courbe...

Prenons l'exemple de la courbe qu'on appelle _astroïde_.

C'est une courbe compliquée à réaliser, lorsqu'on s'intéresse à son équation: $(x^2+y^2-1)^3+27x^2y^2=0$... ouf !

![geogebra_astroide.png](geogebra_astroide.png)

Or, on peut envelopper cette courbe avec des droites, dont l'équation est clairement beaucoup plus simple à manipuler.

Évidemment, il va falloir plusieurs droites...
![ASTROÏDE](ASTROIDE.png)

### Définition
  
  La tangente à une courbe en un point est la meilleure approximation affine au voisinage de ce point.

  > Remarque: Une tangente à une courbe en un point doit être vue comme la meilleure droite pour remplacer la courbe autour d'un point.

![parabole_derive_enveloppetangente](parabole_derive_enveloppetangente.png)


L'enjeu est donc de déterminer l'équation de ces tangentes pour mieux connaitre la courbe.

### Coefficient directeur de la tangente

$A$ est un point d'abscisse $a$ appartenant à la courbe représentative d’une fonction $f$.

Ainsi, les coordonnées de $A$ sont $(a;f(a))$

![geogebra_derive1](geogebra_derive1.png)

Plaçons un point $M$ sur cette courbe, et notons $dx$ la différence entre les $x$ de $A$ et $M$, c'est à dire $dx=x_M-x_A$

Les coordonnées de $M$ sont $(a+dx;f(a+dx))$

![geogebra_derive2](geogebra_derive2.png)

On peut tracer la droite $(AM)$... Elle est sécante et réalise une approximation grossière de la courbe. Il est clair que ce n'est pas la meilleure droite pour "coller" à la courbe.

![geogebra_derive3](geogebra_derive3.png)

Calculons quand même le coefficient directeur de $(AM)$ : $m=\frac{dy}{dx}=\frac{f(a+dx)-f(a)}{a+dx-a}=\frac{f(a+dx)-f(a)}{dx}$

En observant bien le graphique, on peut voir que si on réduit $dx$ la droite $(AM)$ va se rapprocher de la tangente.

![geogebra_derive4](geogebra_derive4.png)

La tangente sera la position limite de $(AM)$ lorsque $dx$ sera très proche de $0$. Évidemment, $dx$ ne peut pas être égal à $0$, sinon $M=A$ et celà n'aurait pas de sens de parler de la droite $(AA)$

La notation choisie pour parler de limite est la suivante: $\lim\limits_{dx \to 0}...$

Et donc le coefficient directeur de la tangente en $A$ : $\lim\limits_{dx \to 0}\frac{f(a+dx)-f(a)}{dx}$

## Partie 3 : Nombre dérivé

### Définition - Notation

Le coefficient directeur de la tangente à une courbe en un point d'abscisse $a$ s'appelle le nombre dérivé de $f$ en $a$ et se note $f'(a)$.

$$f'(a)=\lim\limits_{dx \to 0}\frac{f(a+dx)-f(a)}{dx}$$

Vous pourrez observer de manière dynamique ce résultat établit dans la partie 2: cliquez [figure dynamique sur geogebra](geogebra_derive.html){:target=_blank}



### Équation générale de la tangente

  On a vu que pour une droite $(AM)$:

$$m=\frac{y_M-y_A}{x_M-x_A}$$

  Donc si $A(a;f(a))$ et $M(x;y)$

$$m=\frac{y-f(a)}{x-a}$$

  D'un autre côté, dans le cas d'une tangente, $m=f'(a)$ et donc en remplaçant dans l'équation précédente:

$$\frac{y-f(a)}{x-a}=f'(a)$$

 et donc $y-f(a)=f'(a) \times (x-a)$

  On aboutit à l'équation générale de la tangente à la courbe de $f$ en un point d'abscisse $a$:

$$y=f'(a) \times (x-a) +f(a)$$

-----------------------------------------

## Résumé

!!! cours "Ce qu'il faut retenir"

    *   Le nombre dérivé d'une fonction$f$ en $a$ est le coefficient directeur de la tangente à la courbe de $f$ au point d'abscisse $a$ 
 
    *   Comment le déterminer graphiquement ? On choisit deux points **sur la tangente** et on calcule $\frac{dy}{dx}$

    *   Équation de la tangente à la courbe de $f$ en un point d'abscisse $a$ : $y=f'(a) \times (x-a) +f(a)$



----------------------------------------

## Pour la petite histoire ...

### Calcul infinitésimal

De nombreuses spécialités scientifiques étudient les objets en mouvement et leur changement au cours du temps. Par exemple,
lorsqu’une balle dévale une pente, sa position change. La vitesse de la balle est le taux du changement de sa position. Bien sûr,
cela peut se modifier. On appelle accélération le taux du changement de la vitesse. La question est la suivante :si vous avez une
formule mathématique décrivant la position de la balle, pouvez-vous calculer sa vitesse et son accélération ? Le problème
géométrique démarre avec une ligne courbe sur le plan et détermine comment est l’inclinaison en tout point donné. Si la courbe
est un graphique de la position de la balle contre le temps, alors sa pente représente la vitesse de la balle. Ceci avait été compris
dès l’époque d’Archimède, mais on ne connaissait alors que des méthodes approximatives pour la calculer. A la fin du XVIIème
siècle, **Isaac Newton(1642-1727)** et **Gottfried Leibniz (1644-1716)** développèrent chacun de leur côté le calcul infinitésimal, un ensemble magnifique de
règles décrivant la pente des graphiques et les idées qui y sont reliées.

Ce sujet se divisait en deux branches. 

* En partant d’une courbe, un calcul infinitésimal différentiel vous donnera le taux de variation "instantanée" de la courbe. 
* Un calcul infinitésimal intégral décrit la zone bloquée au-dessous d’elle (ce qui n'est pas au programme cette année).

Cependant, il s’agit de deux procédés opposés.

Le mot « dérivé » vient du latin « derivare » qui signifiait « détourner un cours d’eau ».

Le mot a été introduit par le mathématicien franco-italien **Joseph Louis Lagrange (1736-1813)**
pour signifier que cette nouvelle fonction dérive (au sens de "provenir") d'une autre fonction.


### Gottfried Leibniz

Né le 1er juillet 1644 à Leipzig et mort le 14 novembre 1716 à Hanovre.

De nombreuses idées de Leibniz préfigurent la théorie de la pensée moderne en physique, technologie, biologie, médecine,
géologie, psychologie, linguistique, politique, loi, théologie, histoire, philosophie et mathématiques.

Désirant être abordable, il est le plus grand créateur de notations. Il introduit le "$d$", abréviation de différence, pour la différentiation, ainsi que la notation $\frac{df}{da}$ ($=f'(a)$). 

C’est aussi grâce à lui et à Newton que le signe "$=$" se généralise. 

Il est, de plus, le premier à utiliser le terme de fonction.

### Isaac Newton

![newton](https://www.bibmath.net/bios/images/newton.jpg)

Né en 1642 à Woolsthorpe et mort en 1727 à Kensington.

Ses travaux scientifiques concernent alors principalement la
physique : l’optique (découverte de la nature
de la lumière blanche), et surtout la théorie de la gravitation. Halley, impressionné par l’importance de ses découvertes, le pousse
à les publier. Newton se met alors à la tâche et fait paraître ses Principia. Le monde scientifique se rend vite compte de
l’importance de cette œuvre. La réputation de Newton est faite : il est comblé d’honneurs.

Newton est considéré comme le fondateur, avec Leibniz du calcul différentiel et intégral. Ses travaux portent aussi sur les
fonctions et sur les courbes. 

Newton note les dérivées par un point situé au-dessus (𝑥̇ ).

### Des sages, pas si sages...

On ne peut passer ici sous silence la violente querelle qui opposa Newton et Leibniz. Newton était parvenu, quelques années auparavant, aux mêmes conclusions que Leibniz. 

En 1673, lors d’un voyage à Londres, Leibniz rencontre des mathématiciens anglais et il est admis à la Royal Society.

Newton l’accusera plus tard d’avoir lu son manuscrit sur la découverte du calcul différentiel, et une terrible dispute débutera bientôt entre les deux hommes. 

Newton accusera son homologue allemand de plagiat, intentera plusieurs actions auprès de la Royal Society de Londres, qui rendra toujours un avis unilatéral en faveur de Newton. 

S'il est vrai que Newton a réalisé ses découvertes quelques temps avant Leibniz, mais que ce dernier a publié ses résultats en premier, il semble bien que les deux mathématiciens aient fait leurs recherches indépendamment l'un de l'autre.

L'Histoire a retenu les deux noms comme inventeurs du calcul infinitésimal, et ce sont plutôt les notations symboliques de Leibniz qui se sont imposées.

<!--
### Gottfried Leibniz

Né le 1er juillet 1644 à Leipzig et mort le 14 novembre 1716 à Hanovre.
Il est le fils d’un professeur de philosophie morale de l’université de Leipzig. Dès l’âge de 6 ans, il campe dans la bibliothèque
paternelle et devient un lecteur assidu. Il entre à 15 ans l’université, où il étudie la philosophie, la théologie et le droit. Il ne fait
pas de mathématiques si l’on excepte sa découverte de l’œuvre d’Euclide lors d’un bref passage à l’université de Iena. Une fois
son doctorat de droit en poche, Leibniz se met au service de l’électeur de Mayence, puis du prince de Hanovre, dans un poste de
nature diplomatique. On l’envoie en mission en France, et se lie d’amitié avec Huygens. Il approfondit son étude des
mathématiques. En 1673, lors d’un voyage à Londres, il rencontre des mathématiciens anglais et il est admis à la Royal Society.
Newton l’accusera plus tard d’avoir lu son manuscrit sur la découverte du calcul différentiel, et une grande querelle de
préséance surgira bientôt entre les deux hommes. Leibniz se rend aussi à La Haye, où il rencontre Spinoza, et à Delft, où il fait
connaissance de Leeuwenhoek. En 1676, il doit rentrer en Allemagne. Il fonde en 1682 la revue Acta Eruditorum qui lui permet de diffuser ses découvertes, mais aussi ses notations, et de rester en contact avec les frères Bernoulli. En 1700, il fonde
l’Académie de Berlin dont il devient le 1er président. La fin de sa vie est assombrie par sa querelle avec Newton et sa relative
disgrâce auprès des souverains de Hanovre.
Il meurt dans la solitude et son secrétaire, seul, assistera à ses funérailles.
De nombreuses idées de Leibniz préfigurent la théorie de la pensée moderne en physique, technologie, biologie, médecine,
géologie, psychologie, linguistique, politique, loi, théologie, histoire, philosophie et mathématiques. Il améliora la machine à
calculer de Pascal, développa la théorie binaire qui était la technologie numérique moderne, développa ce que nous connaissons
sous le nom d’algèbre de Boole et la logique symbolique. Désirant être abordable, il est le plus grand créateur de notations. Il
introduit le d, abréviation de différence, pour la différentiation, ainsi que la notation 𝑑𝑓
𝑑𝑎 (= 𝑓′(𝑎)), le symbole ∫ −c’est le s de
l’époque, première lettre du mot latin summa (somme), pour l’intégration. Il utilise systématiquement le point pour la
multiplication et les deux points ( : ) pour la division. C’est grâce à lui et à Newton que le signe = se généralise. Il est, de plus, le
premier à utiliser le terme de fonction.

### Isaac Newton


Né en 1642 à Woolsthorpe et mort en 1727 à Kensington.
Isaac Newton, enfant chétif, distrait en classe, fait partie des élèves médiocres de l’école de Grantham. Il s’intéresse à la
mécanique, écrit des vers et dessine. Par la suite, ses aptitudes se révèlent et on l’envoie en 1660 au Trinity College de
Cambridge, où il obtient son diplôme en 1664. Une épidémie de peste oblige le collège à fermer et Newton se réfugie à
Woolsthorpe. Les deux années qui suivent sont les plus fécondes de sa production mathématique, mais elle restera manuscrite.
Lorsqu’il pourra la faire publier, la qualité de ses découvertes étant reconnue, il se passionnera pour l’alchimie. En 1669, son
maître Barrow lui cède sa chaire de mathématiques à Cambridge. Ses travaux scientifiques concernent alors principalement la
physique : l’optique, et surtout la théorie de la gravitation. Halley, impressionné par l’importance de ses découvertes, le pousse
à les publier. Newton se met alors à la tâche et fait paraître ses Principia. Le monde scientifique se rend vite compte de
l’importance de cette œuvre. La réputation de Newton est faite : il est comblé d’honneurs.
La fin de sa vie est improductive au niveau scientifique. Atteint d’une dépression nerveuse en 1693, il quitte Cambridge. En
1703, il devient président de la Royal Society, poste qu’il conservera jusqu’à sa mort. Il est anobli par la reine Anne en 1705.
Newton est considéré comme le fondateur, avec Leibniz du calcul différentiel et intégral. Ses travaux portent aussi sur les
fonctions et sur les courbes. Il se passionne pour la théologie, et polémique pour établir la prééminence de ses travaux sur ceux
de Leibniz. Il paraît maintenant certain que les œuvres des deux hommes ont été conçues indépendamment, celle de Newton
précédant d’une dizaine d’années celle de Leibniz, mais ce dernier en donne une formulation plus explicite et mieux exploitable.
Malheureusement, cette querelle divise le continent et l’Angleterre, où les mathématiques vont stagner un siècle durant.
Newton note les dérivées par un point situé au-dessus (𝑥̇ ). Son œuvre en physique est fondamentale : découverte de la nature
de la lumière blanche, de la gravitation universelle.

On ne peut passer ici sous silence la violente querelle qui opposa Newton et Leibniz. Newton était parvenu, quelques années auparavant, aux mêmes conclusions que Leibniz. 
Il accusera son homologue allemand de plagiat, intentera plusieurs actions auprès de la Royal Society de Londres, qui rendra toujours un avis unilatéral en faveur de Newton. S'il est vrai que Newton a réalisé ses découvertes quelques temps avant Leibniz, mais que ce dernier a publié ses résultats en premier, il semble bien que les deux mathématiciens aient fait leurs recherches indépendamment l'un de l'autre. L'Histoire a retenu les deux noms comme inventeurs du calcul infinitésimal, et ce sont plutôt les notations symboliques de Leibniz qui se sont imposés.

-->