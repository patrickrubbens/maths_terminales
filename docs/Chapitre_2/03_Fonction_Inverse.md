# 3. Fonction inverse

## Définition
la fonction inverse est la fonction qui à tout réel $x$ **non nul** associe son inverse, noté $\frac{1}{x}$. 

On note : 

$$f(x)=\frac{1}{x};x \neq 0$$

## Courbe : HYPERBOLE
![hyperbole](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Hyperbole_1_sur_x.png/260px-Hyperbole_1_sur_x.png)

La valeur $0$ "coupe" la courbe en 2 morceaux que l'on appelle **branches hyperboliques**

## Fonction dérivée et sens de variation


 $\begin{array}{|l|lccccccr|}
 \hline
 x                      & -\infty &          &             & 0&        &              &  & +\infty   \\\hline
 \text{signe de } f'(x) &         &    -     &             &||&        &    -         &  &           \\\hline
                        & 0       &          &             &||& +\infty&              &  &            \\ 
 \text{variations de } f&         & \searrow &             &||&        & \searrow     &  &            \\
                        &         &          &  -\infty    &||&        &              & 0&            \\\hline 
\end{array}$

On remarque que la double barre sur la valeur $0$ signifie que cette valleur est interdite. Le tableau est donc interrompu. 

<!--
|x                      | $-\infty$ |              |0    |            |  $+\infty$ |
|:---:                  | :---:     |     :---:    |:---:|    :---:   |  :---:     |
|Variations de f        |           | $\searrow$   |     | $\searrow$ |            |


https://www.maths-et-tiques.fr/index.php/cours-maths/niveau-terminalet#4

-->


## $f(x)= P(x)+k \times \frac{1}{x}$ avec $x \neq 0$

## Mise en garde. Ne pas tomber dans le piège !!!

[Un exercice type complet autour de la fonction inverse](../Term_ST2S_documents/TST2S_exoFonctionInverse.pdf)
