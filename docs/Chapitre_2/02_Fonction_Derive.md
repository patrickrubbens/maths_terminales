# 2. Fonction dérivée

## Partie 1 : Rappel sur les polynômes.
 
On a vu en première que les nombres dérivés s'obtiennent facilement grâce aux fonctions dérivées, qui se calculent plus ou moins facilement à l'aide de formules.

Il faut ici connaitre la règle générale pour dériver un monôme: $f(x)=x^n$ de fonction dérivée $f'(x)=nx^{n-1}$ 

Ainsi, vous devrez connaitre sans hésitation les plus courantes:

|$f(x)$|$f'(x)$|
|:----:|:-----:|
|$x^3$|$3x^2$|
|$x^2$|$2x$|
|$x$|$1$|
|$constante$|$0$|

Dans le cas des fonctions **poly**nômes, ces formules suivent toutes la même règle.

On utilise le fait que l'opération de dérivation est linéaire, ce qui signifie qu'elle est "compatible" avec l'addition (et la soustraction) 

  > Prenons par exemple le polynôme $P(x)=x^3+x^2+x-7$. Alors $P'(x)=3x^2+2x+1-0$</br>

  > Prenons maintenant le polynôme $g(x)=5x^3$.</br> Attention, ici $5$ n'est pas une constante mais un coefficient multiplicatif pour le monôme $x^3$ qui signifie que $g(x)=x^3+x^3+x^3+x^3+x^3$ </br> Et $g'(x)=3x^2+3x^2+3x^2+3x^2+3x^2$, soit $g'(x)=5 \times 3x^2$ et enfin, on cocluera que $g'(x)=15x^2$</br> 

  > Maintenant, on considère $f(x)=5x^3-4x^2+2x-7$ </br> On peut dériver terme à terme car on a bien une addition et on connait une formule pour chaque terme </br> $f'(x)=5 \times 3x^2-4 \times 2x+2 \times 1-0$</br> 
  Et donc finalement: $f'(x)=15x^2-8x+2$

## Partie 2 : Fonction dérivée et variations de la fonction.

### Retour sur les tangentes:

2 points essentiels revus précédemment: 

  * Une tangente à une courbe en un point doit être vue comme la meilleure droite pour remplacer la courbe autour d'un point.

  * Le coefficient directeur de la tangente à une courbe en un point d'abscisse $a$ est égal à $f'(a)$.

Or, lorsqu'on connait le coefficient directeur d'une droite, on connait facilement sa variation:

  * Si le coefficient directeur d'une droite est positif, la droite monte.

  * Si le coefficient directeur d'une droite est négatif, la droite descend.

Et donc, si on parvient à connaitre le signe de $f'(a)$, on saura si la tangente monte ou descend. 

Imaginons que sur un intervalle donné, toutes les tangentes montent, alors, la courbe étant "collée" à ses tangentes va suivre le mouvement... et monter.

![parabole_derive_enveloppetangente](parabole_derive_enveloppetangente.png)

Sur le graphique, on a tracé la parabole de référence, c'est à dire la courbe représentative de $f(x)=x^2$. 

On calcule facilement $f'(x)=2x$ et il est tout aussi simple de se convaincre que $2x$ est positif quand $x$ est positif. 

On observe bien que la fonction $f$ est croissante pour $x$ positif.

### Théorème:

!!! cours "Théorème"
    **On considère une fonction dérivable sur un intervalle.**
     
     * Si la fonction **dérivée est positive** sur cet intervalle, alors la **fonction est croissante** sur cet intervalle.
     * Si la fonction **dérivée est négative** sur cet intervalle, alors la **fonction est décroissante** sur cet intervalle.
     * Si la fonction dérivée s'annule et change de signe sur cet intervalle, alors la fonction admet un minimum ou un maximum local sur cet intervalle.

### Coefficient directeur d'une droite : 

$$m=\frac{dy}{dx}$$
  
  Dans cette notation, le $d$ signifie **"_différence_"**.
  
  Le coefficient directeur de la droite (AB) est égal à : 
  
$$m=\frac{y_B-y_A}{x_B-x_A}$$

  > C'est à dire *"la différence entre les y des deux points divisée par la différence entre les x des deux points"*



## Partie 3 : Une étude simple pour comprendre

### $f(x)=x^2-4x+8$, pour $x \in [-1;5]$

1. Calcul de $f'(x)$
2. Étude du signe de $f'(x)$
3. Construction du tableau de variations de $f$.

### Correction

1. $f'(x)=2x-4+0$ soit $f'(x)=2x-4$

2. Commençons par résoudre $f'(x)=0$, c'est à dire $2x+4=0$ donc $2x=-4$ et $x=2$.</br> Pour $x \in [-1;5]$, on peut donc observer que $f'(x)<0$ si $x<2$ et que $f'(x)>0$ si $x>2$.</br> C'est à dire que pour $x \in [-1;2]$: $f'(x)$ est négative si $x \in [-1;2]$ et $f'(x)$ est positive si $x \in [-1;2]$

3. Le tableau de variation de $f$ va se déduire alors directement du tableau de signes de $f'$.

$\begin{array}{|l|lccccr|}
 \hline 
 x                      & -1    &          & 2    &             &  & 5   \\\hline 
 \text{signe de } f'(x) &       &    -     & 0     & +           &  &     \\\hline
                        & f(-1) &          &       &             &  & f(5) \\ 
 \text{variations de } f&       & \searrow &       & \nearrow &  &         \\
                        &       &          & f(2) &          &  &         \\\hline 
\end{array}$

Alors, pour être vraiment complet, il faut maintenant calculer les valeurs remarquables de ce tableau:

$f(-1)=(-1)^2-4 \times (-1) +8=1+4+8=13$

$f(2)=(2)^2-4 \times (2) +8=4-8+8=4$

$f(5)=(5)^2-4 \times (5) +8=25-20+8=13$

$\begin{array}{|l|lccccr|}
 \hline 
 x                      & -1    &          & 2    &             &  & 5   \\\hline 
 \text{signe de } f'(x) &       &    -     & 0     & +           &  &     \\\hline
                        & 13    &          &       &             &  & 13  \\ 
 \text{variations de } f&       & \searrow &       & \nearrow &  &         \\
                        &       &          & 4     &          &  &         \\\hline 
\end{array}$

Ce tableau est cohérent avec le graphique obtenu par Géogébra:

![geogebra_fonction_derive](geogebra_fonction_derive.png)


## Un peu plus technique : 

### $f(x)=x^3 + 1,5x^2 - 6x - 3$, pour $x \in [-3;2]$

1. Calcul de $f'(x)$
2. Étude du signe de $f'(x)$
3. Construction du tableau de variations de $f$.

### Correction

La première question n'a pas du vous poser de problème: $f'(x)=3x^2+3x-6$

Par contre, comment va t'on pouvoir étudier le signe de $f'(x)$ ? Sait-on résoudre $3x^2+3x-6=0$ ?

Et bien non... vous avez peut-être essayer d'identifier une identité remarquable? Mais ce n'en est pas une !

Il existe bien une méthode, mais elle n'est pas (n'est plus) à votre programme. Cette technique va permettre de **factoriser** une équation du second degré. Ainsi, dans les énoncés, on vous donnera toujours cette **forme factorisée** à vérifier.

La première question deviendra alors: 

* Calculer $f'(x)$ et vérifier que $f'(x)=3(x+2)(x-1)$

Comment répondre ? Et bien en développant et en **vérifiant** que l'on obtient bien ce qu'on a calculé pour $f'(x)$

$3(x+2)(x-1)=3(x^2-x+2x-2)=3(x^2+x-2)=3x^2+3x-6=f'(x)$. 

On a bien vérifié que $f'(x)=3(x+2)(x-1)$

* Et maintenant résoudre $f'(x)=0$ revient à résoudre$3(x+2)(x-1)=0$... et ça, vous savez faire!

Il est assez facile de voir que $f'(x)=0$ pour $x=-2$ ou bien pour $x=1$

Reste une question: comment obtenir le signe de $f'(x)$ ? Donc le signe de $3x^2+3x-6$... 

Souvenirs de seconde et première: le signe de $ax^2+bx+c$ suit la règle suivante 

$\begin{array}{|l|lcccccr|}
 \hline 
 x                      & -3    &          & -2    &             &1    &          & 2   \\\hline 
 \text{signe de } f'(x);a=3 &       &    signe(a):+     & 0     & signe(-a):-           &0    & signe(a):+        &     \\\hline
\end{array}$

* Et en conclusion, la question 3 devient très simple:

$\begin{array}{|l|lcccccr|}
 \hline 
 x                      & -3    &          & -2    &             &1    &          & 2   \\\hline 
 \text{signe de } f'(x) &       &    +     & 0     & -           &0    & +        &     \\\hline
                        &       &          & f(-2) &             &     &          & f(2)  \\ 
 \text{variations de } f&       & \nearrow &       & \searrow    &     & \nearrow &        \\
                        & f(-3) &          &       &             & f(1)&          &       \\\hline 
\end{array}$

Et il faut maintenant calculer les valeurs remarquables de $f(x)=x^3 + 1,5x^2 - 6x - 3$ dans ce tableau

$f(-3)=(-3)^3 + 1,5 \times (-3)^2 -6 \times (-3) - 3 =-27+13,5+18-3=1,5$

$f(-2)=(-2)^3 + 1,5 \times (-2)^2 -6 \times (-2) - 3 =-8+6+12-3=7$

$f(1)=(1)^3 + 1,5 \times (1)^2 -6 \times (1) - 3 =1+1,5-6-3=-6,5$

$f(2)=(2)^3 + 1,5 \times (2)^2 -6 \times (2) - 3 =8+6-12-3=-1$

$\begin{array}{|l|lcccccr|}
 \hline 
 x                      & -3    &          & -2    &             &1    &          & 2   \\\hline 
 \text{signe de } f'(x) &       &    +     & 0     & -           &0    & +        &     \\\hline
                        &       &          & 7 &             &     &          & -1  \\ 
 \text{variations de } f&       & \nearrow &       & \searrow    &     & \nearrow &        \\
                        & 1,5 &          &       &             & -6,5&          &       \\\hline 
\end{array}$

Ce tableau est cohérent avec le graphique obtenu par Géogébra:

![geogebra_fonction_derive2](geogebra_fonction_derive2.png)

## C'est à vous maintenant...
