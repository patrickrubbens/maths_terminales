# Documents de Terminales ST2S

## Les fiches d'exercices
  * [Exercices sur Pourcentages, Proportionnalité](Fiche_ExoPourcentage.pdf)
  * [Deux exercices sur le calcul de variations de taux global ou moyen](TST2S_exoTauxGlobalMoyen.pdf)
  * <a href="https://www.lelivrescolaire.fr/page/36730037" target="_blank" rel="noopener">le livre scolaire : les statistiques à 2 variables</a>
  * <a href="../TST2S_exoStat2Var.pdf" target="_blank" rel="noopener">Un exercice complet sur les statistiques à 2 variables</a>
  * [Fiche exercice derive.pdf](Fiche exercice derive.pdf)
  * [Exercices sur les nombres dérivés](Nbre derivees et tangentes.pdf)
  * [Un exercice type complet sur la fonction inverse](TST2S_exoFonctionInverse.pdf)
  * [Fiche_ProbasArbres](Fiche_ProbasArbres.pdf)
  * [Exercices-probabilites-conditionnelles.pdf](Exercices-probabilites-conditionnelles.pdf)
  
  
## Les évaluations
  * [TST2S_IE1.pdf](TST2S_IE1.pdf)
  * [TST2S_IE2.pdf](TST2S_IE2.pdf)
  * [TST2S_IE3.pdf](TST2S_IE3.pdf)
  * [TST2S_IE4.pdf](TST2S_IE4.pdf)
  * [TST2S_IE5.pdf](TST2S_IE5.pdf)
  * [TST2S_TD.pdf](TST2S_TD.pdf)
  * [TST2S_DM.pdf](TST2S_DM.pdf)
  * [DM3.pdf](DM3.pdf)
  * [ST2S_sept_2020_Metropole_DV.pdf](ST2S_sept_2020_Metropole_DV.pdf)
  <!-- * [1S1_d10_ie.docx](1S1_d10_ie.docx) 
  * [TST2S_IE1b.pdf](TST2S_IE1b.pdf) -->
  <!-- * []() -->
  <!-- * <a href="../fichier.pdf" target="_blank" rel="noopener">Desciption</a> -->

## Les B.O.
  * [BO_1ere_techno_annexe_1063530.pdf](BO_1ere_techno_annexe_1063530.pdf)
  * [BO_Term_techno_annexe_1158914.pdf](BO_Term_techno_annexe_1158914.pdf)
