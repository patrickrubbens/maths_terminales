

<!--
title : Page de nav
nav : 
    - "Chapitre 1 : Séries statistiques à deux variables" : Chapitre_1
    - "Chapitre 2 : Fonction inverse" : Chapitre_2
    - "Chapitre 3 : Suites arithmétiques" : Chapitre_3
    - "Chapitre 4 : Probabilités conditionnelles" : Chapitre_4
    - "Chapitre 5 : Fonctions exponentielles" : Chapitre_5
    - "Chapitre 6 : Variables aléatoires discrètes" : Chapitre_6
    - "Chapitre 7 : Suites géométriques" : Chapitre_7
    - "Chapitre 8 : Fonction logarithme décimal" : Chapitre_8 
    -->   
