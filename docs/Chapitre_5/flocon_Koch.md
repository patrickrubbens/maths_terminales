# Le Flocon de Von Koch

Le Flocon de von Koch se construit de manière **récurrente**.
Partant d'un triangle équilatéral, on divise chaque côté en trois et à partir de chaque segment "du milieu" on construit un triangle équilatéral de côté 1/3 en prenant le soin d'ôter le segment sur lequel on s'est basé. On **répète** cela **à chaque étape**.

![floconanime](floconanime2.gif)



## Ligne de base du Flocon de von Koch

![koch_principe.gif](koch_principe.gif)

Si on réitère ce principe sur chaque nouveau segment obtenu, on applique une **récurrence**. Ce genre de construction se fait assez facilement avec un algorithme qu'on appelle... récursif. [Algorithme récursif](#algorithme-récursif)

![Ligne_flocon_generation.jpg](Ligne_flocon_generation.jpg)

Supposons qu’à la génération 0, une ligne soit de mesure $1$. On considère qu'elle n'a qu'un seul "côté".

* À la génération 1, on voit clairement qu'on obtient $4$ côtés par la construction et la ligne est de mesure $\dfrac{4}{3}$

* À la génération 2, on obtient $4 \times 4$ côtés et la ligne est de mesure $\dfrac{4}{3} \times \dfrac{4}{3}$

* À la génération 3, on obtient ...

En notant $n$ le numéro de la génération et $(c_n)$ la suite du nombre de côtés, que peut-on dire de la nature de $(c_n)$ ?

Et en notant $(L_n)$ la mesure de la ligne ?

### Le Flocon par récurrence

![flocon_generations](flocon_generations.jpg)

## Algorithme récursif

```Python
from turtle import *

hideturtle()

def koch(l,n):
    if n==0:
        forward(l)
    else: # construction sur 1 côté
        koch(l/3,n-1) 
        left(60)
        koch(l/3,n-1)
        right(120)
        koch(l/3,n-1)
        left(60)
        koch(l/3,n-1)
    
def gen(n):
    up()
    goto(-400,350-200*n)
    down()
    koch(600,n)
    up()
    write(" Génération ", move=True,font=("Verdana",15, "normal"))
    write(n,font=("Verdana",15, "normal"))

gen(0)
gen(1)
gen(2)
gen(3)

done()

```





[https://aesculier.fr/fichiersMaple/koch/koch.html](https://aesculier.fr/fichiersMaple/koch/koch.html){target=_blank}

