# Quelques exemples d'applications des fractales

  Les fractales connaissent aujourd'hui diverses applications dans de nombreux domaines scientifiques et en plus de leur utilité indéniable, elles permettent d'ouvrir de nouvelles perpectives de recherches.

## Conception d'antennes fractales

![Schéma d'une antenne fractale](https://fr.wikipedia.org/wiki/Antenne_fractale#/media/Fichier:6452553_Vicsek_Fractal_Antenna.png)

Ces antennes particulières sont utilisées dans la télécommunication. Grâce à leur structure fractale, elles sont très petites et très performantes car elles peuvent recevoir une multitude de fréquences. En effet, elles possèdent une ergonomie remarquable avec un gain de place important, mais ont aussi la capacité d'augmenter considérablement le niveau de performance de l'antenne. Elles sont principalement utilisées pour des téléphones portables multi-bandes ou la RFID.

## Des aérogels avec une grande capacité d'isolation thermique

Des gels ultra-légers sont aujourd'hui créés avec une structure microscopique qui est fractale. Ces gels possèdent une très grande capacité d'isolation thermique: jusqu'à 100 fois supérieure à celui du verre que l'on retrouve par exemple dans la laine de verre.  


## Le dépistage de l'ostéoporose

L'ostéoporose est une maladie caractérisée par une fragilité excessive du squelette dûe à une diminution de la masse osseuse et des modifications de la micro-architecture des os. L'os est plus fragile, et par conséquent, le risque de fracture augmente.

Les dépistages liés à l'ostéoporose ont toujours été coûteux et douloureux. De plus, ils n'étaient pas totalement fiables. Mais grâce à l'étude des fractales, cela a changé. Avec la numérisation d'une radiographie du talon, les médecins parviennent aujourd'hui à construire une courbe fractale. Plus la dimension fractale de cette courbe est élevée plus l'os est poreux. Autrement dit, plus la courbe est irrégulière et agitée et plus l'os est fragilisé. Au-delà du diagnostic, cette nouvelle méthode permet de prévoir l'évolution du développement de la maladie au cours du temps.

![Comparaison d'images d'une vertèbre saine (à gauche) et d'une vertèbre poreuse (à droite)](http://img.over-blog-kiwi.com/1/93/65/56/20160103/ob_8186e8_osteoporosis-fig01-fr.jpg)

## Compréhension du fonctionnement de l'appareil respiratoire des mammifères

Le concept des fractales a permis de mieux comprendre le fonctionnement de l'appareil respiratoire des mammifères et de démontrer que les alvéoles plumonaires sont de structure fractale. Cette structure offre la meilleure surface possible pour assurer des échanges gazeux optimums. Nous pouvons observer dans les poumons l'auto-similarité des bronches, puis des bronchioles et enfin des alvéoles pulmonaires.

![Poumons de structure fractale](http://img.over-blog-kiwi.com/1/93/65/56/20160103/ob_acc5e7_sans-titre2.png)


## Perspective de despitage du cancer du sein

​En plus du dépistage de l'ostéoporose déjà utilisé, des chercheurs américains pensent pouvoir établir un dépistage du cancer du sein grâce aux fractales. Jusqu'ici, seule l'observation de la forme et des caractéristiques des cellules prélevées sur un tissu suspect permettaient d'établir un diagnostic. Mais celui-ci reposait sur une certaine subjectivité de la part du médecin. Or, ces chercheurs ont prouvé que la chromatine (substance composée de molécules d'ADN, d'ARN et de protéines, contenue dans le noyau des cellules) était de structure fractale faiblement irrégulière dans les tissus sains du sein. Après avoir évalué mathématiquement les dimensions particulières de la chromatine, ils ont remarqué que dans les cellules malignes la chromatine avait une forte irrégularité. Une étude a alors été faite sur 41 femmes: le diagnostic a été correct pour 95% des cas.  



[_Très largement inspiré du texte rédigé par Inès, Oriane, Ivana et publié depuis Overblog_](http://tpelesfractales.over-blog.com/)
