# Suites Géométriques

Si je vous donne la suite de nombre $1, 2, 4, 8, 16, 32$ est-ce que vous pouvez trouver le prochain nombre de la suite ?

Nous allons d’abord passer en revue quelques suites courantes, certaines d’entre elles étant introduites par des exemples de la vie réelle.

## Un dessin pour une suite ... géométrique !

![koch_principe.gif](koch_principe.gif)

Si on réitère ce principe sur chaque nouveau segment obtenu, on applique une **récurrence**. 

![Ligne_flocon_generation.jpg](Ligne_flocon_generation.jpg)

Supposons qu’à la génération 0, une ligne soit de mesure $1$. On considère qu'elle n'a qu'un seul "côté".

* À la génération 1, on voit clairement qu'on obtient $4$ côtés par la construction et la ligne est de mesure $\dfrac{4}{3}$

* À la génération 2, on obtient $4 \times 4$ côtés et la ligne est de mesure $\dfrac{4}{3} \times \dfrac{4}{3}$

* À la génération 3, on obtient ...

En notant $n$ le numéro de la génération et $(c_n)$ la suite du nombre de côtés, que peut-on dire de la nature de $(c_n)$ ?

Et en notant $(L_n)$ la mesure de la ligne ?

Si vous voulez en savoir plus sur cette figure qu'on appelle Flocon de von Koch et qui fait partie de la grande famille des Fractales : 
 [Flocon de von Koch](flocon_Koch.md) et à quoi peuvent servir ces fractales : [Exemples d'applications des fractales](Exemples_applications_ fractales.md)

## Définition

Une suite est **géométrique** esi chaque terme s'obtient **en multipliant** le terme précédent par une constante.

La constante, souvent notée $q$, est appelée la **raison** de la suite. 

Cette raison peut être positive, négative, fractionnaire, réelle, etc. 

Si la raison est négative, la suite est composée d’une succession de termes positifs et négatifs : un terme positif est intercalé entre deux négatifs, et réciproquement. 

Par exemple, on peut citer la suite $4, -8, 16, -32, 64, -128, 256,$ la raison est $q=... ?$

## Exemple Concret

Vous possédez tous un compte bancaire sur lequel vous épargnez votre argent (du moins, je vous le souhaite). 

Comme vous le savez, les comptes d’épargne sont rémunérés par des intérêts: vous touchez environ 2 à 3 % de la somme présente sur votre compte en intérêts, le pourcentage exact étant nommé le taux d’intérêt.

Si vous n’ajoutez ni ne retirez d’argent sur votre compte, la somme totale présente sur votre compte augmente chaque année.

Quelle est la somme que vous aurez sur ce compte dans plusieurs années, connaissant la somme actuelle?

Formalisons la situation: 

* soit $u_n$ la somme d’argent présente sur votre compte $n$ années après le dépôt de l’argent.
* $t$ le taux d'intérêt en $\%$.

Usuellement, on peut lire que le compte sera rénuméré à $+t\%$ chaque année.. et ainsi croire que la suite est une suite arithmétique...

mais rappelons que les intérêts ne sont pas une somme fixe: ils sont calculés chaque année sur la somme présente sur votre compte au début de la nouvelle année qui s'annonce. Donc pour un taux d’intérêt $t$, la somme sur votre compte est multipliée par $(1 + \frac{t}{100})$.

En posant $q=(1 + \dfrac{t}{100})$, on obtient: $u_{n+1} = u_n \times q$ 

## Définition par récurrence, pour n entier naturel.

$$u_0 \text{ , le terme initial}$$  

$$ u_{n+1} = u_n \times q$$

> Remarque : on note cette fois $q$ la raison, car elle correspond au quotient de deux termes successifs. En effet le quotient est le résultat d'une division.

## Exercices automatismes:

* $(u_n)$ et géométrique et on sait que $u_{12}=30$ et $u_{13}=45$. Calculer la raison $q$ de cette suite ?

* Avec la suite définie précédemment, calculer $u_{11}$

* Un nénuphar double de taille chaque jour. 21 jours plus tard il recouvre la moitié de l'étang. Quand aura-t-il recouvert l’étang en entier ?

![](Enigme-nénuphar-3-900x506.png)

* Si deux nénuphars étaient présents au départ, quand l’étang aurait été recouvert en entier ?

* “À chaque minute une bactérie donne naissance à une autre bactérie.” Au départ vous n’avez qu’une seule bactérie. A la fin de la première minute vous en obtenez 2, à la 2ème, 4… à la 10ème minute… 1024 !

![](Bacteries.gif)

## Un autre exemple : loyer pour une infirmière libérale.

Une infirmière libérale loue un local depuis le 1er janvier 2024 avec un bail de 20 ans.

Le montant annuel du loyer pour 2024 est de 7 200 euros, mais il est spécifié dans le bail qu'il augmentera chaque année de 2 %.

On note $u_n$ le loyer payé au 1er janvier de l'année $2024+n$.

* Expliquer pourquoi la suite $(u_n)$ est une suite géométrique. Quel est son premier terme $u_0$ ? Sa raison $q$?

* Calculer le loyer au 1er janvier de l'année $2025$, puis de l'année $2026$.

## Définition en fonction de $n$, ou formule explicite

Reprenons l'exemple du loyer pour le local de l'infirmière libérale. Comment obtenir directement le terme de rang 20 ? C'est à dire le loyer à payer en $2044$

Il suffit d’utiliser un raisonnement similaire à celui montré avec les suites arithmétiques : remplacez les additions par des multiplications, les séries de multiplications répétées par des puissances, et vous obtiendrez la formule suivante : 

$$u_n = u_0 \times q^n$$

![suitesgeometriques_illustrarion_rapport](suitesgeometriques_illustrarion_rapport.png){width=50%}

Sur cette illustration, on doit comprendre que contrairement aux suites arithmétiques, l'écart entre deux termes consécutifs n'est pas le même... c'est le rapport entre ces deux termes (_ratio_) qui est constant. Nous en reparlerons.

Vous pouvez observer sur les graphiques suivants le comportement et la croissance comparée d'une suite arithmétique de raison 3 et d'une suite géométrique de raison 1,25.

![Comparaison_suites_arithmétiques_et_suites_géométriques](Comparaison_suites_arithmétiques_et_suites_géométriques.png)

Dans le prochain chapitre, nous étudierons plus précisemment la croissance (ou décroissance) d'une suite géométrique... 

### Retour à l'exemple

Reprenons l'exemple du loyer pour le local de l'infirmière libérale. Supposons qu'à la place d'une augmentation de loyer annuelle à 2 %, on lui propose une augmentation constante de 160 euros. Quelle sera la meilleure décision à prendre ?

On garde $u_0=7200$ et $u_{n+1}=u_n*1.02$, mais on étudie en même temps $v_0=7200$ et $v_{n+1}=v_n+160$. La suite $(v_n)$ est donc une suite ... ?

Compléter le tableau : _on peut bien sûr utiliser la calculatrice_

|$u_n$|$v_n$|
|:--:|:--:|
|$u_ 0 =  7200.00$ | $v_ 0 = 7200$|
|$u_ 1 =  7344.00$ | $v_ 1 = 7360$|
|$u_ 2 =  7490.88$ | $v_ 2 = 7520$|
|$u_ 3 =  7640.70$ | $v_ 3 = 7680$|
|$u_ 4 =  7793.51$ | $v_ 4 = 7840$|
|$u_ 5 =  7949.38$ | $v_ 5 = 8000$|
|$u_ 6 =  8108.37$ | $v_ 6 = 8160$|
|$u_ 7 =  8270.54$ | $v_ 7 = 8320$|
|$u_ 8 =  8435.95$ | $v_ 8 = 8480$|
|$u_ 9 =  8604.67$ | $v_ 9 = 8640$|
|$u_{10} =  8776.76$ | $v_{10} = 8800$|
|$u_11 =  8952.30$ | $v_11 = 8960$|
|$u_12 =  9131.34$ | $v_12 = 9120$|
|$u_13 =  9313.97$ | $v_13 = 9280$|
|$u_14 =  9500.25$ | $v_14 = 9440$|
|$u_15 =  9690.25$ | $v_15 = 9600$|
|$u_16 =  9884.06$ | $v_16 = 9760$|
|$u_17 = 10081.74$ | $v_17 = 9920$|
|$u_18 = 10283.37$ | $v_18 = 10080$|
|$u_19 = 10489.04$ | $v_19 = 10240$|
|$u_20 = 10698.82$ | $v_20 = 10400$|

[Script console Python](https://console.basthon.fr/?script=eJwrVbBVMDcyMODlKgOySnm58oAUkMfLVZ6RmZOqoJFnY2SoacXLpQAEBUWZeSUaaUql8dV5Vka1QJXVpVYWekZptQqpJQplCNGyWiVNiBaQcXkK2gqGEG4pyBIFLQVDPQMjiAjI2jKQAjOwrQC7SCCD){target=_blank}

Alors ? Si vous êtes dans la situation de cette infirmière, que choisissez-vous ?

[Script console Python](https://console.basthon.fr/?script=eJwrVbBVMDcyMODlKgOySnm58oAUkFdQlJlXopGmVBpfnVcLFKourVVILVEog3HLapU0ebnKMzJzUhU0ShXsbBXKNK14uRSAAGRCnoK2giGEWwoyF8Q1M4AIgCwqU9BSMNQzMIKIEGcZzFFKukQBJU2Q0Wg-sQGbCTINIgkAxCA-JQ)



### Petit complément

Supposons que je ne connaisse pas le premier terme, mais que je connaisse le terme de position n et la raison: comment obtenir le terme de rang m?

Vu que $u_n = u_0 \times k^n$, et $u_m = u_0 \times k^m$, on a:

$$\dfrac{u_m}{u_n} = \dfrac{u_{0} \times k^{m}}{u_{0} \times k^{n}} = \dfrac{k^{m}}{k^{n}} = k^{m-n}$$

$$u_m = u_n \times k^{m-n}$$

## L'échiquier de Sissa

Le problème de l'échiquier de Sissa est célèbre dans l'histoire des mathématiques, car il a servi des siècles durant à démontrer la nature des progressions géométriques et parce qu'il constitue l'une des premières mentions du jeu d'échecs dans les énigmes.

### Sissa et l'échiquier du roi Shirham

L'érudit musulman Abu-l'Abbas Ahmad Ibn Khallikan (1211-1282) semble être, en 1256, le premier à débattre de l'histoire du grand vizir Sissa ben Dahir, auquel, selon la légende, le roi indien Shirham aurait demandé quelle récompense il souhaitait pour avoir inventé le jeu d'échecs. D'après la légende, Sissa aurait inventé le "chaturanga" pour distraire le prince Shiram de l'ennui, tout en lui démontrant la faiblesse d’un roi sans entourage. Souhaitant le remercier, le monarque propose au sage de choisir lui-même sa récompense.

Sissa répondit ainsi : « Majesté, je serais heureux si vous m'offriez un grain de blé que je placerais sur la première case de l'échiquier, deux grains pour la deuxième case, quatre grains pour la troisième, huit grains pour la quatrième, et ainsi de suite pour les soixante-quatre cases ».

« Et c'est tout ce que tu souhaites, Sissa, espèce d'idiot ? », hurla le roi abasourdi. 

![echiquier_sissa](echiquier_sissa.jpeg)

Mais le roi a-t-il eu raison de penser que cette demande était idiote parceque trop humble ?

En fait, le roi ne se rendait pas compte du nombre de grains de blé qu'il lui faudrait offrir à Sissa ! 

Calculons: il y a 64 cases sur un échiquier ($8 \times 8$). 

Donc, pour déterminer le nombre de grains de blé, il faut calculer la somme $1+2+4+8+16+32+...$ jusqu'à ???

Mais notons $u_n$ le nombre de grains sur la case $n$, il semble clair que $u_{n+1}=u_n \times 2$ et donc $(u_n)$ est une suite géométrique de raison $2$.

En notant que la case initiale correspondra à $n=0$ et en se souvenant que $2^0=1$. 

Alors on peut noter $u_0=2^0=1$ et plus généralement: $u_n=2^n$ pour n allant de $0$ à ... $63$, ce qui fait bien $64$ cases.

Ainsi, faisons la somme des 64 premiers termes de cette suite géométrique:

$u_{0} +u_{1} +u_{2} + u_{3} + ... +u_{63}=1+2+2^2+2^3+...+2^{63}$

$1+2+4+8=15$ ou $1+2+2^2+2^3=15$

$1+2+4+8+16=31$ ou $1+2+2^2+2^3+2^4=31$

$1+2+4+8+16+32=....$ ou $1+2+2^2+2^3+2^4+2^5=...$ Voyez-vous un "schéma" se mettre en place ?

$1+2+2^2+2^3+...+2^{63}=...=18446744073709551615$

Un petit [algorithme pour se convaincre console Basthon](https://console.basthon.fr/?script=eJwLVrBVMOTlygNSBrxcvFzlGZk5qQoaeTZmxppWvFwKQACSy1PQBikDcYOB3GAg10hLKw8iwstVUJSZV6IRrMnLBQBvkg90){target=_blank} :

```python
S = 1
n = 0

while (n<63):
    n = n + 1
    S = S + 2**n
    
print(S)
```

soit $18446744073709551615$ grains ! Soit plus de 18 milliards de milliards de grains de blé ! 

Est-ce beaucoup ? Le poids moyen d'un grain de blé est difficile à estimer mais il tournerait autour de 0,04 grammes. En agriculture, on travaille beaucoup avec le poids de 1.000 grains. Ici, c'est 40 grammes – la Food and Agriculture Organisation (FAO) donne une fourchette de 35 à 50 grammes). Et 0,04 g peut s'écrire $4.10^{-2}$ g et donc $4.10^{-2}.10^{-6}$ tonnes.

18 milliards de milliards de grains, que nous écrivons $18.10^{18}$, péseraient donc $18.10^{18}.4.10^{-2}.10^{-6}=72.10^{10}$ tonnes ou encore $720000$ millions de tonnes.

L'International Grain Council (IGC), qui regroupe les principaux pays importateurs et exportateurs de la planète, tablait en août sur une production mondiale de blé de $784$ millions de tonnes en 2023-24... donc il faudrait 1000 ans au rythme de cette production modiale pour satisfaire la demande de Sissa.

De son côté, Jan Gullberg, écrit dans _[Mathematics From The Birth Of Numbers](https://www.goodreads.com/book/show/383087.Mathematics_From_The_Birth_Of_Numbers)_ : « Avec près de 100 grains par centimètre cube, le volume total des grains aurait représenté environ 200 kilomètres cubes, dont le chargement aurait nécessité 2.000 millions de wagons, soit un train égal à 1.000 fois la circonférence de la Terre ».


## Somme des termes d'une suite géométrique.

Bon, revenons-en à nos suites géométriques. On a compris _j'espère_ que $1+2+2^2+2^3+...+2^{n}=2^{n+1}-1$

Qu'en est-il de $1+3+3^2+3^3+...+3^{n}$ ??? $=3^{n+1}-1$ ???

Essayons: 

$1+3+3^2=1+3+9=13$ Mais $3^3-1=27-1=26$, donc la formule est fausse ici. Continuons quand même pour essayer de voir si il y a un lien:

$1+3+3^2+3^3=1+3+9+27=40$ Mais $3^4-1=81-1=80$. Vous l'avez ? Non ? Bon, on continue alors...

$1+3+3^2+3^3+3^4=1+3+9+27+81=121$ Mais $3^5-1=243-1=242$ ... Alors ?

Pour aller plus loin dans la somme : [script python - console basthon](https://console.basthon.fr/?script=eJwLVrBVMOTlygNSBrxc5RmZOakKGnk25ppWvFwKQACSyFPQBqkBcYOB3GAg11hLKw8iUlCUmVeikaYUkF9apJBnW51Xq2AFVlYdbGVSq5BaomAcp1Gdp21Yq6lrCBRW0gHq1QDygVxNXi4AGMIdCg){target=_blank}

A votre avis : $1+3+3^2+3^3+...+3^{n} =$ ??? 

Et $1+4+4^2+4^3+...+4^{n} =$ ???

Essayons de voir un peu ce qui se passe. Et revenons une fois encore à $S=1+2+2^2+2^3$

Alors $2 \times S = 2 \times (1+2+2^2+2^3) = 2+2^2+2^3+2^4$ en distibuant. Ainsi, $2S-S=(2+2^2+2^3+2^4)-(1+2+2^2+2^3)$

D'un côté, $2S-S=S$ et de l'autre, les termes se simplifient les uns à la suites des autres, sauf $2^4-1$

Et on a bien le résultat qu'on avait deviné: $S=2^4-1$. Il n'est pas très difficile de généraliser...

Maintenant, si $S=1+3+3^2+3^3$, alors $3 \times S = 3 \times (1+3+3^2+3^3) = 3+3^2+3^3+3^4$

Et $3S-S=2S$ d'un côté, et de l'autre, à nouveau les termes se simplifient pour ne laisser que $3^4-1$

Donc cette fois: $2S = 3^4-1$ et donc $S=\dfrac{3^4-1}{2}$

Allez, généralisons pour de bon et prenons un nombre $q$ quelconque comme raison de notre suite:

$S=1+q+q^2+q^3$ Alors $q \times S = q \times (1+q+q^2+q^3)=q+q^2+q^3+q^4$

Donc $qS-S=(q+q^2+q^3+q^4)-(1+q+q^2+q^3)$

Or $qS-S=(q-1)S$ d'un côté et les termes se simplifient pour ne laisser que $q^4-1$

Et finalement $(q-1)S=q^4-1$ et donc $S=\dfrac{q^4-1}{q-1}$

Évidemment, on peut reproduire le même raisonnement pour $1+q+q^2+q^3+q^4=\dfrac{q^5-1}{q-1}$ ou $1+q+q^2+q^3+q^4+q^5=\dfrac{q^6-1}{q-1}$

Il est assez clair dès lors que:

$$ 1+q+q^2+q^3+...+q^n = \dfrac{q^{n+1}-1}{q-1}$$

Et enfin, on peut généraliser à toute suite géométrique. En effet, si $(u_n)$ est géométrique de raison $q$ et de terme initial $u_0$, alors $u_n=u_0 \times q^n$

Donc $u_{0} +u_{1} +u_{2} + u_{3} + ... +u_{n}=u_{0}+u_{0} \times q + u_0 \times q^2 + u_0 \times q^3 +...+u_0 \times q^n$

$u_{0} +u_{1} +u_{2} + u_{3} + ... +u_{n}=u_{0} \times (1+q+q^2+q^3+...+q^n)$

Et enfin:

$$u_{0} +u_{1} +u_{2} + u_{3} + ... +u_{n}=u_{0} \times \dfrac{q^{n+1}-1}{q-1}$$

## Retour à l'exemple du loyer

On rappelle que le loyer initial de $7200$ euros de notre infirmière augmente chaque année de 2 %, pendant $20$ ans.

On a noté $u_n$ le loyer payé au 1er janvier de l'année $2024+n$.

* On a calculé $u_{20}=7200 \times 1,02^{20}=10698,82$ et que le loyer en $2044$ sera donc de $10699$ euros arrondi à l'euro près.

* Mais pendant ces 20 années, combien lui aura donc coûté au total ce loyer ? 
  
  Chaque année $n$, elle verse $u_n$. Donc en $20$ ans, elle aura versé :

  $S=u_0 + u_1 + u_2 + u_3 + ... + u_{20}$

  On calcule cette somme avec la formule : $S=u_{0} \times \dfrac{q^{n+1}-1}{q-1}$ ... c'est à dire ?

  Finir alors ce calcul...

* L'[algorithme Python](https://console.basthon.fr/?script=eJxljcEKwjAQRO-F_MOSU5JWTXIRgh78AyF3QSXFQt1IafVQ-u9uEkHBvewO82bHwx62VmtWIV20XreuDyBwZ7V0rAKa5GBtivAkPNQ5pMxaW6WwOI-hw1G0_BinIWdmdHYBlyOzX4DLArKqoPJX8MMzXKE_QxuH-9SHBiJCvIxdwNGl5AfLvSIVn4TVtZErIzdZ00FFvPkSSv0j9OkNwkM3gQ) nous donne : $S = 185639.88380325172 $. _Vérifier le résultat précédent._

 Soit une somme totale de $185640$ euros à l'euro près.


## Mais au fait, pourquoi ce nom ? Que vient faire la géométrie ici ?

L’expression « géométrique » provient plutôt de la **moyenne géométrique** qui s’obtient par une construction… géométrique.

La question est de "carrer" un rectangle: 

Est-il possible de **construire** un carré de même aire qu'un rectangle donné? [geogebra](https://www.geogebra.org/classic/cruzkukf)

![carre_rectangle](carre_rectangle.png){width=50%}

Il est clair que l'aire du rectangle est $a \times b$, et donc que le carré doit avoir pour côté $\sqrt{a \times b}$

C'est de là que provient donc la notion de moyenne géométrique. 

Ainsi, rappelez-vous, lorque que je vous demandais **le taux moyen** après deux évolutions successives. 

Par exemple, $+20\%$ puis $+30\%$... qui ne donne surtout pas ... 

Il peut être tentant de faire la moyenne arithmétique de ces deux taux d'évolutions. Mais c'est une erreur! 

Pour s'en convaincre, nous allons commettre cette vilaine erreur : $\dfrac{20+30}{2}=\dfrac{50}{2}=25$, et conluons gaiement : _" le taux moyen est de $25\%$ "_

Maintenant, imaginons une population de $1000$ bactéries qui augmente de $+20\%$ sur une heure, puis $+30\%$ l'heure suivante.

Cette population est donc multipliée par $1,20$ la première heure puis $1,30$ la suivante. Ceci revient-il à multiplier successivement par $1,25$ ? Ce qui correspondrait à une variation constante de $+25\%$

$1000 \times 1,20 \times 1,30 = 1560$ alors que $1000 \times 1,25 \times 1,25 = 1562,5$ ... et donc problème !

Ce que l'on aurait voulu : $1000 \times x \times x = 1000 \times 1,20 \times 1,30$, et on vient de voir que $x \neq 1,25$

Mais nous savons résoudre $x^2 = 1,20 \times 1,30$ ! C'est $x=\sqrt{1,20 \times 1,30}$, la moyenne géométrique de $1,20$ et $1,30$

Donc $x \approx 1,2489$ ce qui correspond finalement à une augmentation de $+24,89\%$ !

Mmmmmh, pas grande différence me direz-vous ? Voyons ce qui se passe au bout de $24$ heures.

Si je considère que ma population augmente en moyenne de $+25\%$ pendant $24$ heures, alors je la multiplie à $24$ reprises par $1,25$

$1000 \times 1,25^{24} \approx 211 758,23$.

Mais, si l' augmentation moyenne est de $+24,89\%$, on aura $1000 \times 1,2489^{24} \approx 207330,87$

Donc une différence de $211758-207331 = 4427$ bactéries sur la journée... ce qui n'est pas du tout négligeable.

## Triangle de Kepler

Un triangle de Kepler est un triangle rectangle dont les carrés des longueurs des côtés sont en progression géométrique selon la raison du [nombre d'or](https://fr.wikipedia.org/wiki/Nombre_d%27or){target=_blank}.

[Triangle de Kepler](https://fr.wikipedia.org/wiki/Triangle_de_Kepler){target=_blank}

[Phiramide](https://compterlebeau.weebly.com/geometrie.html){target=_blank}

[Infinite Kepler Triangles](https://publications.azimpremjiuniversity.edu.in/2798/1/14_Infinite%20Kepler%20Triangles.pdf){target=_blank}


