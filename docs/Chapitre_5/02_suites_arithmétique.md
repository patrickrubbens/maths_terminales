# Suites Arithmétiques
## Une situation géométrique
Au collège, vous avez peut-être déjà étudié cette fiche: (source :[Eduscol](http://cache.media.education.gouv.fr/file/Calcul_litteral/29/8/RA16_C4_MATH_nombres_calcul_calcul_litteral_initiative_carre_bordes_548298.pdf))

![carres_bordes.jpg](carres_bordes.jpg)

## Un exemple concret

En 2017 est une entreprise de fournitures agricoles a vendu $24000$ sacs d'un herbicide amené à disparaitre pour des raisons écologiques et de santé publique. [informations sur le glyphosate](https://agriculture.gouv.fr/quest-ce-que-le-glyphosate)

En effet, il sera interdit de l'utiliser à partir de 2033. [Glyphosate dans l'UE : vers une autorisation renouvelée de l'herbicide jusqu'en 2033](https://www.vie-publique.fr/questions-reponses/291363-glyphosate-une-autorisation-renouvelee-dans-lue-jusquen-2033)

Ainsi, l'entreprise décide d'anticiper et de diminuer les ventes de 400 sacs chaque année pour écouler ses stocks progressivement et de stopper la vente en 2025.

|Années      |2017    |2018 |2019  |2020 |2021  |2022    |2023    |2024    |2025  |TOTAL|
|:----------:|:------:|:---:|:----:|:---:|:----:|:------:|:------:|:------:|:----:|:---:|
|Sacs vendus |$24000$ |     |      |     |      |        |        |        |      |     |


Combien de sacs l'entreprise va t'elle vendre en 2025 ?

Combien de sacs l'entreprise aura-t-elle vendu entre 2017 et 2025 ?

Je peux "tricher" et utiliser un tableur : [lien vers le tableur](https://lite.framacalc.org/26dyuj1hgc-a5f1){:target=_blank}

Mais quelle formule a-t-on alors tapé dans les différentes cellules ?

<!--Une association comptait 832 adhérents en 2022 et a 40 nouveaux adhérents en 2023.

On espère que les années à venir connaitront le même succés, c'est à dire que l'association recrutera 40 nouveaux adhérents chaque année.

Combien d'adhérents cette association aurait-elle alors en 2025 ?

Au bout de combien d'années pourra t'elle avoir plus de 1200 adhérents ?-->

## Définition

Une suite est **arithmétique** si chaque terme s'obtient en ajoutant un nombre $r$ constant au terme précédent.

!!! cours "Par récurrence"

    Ce nombre r est appelé la **raison**. Pour tout entier naturel $n$: 

     * $u_0$ fixé,
     * $u_{n+1}= u_n + r$ 

!!! note "Retour à l'exemple"

    En formalisant un peu notre exemple. Si on note $u_0$ le nombre de sacs vendus en 2017, alors on peut écrire que $u_0 = 24000$.    
    C'est le terme de **rang** $0$ 

    $u_1$, le terme de **rang** $1$ sera le nombre de sacs vendus en 2018, donc $u_1=...$ ? et ainsi de **suite**

    $u_2=$ .... le nombre de sacs vendus en ....

    On peut donc écrire que :$u_0=24000$ fixé, et $u_{n+1}= u_n +$ .....

    La suite $u_n$ est une suite arithmétique de raison ... ?


 Compléter ce tableau en utilisant le menu suite de la calculatrice :

|Rang $n$      |0      |1    |2     |3       |4       |5       |6       |7        |8     |TOTAL|
|:----------:  |:---:  |:---:|:----:|:------:|:------:|:------:|:------:|:------: |:----:|:---:|
|$u_n$         |$24000$|.....|..... |.....   |.....   | .....  |.....   | .....   |..... |.....|

![imageNumworks](Numw_screenshot_suitesArith.png){width=30%} 

<!-- > Exemple. La suite des nombres pairs est une suite arithmétique de raison ... ? -->

Si vous ne vous souvenez plus comment utilser la calculatrice, retournez [ici: utilisation de la calculatrice](01_suites_generalites.md#utilisation-de-la-calcultrice)

On peut aussi faire appel au langage Python et utiliser une boucle: [Exemple :](https://console.basthon.fr/?script=eJwrVbBVMDIxMDDg5Sooyswr0UhTKo2vNqi1rS6tVdLk5eLlSssvUshTyMxTKErMS0_VsNC04uVSAIJSoM5SBV0FE5BekACS_jxtQ7gJICleLgChKhpk){target=_blank}

En cliquant sur le lien : [console Python](https://console.basthon.fr/?script=eJwrVbBVMDIxMDDg5Sooyswr0UhTKo2vNqi1rS6tVdLk5eLlSssvUshTyMxTKErMS0_VsNC04uVSAIJSoM5SBV0FE5BekACS_jxtQ7gJICleLgChKhpk){target=_blank}, vous aurez le code suivant dans une console Python, que vous pourrez exécuter.

```python
u = 24000                     # on initialise la suite (terme initial)
print(f"u_{0}={u}")           # on affiche u_0

for n in range(8):            # n va augmenter de 1 en 1, entre 0 et 7.
    u = u - 400               # à chaque tour de boucle, on soustrait 400 à la valeur précédente de u
    print(f"u_{n+1}={u}")     # on affiche la valeur obtenue
```

Il suffit de comprendre que la fonction ``` range() ``` permet de générer une liste d'entiers.

L'appel de fonction ```range(8)``` renvoie la liste des entiers de $0$ inclus à $8$ **exclu**, donc il y a bien $8$ valeurs:

$n=0 ; n=1 ; n=2 ; n=3 ; n=4 ; n=5 ; n=6 ; n=7$ donc ```n dans [0,1,2,3,4,5,6,7]:``` 

Ainsi ```for n in range(8):``` permet de générer une **boucle**, c'est à dire une suite de calcul **récurrents** (qui "reviennent")

On pourrait de manière équivalente écrire ```for n in [0,1,2,3,4,5,6,7]:``` mais c'est évidemment bien plus long...

La fonction ``` print() ``` permet d'afficher les valeurs que l'on souhaite. 

Remarquez bien qu'on affiche $u_{n+1}=u_n-400$ à la fin de chaque tour de boucle.


## Expression du terme général en fonction de n

Si $(u_n)$ est une suite **arithmétique** de terme initial $u_0$ et de raison $r$, alors:

!!! cours "Pour tout entier naturel $n$: "
    
    $u_n = u_0 + n \times r$

> Dans notre exemple, le nombre de sacs vendus en $2017+n$ sera $u_n = u_0 + n \times r = 24000 - n \times 400$ 

  Et donc, comme $2017+8=2025$, en $2025$, $n=8$. Ainsi $u_8=$... 
  
<!-- Exemple. Le terme général de la suite des nombres pairs s'exprime ainsi : $P_n = 0 + n × 2$ -->

## Variations

Une suite arithmétique $(u_n)$ de raison $r$ est:

 * strictement croissante si $r > 0$
 * strictement décroissante si $r < 0$
 * constante si $r = 0$

> Exemple: La suite $u_n$ des sacs vendus est strictement décroissante. $r=-400$

## La somme de Gauss:
### ou Somme des n premiers entiers naturels.

![Johann Karl Friedrich Gauss](https://mathshistory.st-andrews.ac.uk/Biographies/Gauss/thumbnail.jpg)

La somme de Gauss est nommée en l’honneur de [Johann Karl Friedrich Gauss](https://mathshistory.st-andrews.ac.uk/Biographies/Gauss/)(1777 ; 1855).
<!-- (https://www.maths-et-tiques.fr/index.php/histoire-des-maths/mathematiciens-celebres/gauss) -->

C’était un mathématicien allemand. Gauss est l’un des penseurs mathématiques les plus influents de l’histoire.

Une anecdote (légende?) raconte que l' enseignant de mathématiques aurait demandé à la classe de Gauss, alors un très jeune (entre 7 et 10 ans suivant les sources), d’additionner les nombres entiers successifs de 1 à 100. En d’autres mots, l’enseignant souhaitait qu’ils additionnent $1 + 2 + 3 + 4 + · · · + 100$

L’enseignant a présumé que cela allait exiger beaucoup de temps de la part des élèves. 
Pourtant, Gauss a répondu 5 050 presque immédiatement. 

![etonnant](garçon_étonné.png){width=15%}

Réfléchissons un peu au modèle que Gauss a utilisé pour résoudre le problème aussi rapidement. 

Le "truc" employé par Gauss est que l’ordre dans lequel nous additionnons les nombres n’a pas d’importance. Peu importe l’ordre suivi, nous obtiendrons le même résultat. 

Il est possible de réorganiser les nombres de 1 à 100 d’une manière intelligente.

Observons $1 + 2 + 3 + 4 + 5 + 6$  = $(1+6) + (2+5) + (3+4)$ c'est à dire que l'on regroupe le premier terme et le dernier, le deuxième et l'avant dernier,... et il devient évident que $1 + 2 + 3 + 4 + 5 + 6$ = $3 \times 7$

Saurez-vous retrouver la logique suivie par le jeune prodige ? On cherche à calculer:

$$1 + 2 + 3 + 4 + · · · · · ·+97+98+ 99+ 100$$

!!! cours "Somme des n premiers entiers naturels"
    
    $$1 + 2 + 3 + · · · + n = \frac{n(n+1)}{2}$$

 > Une approche différente est possible: voir [ici](#une-approche-geometrique-de-la-somme-de-gauss)
  
 > Un petit exercice de mise en pratique : Lors une réunion vingt-cinq personnes sont présentes et elles se sont toutes serrées la main pour se saluer. Donc combien de poignées de mains ont été échangées? 


## Somme des termes d'une suite arithmétique

!!! cours "Soit $(u_n)$ une suite **arithmétique** de raison $r$."
     
     $$u_0 + u_1 + u_2 + u_3 + · · · + u_n = \dfrac{(u_0+u_n) \times (n+1) }{2} $$

> On peut retenir ce résultat ainsi $\text{Somme} = \dfrac{(\text{terme initial} + \text{terme final}) \times (\text{nombre de termes})}{2}$

### Démonstration:

Tout d'abord, donons un nom à notre somme... appelons la $S_n$. Donc $S_n=u_0 + u_1 + u_2 + u_3 + · · · + u_n$ 

$$S_n=u_0 + u_1 + u_2 + u_3 + · · · + u_{n-3} + u_{n-2}+ u_{n-1} + u_n$$

$$S_n=u_n + u_{n-1} + u_{n-2} + u_{n-3} + · · · + u_3 + u_2+ u_{1} + u_0$$

On additionne $S_n$ dans un sens et dans l'autre... on fait donc des "paquets de 2 termes symétriques" comme Gauss.

$$2 \times S_n = (u_0 + u_n) + (u_1 + u_{n-1}) + (u_2 + u_{n-2}) + (u_3 + u_{n-3}) + · · · + (u_n + u_0)$$

On va ainsi obtenir $n+1$ "paquets". 

Intéressons nous au "paquet" $(u_1 + u_{n-1})$ : 

$u_1=u_0 + r$ et $u_{n-1}=u_n - r$ et donc $(u_1 + u_{n-1})=u_0 + r + u_n - r$

Il est alors facile de voir que: $(u_1 + u_{n-1}) = u_0 + u_n$

Et il en sera de même pour les $n+1$ paquets symétriques. Ce qui permet d'écrire:

$$2 \times S_n = (u_0 + u_n) \times (n+1)$$ 

qui donne bien le résultat à démontrer en divisant par $2$ : $S_n=\dfrac{(n + 1) \times (u_0 + u_n)}{2}$

> Remarque: on peut procéder de manière plus "directe" en utilisant la somme de Gauss
  $u_0 + u_1 + u_2 + u_3 + · · · + u_n = u_0 + (u_0 + r) + (u_0 + 2r) + (u_0 + 3r) + · · · + (u_0 + nr)=$ </br>
  $=u_0(n + 1) + 1r + 2r + 3r + · · · + nr=$</br>
  $=u_0(n + 1) + r(1 + 2 + 3 + · · · + n)=$</br>
  $=u_0(n + 1) + r \times \dfrac{n(n+1)}{2}=$          #_on met au même dénominateur_</br>
  $=\dfrac{2u_0(n + 1) + r \times n(n+1)}{2}=$         #_$(n+1)$ est un facteur commun_</br>
  $=\dfrac{(n + 1) \times (2u_0 + r \times n)}{2}=$    #_on a mis $(n+1)$ en facteur_</br>
  $=\dfrac{(n + 1) \times (u_0 + u_0 + r \times n)}{2}=$ #_on sait que $u_n=u_0 + r \times n$_</br> 
  et donc on peut conclure:
  $=\dfrac{(n + 1) \times (u_0 + u_n)}{2}$</br>

-------------------------------------------------

!!! note "Retour à l'exemple"
   
    Combien de sacs l'entreprise aura-t-elle vendu entre 2017 et 2025 ?

    $$u_0 + u_1 + u_2 + u_3 + u_4 + u_5 + u_6 + u_7 + u_8 = ?? $$

------------------------------------------------

On peut aussi écrire un script Python en s'inspirant de ce qu'on a fait précédemment:

```python
u = 24000                     # on initialise la suite (terme initial)
print(f"u_{0}={u}")           # on affiche u_0
S = u                         # on initialise la somme S

for n in range(8):            # n va augmenter de 1 en 1, entre 0 et 7.
    u = u - 400               # à chaque tour de boucle, on soustrait 400 à la valeur précédente de u
    print(f"u_{n+1}={u}")     # on affiche la valeur obtenue
    S = S + u                 # on ajoute u à la somme précédente

print(f"Somme={S}")           # on sort de la boucle et on affiche la valeur de la somme finale
```


----------------------------------------------------------------

## Une approche géométrique de la somme de Gauss. 
### Mais toujours symétrique:

□

□□

□□□

□□□□

□□□□□

□□□□□□

 
On considère une suite de □ qui augmente de 1 unité. Chercher à calculer la somme $1 + 2 + 3 + 4 +5 + 6$, c'est la même chose que chercher à calculer le nombre de □. Pour une petite somme comme celle-ci, il est facile de vérifier qu'elle correspond au dessin de $21$ carrés.

Mais si on représente la somme symétrique:

□□□□□□

□□□□□

□□□□

□□□

□□

□

En "additionnant" les deux sommes:

|Somme| +  |Somme | = |$2 \times$ Somme|
|-----|:--:|------|:-:|:--------------:|
|□    |  + |□□□□□□| = |□□□□□□□|
|□□   |  + |□□□□□ | = |□□□□□□□|
|□□□  |  + |□□□□  | = |□□□□□□□|
|□□□□ |  + |□□□   | = |□□□□□□□|
|□□□□□|  + |□□    | = |□□□□□□□|
|□□□□□□| + |□     | = |□□□□□□□|

On construit ainsi un rectangle de largeur $7$ carrés et longueur $6$ carrés, donc on obtient $7 \times 6$ carrés

Comme on a doublé le nombre de carrés, il faut diviser par $2$ pour retrouver notre somme de Gauss:

$\frac{7 \times 6}{2}=\frac{42}{2}=21$. Il y a bien $21$ □ 

Pour $1+2+3+4+...+(n-3)+(n-2)+(n-1)+n$, il est facile de généraliser ce raisonnement en **voyant** que la largeur du rectangle est augmentée de $1$ □

$S=\frac{(n+1) \times n}{2}$

 <!-- 
 caractère UTF-8 : □  ; \0025A1 ; &#9633 
 caractère UTF-8 : ◺ ; \0025FA;  &#9722; &#x25FA; 
 caractère UTF-8 : ◹ ; \0025F9 &#x25F9 
-->

## Petit clin d'oeil de M. Verdier - Inspecteur de Maths

### Bonne Année 2024 : Après les carrés, une somme de cubes ... pour la suite d'une année !

![Bonne Année 2024](voeux_2024.jpg)

