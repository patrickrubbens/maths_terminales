# Généralités sur les suites numériques
## Introduction

  * Exemple 1 : 
	Si vous travaillez chaque mois, vous recevez un salaire : un nombre

	|Janvier |Février |Mars |Avril |Mai |Juin |Juillet |Août |Septembre |Octobre |Novembre |Décembre|
	|:------:|:------:|:---:|:------: |:------: |:------: |:------: |:------: |:------: |:------: |:------: |:------:|
	|salaire 1 |salaire 2 |salaire 3 |salaire 4 |salaire 5 |salaire 6 |salaire 7 |salaire 8 |salaire 9 |salaire 10 |salaire 11 |salaire 12|
	
	On a obtenu une suite de nombres : salaire 1, salaire 2, salaire 3, . . .

  * Exemple 2 (Si pas d'autre bonne idée dans la classe). 

	À chaque anniversaire, vous vous mesurez (en cm). 

	$u_0=$ [taille à la naissance], $u_1=$ [taille à un an], etc...

  * Remarques:
    * Pour une suite quelconque, on ne peut pas deviner $u_2$ en connaissant $u_1$.
    * Une suite ne s'arrête jamais : elle est infininie (_on ne meurt jamais alors ?_).

## Définitions
  1. Définition. 

    Si, à chaque entier naturel n ≥ 0, on associe un nombre réel $u_n$, on dit que l'ensemble (ordonné) des nombres $u_n$ forme la suite de terme général $u_n$, que l'on note $(u_n)$.

  2. Rang.

    L'entier naturel $n$ associé au terme $u_n$ est appelé **rang** de $u$.

    > exemple: $u_9$ est le terme de rang 9

  3. Remarques.

    * Les indices sont en ... indice, bien en bas. C'est important de s'appliquer à bien écrire pour éviter les confusions !

    > si $u_9=1847$ et $u_{10}=1900$, on peut écrire que $u_{9+1}=u_{10}=1900$ mais $u_9 + 1=1847+1=1848$ et il est clair que $u_{10}=u_{9+1} \neq u_9 + 1$

    * On s'intéressera plus particulièrement aux suites dont les termes ont des liens logiques entre eux.

  4. Par récurrence.
    * Des exemples:
      * $P_0=0$ ; $P_1=2$ ; $P_2=4$ ; $P_3=6$ ; $P_4=8$ ; $P_5=10$ ; $P_6=... ?$ 
      * $I_0=1$ ; $I_1=3$ ; $I_2=5$ ; $I_3=7$ ; $I_4=9$ ; $I_5=11$ ; $I_6=... ?$
      * $u_0=0.5$ ; $u_1=0.7$ ; $u_2=0.9$ ; $u_3=0.11$ ; $u_4=0.13$ ; $u_5=0.15$ ; $u_6=... ?$
      * plus généralement : $P_{n+1}=... ?$ ; $I_{n+1}=... ?$ ; $u_{n+1}=... ?$

    * Les suites précédentes sont alors définies par une **formule de récurrence**. 
      
      Attention, il faut bien définir le terme initial !
      
      Ainsi on peut **calculer** les termes successifs.

## Utilisation de la calcultrice

On va utiliser un exemple de suite définie par récurrence à l'aide de la fonction affine $f(x)=x \times 1,02 + 500$

Supposons concrétement qu'on a déposé $1000$€ à la banque, pour un placement annuel d'intérêts composés à $2 \%$ et que l'on s'engage chaque année à verser $500$€ supplémentaires.

En notant $C_n$ le capital obtenu au bout de $n$ années, et $C_0$ le capital initial. 

Quel capital aura-t-on au bout de 1 an ? de 2 ans ? de 3 ans ?..... de 20 ans ?

On demande donc de calculer $C_1$ , $C_2$ , $C_3$ , ...., $C_{20}$

Allons-y : $C_0=1000$ augmenter de $2 \%$ revient à $\times 1,02$, puis on rajoute $500$

$C_1=C_0 \times 1,02 + 500$, ou encore $C_1=f(C_0)$ ce qui donne:  $C_1= 1000 \times 1,02 + 500=1520$

De même, $C_2=C_1 \times 1,02 + 500$, ou encore $C_2=f(C_1)$ donc $C_2= 1520 \times 1,02 + 500=2050,4$

Puis $C_3=C_2 \times 1,02 + 500$ et donc $C_3= 2050,4 \times 1,02 + 500=2591.408$

**pffff, tout celà est bien répétitif!!! Demandons donc à notre machine de le faire!**

Ici, le formalisme prend tout son sens: il faut savoir parler à sa machine ! Comment est définie $C_n$ ? Par récurrence ...

$C_0=1000$ et $C_{n+1}=C_n \times 1,02 + 500$ 

On va se diriger dans le menu `suites` de notre calculatrice:

![Numw_screenshot_suites01](Numw_screenshot_suites01.png)

et évidemment ajouter une suite ... pas le choix pour la nommer, elle devra s'appeler $u$, à nous de faire attention pour adapter!

![Numw_screenshot_suites01](Numw_screenshot_suites02.png)

On a définit notre suite par récurrence (_c'est une récurrence d'ordre 1_)

![Numw_screenshot_suites01](Numw_screenshot_suites03.png)

donc en remplaçant $C$ par $u$, on entre:

![Numw_screenshot_suites01](Numw_screenshot_suites04.png)

Et en se dirigeant vers le tableau :

![Numw_screenshot_suites01](Numw_screenshot_suites06.png)

Et voilà, vous avez accès à toutes les valeurs de $C_n$ pour tout entier naturel $n$

## Représentation graphique
Il n'y a que des points **isolés**, car $u_n$ n'existe que pour n entier (n = 0, n = 1, n = 2, ...).

![Numw_screenshot_suites01](Numw_screenshot_suites05.png)


## Par une formule explicite, c'est à dire en fonction de $n$

C'est une formule qui permet de calculer directement un terme sans avoir besoin de calculer les précédents. 

C'est le plus souvent très difficile de trouver une telle formule, sauf dans deux cas très simples (qui sont au programme): les suites arithmétiques et les suites géométriques.

Pour notre exemple, c'est bien plus difficile (et pas au programme), mais on sait trouver : 

$C_n=26000*(1.02)^n - 25000$

Vous remarquerez qu'on peut alors caclculer $C_6=26000*(1.02)^6 - 25000=4280,22901$ sans avoir eu besoin de $C_5$, ou $C_4$, ... ou n'importe quel qutre termes. On a juste eu besoin de savoir que $n=6$. C'est donc bien une formule **en fonction de $n$**

  > Remarque: Toujours tester les formules obtenues : est-ce que, si on remplace n par 0, 1, 2 ou 3, on obtient bien la même valeur que par le calcul direct ? Dans l'exemple, on peut observer que la valeur de $C_6$ est cohérente avec le tableau de valeurs de la calculatrice.

## Variations

Définition

 * La suite $(u_n)$ est croissante si et seulement si, pour tout n, $u_{n+1} \geq u_n$
 * La suite $(u_n)$ est décroissante si et seulement si, pour tout n, $u_{n+1} \leq u_n$
 * La suite $(u_n)$ est constante si et seulement si, pour tout n, $u_{n+1} = u_n$
 
 Et évidemment, on dira:
 
 * La suite $(u_n)$ est strictement croissante si et seulement si, pour tout n, $u_{n+1} > u_n$
 * La suite $(u_n)$ est strictement décroissante si et seulement si, pour tout n, $u_{n+1} < u_n$

 Dans la pratique, on étudiera le signe de $u_{n+1} - u_n$.
