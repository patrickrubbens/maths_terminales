<html><head><meta charSet="utf-8"/><meta data-frontend="content-web"/><title>Statistique à deux variables : cours Tle - Mathématiques</title><meta name="viewport" content="width=device-width, initial-scale=1"/><meta property="og:title" content="Statistique à deux variables : cours Tle - Mathématiques"/><meta name="description" content="SchoolMouv ® te propose ce cours sur Statistique à deux variables (Tle - Maths) pour TOUT comprendre avec ✔️ vidéo ✔️ fiche de révision ✔️ exercices…"/><link rel="canonical" href="https://www.schoolmouv.fr/cours/statistique-a-deux-variables-quantitatives/fiche-de-cours"/><link rel="icon" href="/content/favicon.ico"/><script id="json-ld" type="application/ld+json">{"@context":"https://schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"@id":"https://www.schoolmouv.fr","name":"Accueil"}},{"@type":"ListItem","position":2,"item":{"@id":"https://www.schoolmouv.fr/terminale","name":"Tle"}},{"@type":"ListItem","position":3,"item":{"@id":"https://www.schoolmouv.fr/terminale/mathematiques","name":"Mathématiques"}},{"@type":"ListItem","position":4,"item":{"@id":"https://www.schoolmouv.fr/cours/statistique-a-deux-variables-quantitatives/fiche-de-cours","name":"Cours : Statistique à deux variables"}}]}</script><meta name="next-head-count" content="9"/><link rel="preload" href="/content/_next/static/css/b70c70ad4185622d.css" as="style"/><link rel="stylesheet" href="/content/_next/static/css/b70c70ad4185622d.css" data-n-g=""/><link rel="preload" href="/content/_next/static/css/783b6c2bdad5f5f5.css" as="style"/><link rel="stylesheet" href="/content/_next/static/css/783b6c2bdad5f5f5.css" data-n-p=""/><noscript data-n-css=""></noscript><script defer="" nomodule="" src="/content/_next/static/chunks/polyfills-c67a75d1b6f99dc8.js"></script><script src="/content/_next/static/chunks/webpack-5690b167ef23b98a.js" defer=""></script><script src="/content/_next/static/chunks/framework-66d32731bdd20e83.js" defer=""></script><script src="/content/_next/static/chunks/main-b8e046f50a7271a7.js" defer=""></script><script src="/content/_next/static/chunks/pages/_app-0e31bd59abb86d99.js" defer=""></script><script src="/content/_next/static/chunks/175675d1-e45fffc9c1589617.js" defer=""></script><script src="/content/_next/static/chunks/532-871ae0be025323f1.js" defer=""></script><script src="/content/_next/static/chunks/148-05bfbf14df8f6750.js" defer=""></script><script src="/content/_next/static/chunks/pages/%5Btype%5D/%5BsheetSlug%5D/%5BresourceType%5D-b98c4afc29216ba3.js" defer=""></script><script src="/content/_next/static/w3DfU_Q672BFP4XviUOun/_buildManifest.js" defer=""></script><script src="/content/_next/static/w3DfU_Q672BFP4XviUOun/_ssgManifest.js" defer=""></script></head>
<body>

<div class='introduction' markdown='1'><p>Introduction&nbsp;:</p>
<p>En statistique, on cherche à étudier l’effet d’un ou de plusieurs paramètres. Les années précédentes, il était question d’étudier en mathématiques une population avec des séries statistiques à une variable.<br />
Cependant, dans de nombreux cas, les différents paramètres que l’on étudie pour une même population présentent des liens qu’il est important de pouvoir mettre en évidence, même s’il ne faut pas conclure trop vite à un lien de cause à effet. On parle alors de statistiques à plusieurs variables.</p>
<p>Ainsi, dans ce cours, nous allons nous intéresser aux séries statistiques à deux variables. 
Dans un premier temps, nous définirons ce qu’est une série statistique à deux variables, comment la représenter et quelles données caractéristiques on peut en déduire.<br />
Ensuite, afin de mettre en avant les corrélations entre les deux variables, nous expliquerons comment faire un ajustement affine et comment en déduire la droite des moindres carrées, ce qui permettra d’interpoler ou d’extrapoler.<br />
Enfin, nous montrerons comment se ramener à un ajustement linéaire avec un changement de variable.</p></div>
<h2 range="1" markdown="1"><div markdown="1"><p>Série statistique à deux variables</p></div></h2>
<p>Pour étudier simultanément deux variables statistiques, il est possible de définir une série statistique double ou à deux variables.<br />
Pour cela, on a pour une population donnée de $n$ individus deux caractères quantitatifs&nbsp;:</p>
<ul>

<li>la variable $x$, pour laquelle les données relevées sont $x_1$, $x_2$, …, $x_n$ ;</li>

<li>la variable $y$, pour laquelle les données relevées sont $y_1$, $y_2$, …, $y_n$.</li>
</ul>
<p>Chaque individu aura ainsi un couple de caractères associé $(x_i\ ;\, y_i)$ (avec $i\in \lbrace 1,\,2,\,…,\,n\rbrace$). Notons que leurs unités seront souvent différentes (par ex., si on s’intéresse au lien entre la température extérieure et la consommation électrique, ou l’évolution d’une population quelconque en fonction du temps).</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">L’ensemble de ces couples constitue une <b>série statistique à deux variables</b>.</div>
</li>
</ul>
<h3 range="a." markdown="1"><div markdown="1"><p>Tableau de données et nuage de points</p></div></h3>
<p>On représente généralement les couples sous forme de tableau avec une colonne (ou une ligne) pour le caractère $x$ et une colonne (ou une ligne) pour le caractère $y$.</p>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><p>Nous nous intéressons à l’éventuel lien entre la teneur en carbone, en pourcent, d’un objet et la charge de rupture, c’est-à-dire la charge, en kilogramme, qui provoquera la rupture de l’objet.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">$10$ essais ont été faits en laboratoire, et nous obtenons les résultats suivants&nbsp;:</div>
</li>
</ul>
<div markdown="1" class="tab-c texte-centre"><table markdown="1" width="auto%"  style=""><p><tr>
<td style="background:#52d2fb; "  ><b>Teneur en carbone $x_i$</b></p>
<p><div markdown='1' class='paragraph' style='width:auto%; margin-top:-25px;margin-right:0px;margin-bottom:-25px;margin-left:0px;'><p>(en $\%$)</p></div></p>
<p></td>
<td style="background:#52d2fb; "  ><b>Charge de rupture $y_i$</b></p>
<p><div markdown='1' class='paragraph' style='width:auto%; margin-top:-25px;margin-right:0px;margin-bottom:-25px;margin-left:0px;'><p>(en $\text{kg}$)</p></div></p>
<p></td>
</tr>
<tr>
<td>$64$</td>
<td>$77$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$68$</td>
<td style="background:#abe6ff; "  >$81$</td>
</tr>
<tr>
<td>$61$</td>
<td>$72$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$71$</td>
<td style="background:#abe6ff; "  >$86$</td>
</tr>
<tr>
<td>$66$</td>
<td>$79$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$74$</td>
<td style="background:#abe6ff; "  >$93$</td>
</tr>
<tr>
<td>$63$</td>
<td>$74$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$70$</td>
<td style="background:#abe6ff; "  >$86$</td>
</tr>
<tr>
<td>$60$</td>
<td>$70$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$62$</td>
<td style="background:#abe6ff; "  >$71$</td>
</tr></p></table></div></div></div>
<p>La plupart du temps, nous représentons ces données par un <b>nuage de points</b>, qui nous permet de mieux visualiser les données.</p>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Nuage de points&nbsp;:</b></p>
<p>On représente une série statistique à deux variables $x$ et $y$ par un nuage de points dans un repère orthogonal $(O\ ;\, I,\,J)$, constitué de points $M_i$ de coordonnées $(x_i\ ;\, y_i)$, $x_i$ et $y_i$ étant respectivement les valeurs des variables $x$ et $y$ pour l’individu $i$ ($i$ allant de $1$ à $n$, avec $n$ la taille de la population).</p></div></div>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><p>Donnons le nuage de points correspondant aux données de notre exemple&nbsp;:</p>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img01.png?1649859889886" alt="Teneur en carbone et charge de rupture" /> 
<span>Teneur en carbone et charge de rupture
 </span></p></div></div></div></div>
<p>Le nuage de points que nous venons de représenter montre que les points ne semblent pas répartis au hasard.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Les deux variables semblent avoir une corrélation&nbsp;: en effet, quand la teneur en carbone augmente, la charge de rupture paraît augmenter aussi. </div>
</li>
</ul>
<p>Mais comment rendre cette corrélation plus évidente et, surtout, comment la quantifier&nbsp;?</p>
<h3 range="b." markdown="1"><div markdown="1"><p>Point moyen</p></div></h3>
<p>Tout d’abord, nous pouvons calculer la moyenne des deux variables, ce qui nous permet de définir le <b>point moyen</b>.</p>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Point moyen&nbsp;:</b></p>
<p>Soit une série statistique à deux variables $x$ et $y$, représentée par un nuage de points dans un repère $(O\ ;\, I,\,J)$.<br />
On définit le point moyen de ce nuage comme le point $G$, de coordonnées $(\bar{x}\ ;\,  \bar{y})$, où&nbsp;:</p>
<ul>



<li>$\bar{x}$ est la moyenne arithmétique des valeurs $x_i$ associées à la variable $x$ ;</li>



<li>$\bar{y}$ est la moyenne arithmétique des valeurs $y_i$ associées à la variable $y$.</li>
</ul></div></div>
<p>Nous savons calculer ces moyennes grâce aux formules suivantes.</p>
<div class='blocks rappel' markdown='1'><div class='blocks-icon'>
<img src='/content/images/rappel.svg' alt='bannière rappel' />
<p>Rappel</p>
</div>
<div class='blocks-text' markdown='1'><p>Soit  $x_1$, $x_2$ …, $x_n$ les $n$ valeurs de la variable $x$, et $y_1$, $y_2$, …, $y_n$ les $n$ valeurs de la variable $y$.<br />
Nous avons alors&nbsp;:</p>
<p>$$\begin{aligned}
\bar{x}&amp;=\dfrac{x_1+x_2+x_3+…+x_n}{n} \\
\bar{y}&amp;=\dfrac{y_1+y_2+y_3+…+y_n}{n}
\end{aligned}$$</p></div></div>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><p>Continuons de filer notre exemple, et calculons les moyennes de $x$ et $y$ :</p>
<p>$$\begin{aligned}
\bar x&amp;=\dfrac{64+68+61+71+66+74+63+70+60+62}{10} \\
&amp;=\dfrac {659}{10} \\
&amp;=65,9 \\
\\
\bar y&amp;=\dfrac{77+81+72+86+79+93+74+86+70+71}{10} \\
&amp;=\dfrac {789}{10} \\
&amp;=78,9 
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Le point moyen $G$ a donc pour coordonnées $(65,9\ ;\, 78,9)$.</div>
</li>
</ul>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img02.png?1649859933962" alt="Point moyen" /> 
<span>Point moyen
 </span></p></div></div></div></div>
<h3 range="c." markdown="1"><div markdown="1"><p>Covariance et coefficient de corrélation</p></div></h3>
<p>Dans les classes précédentes, nous avons appris à calculer un indicateur qui permet de mesurer la dispersion des données d’une série statistique autour de sa moyenne.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Il s’agit de la <b>variance</b>.</div>
</li>
</ul>
<div class='blocks rappel' markdown='1'><div class='blocks-icon'>
<img src='/content/images/rappel.svg' alt='bannière rappel' />
<p>Rappel</p>
</div>
<div class='blocks-text' markdown='1'><p>Soit une série statistique à une variable $x$, d’effectif $n$ : $(x_1,\,x_2,\,…,\,x_n)$, de moyenne $\bar x$.<br />
La variance de $x$, que nous notons ici $\text{var}(x)$, est donnée par la formule&nbsp;:</p>
<p>$$\begin{aligned}
\text{var}(x)&amp;=\dfrac 1n \sum_{i=1}^n (x_i-\bar x)^2 \\
&amp;=\dfrac 1n\big((x_1-\bar x)^2+(x_2-\bar x)^2+…+(x_n-\bar x)^2\big)
\end{aligned}$$</p>
<p>Rappelons aussi que nous définissons l’écart-type de $x$, noté $\sigma(x)$, ainsi&nbsp;:</p>
<p>$$\sigma(x)=\sqrt{\text{var}(x)}$$</p></div></div>
<p>La <b>covariance</b>, elle, est une notion nouvelle, qui vient avec la notion de statistique à deux variables. Comme son nom l’indique, elle mesure la façon dont évoluent conjointement les deux variables considérées.</p>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Covariance de $(x\ ;\, y)$ :</b></p>
<p>Soit une série statistique à deux variables $x$ et $y$, d’effectif $n$ : $\big((x_1\ ;\, y_1),\,(x_2\ ;\, y_2),\,…,\,(x_n\ ;\, y_n)\big)$, respectivement de moyennes $\bar x$ et $\bar y$.<br />
La covariance de $(x\ ;\, y)$, notée ici $\text{cov}(x\ ;\, y)$, est donnée par la formule&nbsp;:</p>
<p>$$\begin{aligned}
\text{cov}(x)&amp;=\dfrac 1n \sum_{i=1}^n (x_i-\bar x)(y_i-\bar y) \\
&amp;=\dfrac 1n\big((x_1-\bar x)(y_1-\bar y)+(x_2-\bar x)(y_2-\bar y)+…+(x_n-\bar x)(y_n-\bar y)\big)
\end{aligned}$$</p></div></div>
<p>Nous le voyons, pour calculer la variance, nous effectuons une somme de produits entre deux grandeurs qui ne sont pas, la plupart du temps, exprimées dans la même unité.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous allons donc définir un <b>coefficient de corrélation</b>, qui sera plus explicite.</div>
</li>
</ul>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Coefficient de corrélation&nbsp;:</b></p>
<p>Soit une série statistique à deux variables</p>
<ul>



<li>$x$, de variance $\text{var}(x)$ et d’écart-type $\sigma(x)$,</li>



<li>et $y$, de variance $\text{var}(y)$ et d’écart-type $\sigma(y)$.</li>
</ul>
<p>Soit $\text{cov}(x\ ;\, y)$ la covariance de $x$ et $y$.<br />
Le coefficient de corrélation $r$, aussi noté $\rho_{xy}$, est alors défini par&nbsp;:</p>
<p>$$\begin{aligned}
r&amp;=\dfrac{\text{cov}(x\ ;\, y)}{\sqrt{\text{var}(x)\text{var}(y)}} \\
&amp;=\dfrac{\text{cov}(x\ ;\, y)}{\sigma(x)\sigma(y)}
\end{aligned}$$</p></div></div>
<div class='blocks a_retenir' markdown='1'><div class='blocks-icon'>
<img src='/content/images/a-retenir.svg' alt='bannière à retenir' />
<p>À retenir</p>
</div>
<div class='blocks-text' markdown='1'><p>Ce coefficient indique le lien, linéaire, qui existe entre les variables $x$ et $y$ :</p>
<ul>



<li>il appartient à l’intervalle $[-1\ ;\, 1]$ ;</li>



<li>plus il est proche des bornes de l’intervalle $-1$ et $1$, plus la corrélation linéaire entre $x$ et $y$ est forte&nbsp;;</li>



<li>en revanche, deux variables indépendantes ont un coefficient de corrélation proche de $0$ ;</li>



<li>s’il est positif, alors $x$ et $y$ varient «&nbsp;dans le même sens&nbsp;» (plus les valeurs de $x$ grandissent, plus celles de $y$ grandissent)&nbsp;;</li>



<li>s’il est négatif, alors $x$ et $y$ varient «&nbsp;en sens contraires&nbsp;» (plus les valeurs de $x$ grandissent, plus celles de $y$ diminuent).</li>
</ul></div></div>
<p>Nous allons maintenant calculer tous ces indicateurs dans le cas de notre exemple et en tirer une première conclusion.</p>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><ul>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">Rappelons que nous avons trouvé&nbsp;: $\bar x=65,9$ et $\bar y=78,9$.</div>
</li>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Calculons d’abord les variances et écarts-types de $x$ et $y$ :</div>
</li>
</ul>
<p>$$\begin{aligned}
\text{var}(x)&amp;=\dfrac{1}{10}\times \big((x_1-\bar x)^2+ (x_2-\bar x)^2+…+ (x_9-\bar x)^2+ (x_{10}-\bar x)^2 \big) \\
&amp;=\dfrac 1{10}\times \big((64-65,9)^2+(68-65,9)^2+…+(60-65,9)^2+(62-65,9)^2\big) \\
&amp;=\dfrac 1{10}\times \big((-1,9)^2+2,1^2+…+(-5,9)^2+(-3,9)^2\big) \\
&amp;=\dfrac 1{10}\times (3,61+4,41+…+34,81+15,21) \\
&amp;=\dfrac{198,9}{10} \\
&amp;=19,89 \\
\sigma(x)&amp;=\sqrt{19,89} \\
&amp;\approx 4,4598 \\
\\
\text{var}(y)&amp;=\dfrac{1}{10}\times \big((y_1-\bar y)^2+ (y_2-\bar y)^2+…+ (y_9-\bar y)^2+ (y_{10}-\bar y)^2 \big) \\
&amp;=\dfrac 1{10}\times \big((77-78,9)^2+(81-78,9)^2+…+(70-78,9)^2+(71-78,9)^2\big) \\
&amp;=\dfrac{520,9}{10} \\
&amp;=52,09 \\
\sigma(x)&amp;=\sqrt{19,89} \\
&amp;\approx 7,2173 \\
\end{aligned}$$</p>
<ul>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Intéressons-nous à la covariance de $x$ et $y$ :</div>
</li>
</ul>
<p>$$\begin{aligned}
\text{cov}(x\ ;\, y)&amp;=\dfrac 1{10}\big((x_1-\bar x) (y_1-\bar y)+ (x_2-\bar x) (y_2-\bar y)+…+ (x_{10}-\bar x) (y_{10}-\bar y)\big) \\
&amp;=\dfrac 1{10}\big(3,61+4,41+…+30,81) \\
&amp;=\dfrac{318,9}{10} \\
&amp;=31,89
\end{aligned}$$</p>
<ul>

<li class="bullet_list"><div style="" range="4"></div><div class="list_text">Nous pouvons calculer maintenant le coefficient de corrélation&nbsp;:</div>
</li>
</ul>
<p>$$\begin{aligned}
r&amp;=\dfrac{\text{cov}(x\ ;\, y)}{\sqrt{\text{var}(x)\text{var}(y)}} \\
&amp;=\dfrac {31,89}{\sqrt{19,89\times 52,09}} \\
&amp;\approx 0,9907
\end{aligned}$$</p>
<ul>

<li class="bullet_list"><div style="" range="C"></div><div class="list_text">Nous pouvons en déduire que $x$ et $y$ ont une corrélation linéaire forte ($r\approx 1$)&nbsp;; en outre, quand les valeurs prises par $x$ croissent, celles prises par $y$ croissent également ($r&gt;0$).</div>
</li>
</ul></div></div>
<h2 range="2 " markdown="1"><div markdown="1"><p>Ajustement affine</p></div></h2>
<p>Ce qui va suivre a pour but de donner des outils pour effectuer des prévisions pour des valeurs inconnues, que celles-ci soient dans le domaine d’étude ou en dehors.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">On parle alors d’<b>interpolation</b> et d’<b>extrapolation</b>.</div>
</li>
</ul>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Interpolation et extrapolation&nbsp;:</b></p>
<ul>



<li>Lorsque l’on s’intéresse à des valeurs inconnues mais qui font partie du domaine couvert par les données fournies par l’étude, alors on effectue une interpolation.</li>



<li>Si l’on travaille hors de ce domaine, alors on effectue une extrapolation.</li>
</ul></div></div>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><p>Pour la teneur en carbone de nos objets et la charge de rupture associée&nbsp;:</p>
<ul>



<li>nous pouvons vouloir estimer la charge de rupture d’un objet dont la teneur de carbone est de $69,2\,\%$, valeur de $x$ qui est bien comprise entre le minimum ($60\,\%$) et le maximum ($74\,\%$) de la série,</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">nous effectuerons alors une interpolation&nbsp;;</div>
</li>



<li>nous pouvons aussi avoir besoin d’avoir une approximation de la charge de rupture pour un objet de teneur $50\,\%$ ;</li>



<li>nous pouvons encore souhaiter connaître la teneur en carbone de l’objet pour que la charge de rupture soit de $100\ \text{kg}$ ;</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">dans ces deux derniers cas, nous ferons une extrapolation.</div>
</li>
</ul></div></div>
<div class='blocks attention' markdown='1'><div class='blocks-icon'>
<img src='/content/images/attention.svg' alt='bannière attention' />
<p>Attention</p>
</div>
<div class='blocks-text' markdown='1'><p>Effectuer une extrapolation peut être dangereux&nbsp;: en effet, rien ne démontre que le modèle déduit des données fournies reste vrai en dehors de ce domaine.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Il conviendra donc toujours de prendre des précautions.</div>
</li>
</ul></div></div>
<p>Par exemple, si un commerçant s’intéresse au chiffre d’affaires qu’il fait en fonction de l’heure de la journée et qu’il ne relève les données qu’entre 17&nbsp;heures et 19&nbsp;heures, alors, ce créneau correspondant à la sortie des bureaux et donc à ses heures de grande fréquentation, il ne pourra extrapoler le chiffre fait lors des horaires «&nbsp;creux&nbsp;», à&nbsp;14&nbsp;h&nbsp;30, par exemple.</p>
<h3 range="a." markdown="1"><div markdown="1"><p>Ajustement affine</p></div></h3>
<p>Lorsqu’un lien linéaire semble apparaître entre deux variables, et afin de pouvoir faire des interpolations et des extrapolations, il est intéressant d’ajuster le nuage de points au moyen d’une droite et de caractériser ainsi la relation affine entre les deux variables.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">On parle d’<b>ajustement affine</b>.</div>
</li>
</ul>
<div class='blocks definition' markdown='1'><div class='blocks-icon'>
<img src='/content/images/definition.svg' alt='bannière definition' />
<p>Définition</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Ajustement affine&nbsp;:</b></p>
<p>Le principe de l’ajustement affine est de tracer, lorsque les points d’un nuage semblent globalement alignés, une droite passant «&nbsp;au plus près&nbsp;» de ces points.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Cette droite est alors appelée droite d’ajustement, ou droite de régression.</div>
</li>
</ul></div></div>
<p>Remarquons que «&nbsp;au plus près&nbsp;» est une formulation assez vague. Il existe plusieurs techniques.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous allons en présenter deux&nbsp;: une à partir de la notion de point moyen, la <b>méthode de&nbsp;Mayer</b>, et l’autre, très utilisée, dite <b>méthode des moindres carrés</b>.</div>
</li>
</ul>
<h3 range="b." markdown="1"><div markdown="1"><p>Méthode de&nbsp;Mayer</p></div></h3>
<p>Cette méthode, aussi appelée méthode des points moyens, consiste tout simplement à relier deux points moyens du nuage. Elle n’est guère fiable, car elle est notamment sensible aux valeurs extrêmes, mais elle a le mérite d’être simple et rapide.</p>
<div class='blocks a_retenir' markdown='1'><div class='blocks-icon'>
<img src='/content/images/a-retenir.svg' alt='bannière à retenir' />
<p>À retenir</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Méthodologie&nbsp;:</b></p>
<ul>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">On divise le nuage en $2$ groupes de points de même effectif (ou l’un avec un point supplémentaire, si l’effectif est impair).</div>
</li>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">On calcule le point moyen de ces $2$ groupes.</div>
</li>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">On relie ces $2$ points moyens pour obtenir la droite d’ajustement.</div>
</li>

<li class="bullet_list"><div style="" range="4"></div><div class="list_text">On peut aussi, si besoin, connaissant les coordonnées de $2$ points, déterminer l’équation de la droite.</div>
</li>
</ul></div></div>
<p>Appliquons-la rapidement à notre exemple.</p>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><ul>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">Nous considérons les données par ordre croissant de la teneur en carbone, et scindons donc les points en deux groupes de $5$ couples&nbsp;:</div>
</li>
</ul>
<p>$$\begin{aligned}
\textcolor{#A9A9A9}{\text{Groupe 1\ : }} (60\ ;\, 70),\,(61\ ;\, 72),\,(62,\,71),\,(63,\,74),\,(64,\,77) \\
\textcolor{#A9A9A9}{\text{Groupe 2\ : }} (66\ ;\, 79),\,(68\ ;\, 81),\,(70,\,86),\,(71,\,86),\,(74,\,93)
\end{aligned}$$</p>
<ul>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Nous calculons les coordonnées des points moyens $G_1$ et $G_2$, respectivement des groupes&nbsp;1 et&nbsp;2&nbsp;:</div>
</li>
</ul>
<p>$$\begin{aligned}
\bar x_1&amp;= \dfrac{60+61+62+63+64}5 \\
&amp;=62 \\
\bar y_1&amp;= \dfrac{70+72+71+74+77}5 \\
&amp;=72,8 \\
\\
\bar x_2&amp;= \dfrac{66+68+70+71+74}5 \\
&amp;=69,8 \\
\bar y_2&amp;= \dfrac{79+81+86+86+93}5 \\
&amp;=85
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous obtenons donc $G_1$ de coordonnées $(62\ ;\, 72,8)$ et $G_2$ de coordonnées $(69,8\ ;\, 85)$.</div>
</li>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Nous représentons ces points et les relions par une droite, qui sera donc notre droite d’ajustement.</div>
</li>
</ul>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img03.png?1649860008302" alt="Ajustement affine par la méthode de Mayer" /> 
<span>Ajustement affine par la méthode de Mayer
 </span></p></div></div>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous pouvons remarquer que le point moyen $G$ appartient à cette droite.</div>
</li>
</ul></div></div>
<h3 range="c." markdown="1"><div markdown="1"><p>Droite des moindres carrés</p></div></h3>
<p>Nous allons maintenant aborder la méthode la plus utilisée pour effectuer un ajustement affine&nbsp;: la <b>méthode des moindres carrés</b>.</p>
<p>Pour bien la comprendre, considérons un nuage simple de points $M_i\, (x_i\ ;\, y_i)$, représentons aussi une droite, qui passe par le point moyen $G$ et dont l’équation est de la forme $y=ax+b$.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous considérons en outre les points $P_i$ de la droite, d’abscisse $x_i$ et d’ordonnée $ax_i+b$.</div>
</li>
</ul>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img04.png?1649860096370" alt="Ajustement affine par la droite des moindres carrés" /> 
<span>Ajustement affine par la droite des moindres carrés
 </span></p></div></div>
<p>Ce qui nous intéresse, c’est la distance entre les points $M_i$ et $P_i$ associés.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Dans le schéma ci-dessus, nous avons explicité cette distance entre les points $M_3$ et $P_3$. De la même façon, pour tout $i$ compris entre $1$ et $5$, nous avons&nbsp;:</div>
</li>
</ul>
<p>$$M_iP_i=\vert y_i-(ax_i+b)\vert$$</p>
<div class='blocks a_retenir' markdown='1'><div class='blocks-icon'>
<img src='/content/images/a-retenir.svg' alt='bannière à retenir' />
<p>À retenir</p>
</div>
<div class='blocks-text' markdown='1'><p>Soit un nuage de $n$ points, qui représente une série statistique à deux variables.<br />
Déterminer la <b>droite des moindres carrés</b> consiste à trouver la droite qui minimise le carré des distances $M_iP_i$ ($i\in \lbrace 1,\,…,\,n\rbrace$).</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Il s’agit donc de déterminer les réels $a$ et $b$ tels que la somme suivante soit minimale&nbsp;:</div>
</li>
</ul>
<p>$$\begin{aligned}
\sum_{i=1}^n \big(y_i-(ax_i+b)\big)^2 = \big(y_1-(ax_1+b)\big)^2+…+\big(y_n-(ax_n+b)\big)^2 \\
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous parlons aussi de <b>droite d’ajustement de $y$ en $x$</b>.</div>
</li>
</ul></div></div>
<p>Pour cela, nous allons admettre la propriété suivante.</p>
<div class='blocks propriete' markdown='1'><div class='blocks-icon'>
<img src='/content/images/propriete.svg' alt='bannière propriete' />
<p>Propriété</p>
</div>
<div class='blocks-text' markdown='1'><p>Soit une série statistique à deux variables&nbsp;:</p>
<ul>



<li>$x$, de moyenne $\bar x$, de variance $\text{var}(x)$ et d’écart-type $\sigma(x)$,</li>



<li>et $y$, de moyenne $\bar y$, de variance $\text{var}(y)$ et d’écart-type $\sigma(y)$.</li>
</ul>
<p>Soit $\text{cov}(x\ ;\, y)$ la covariance de $x$ et $y$.<br />
La droite des moindres carrés, ou droite d’ajustement de $y$ en $x$, a pour équation $y=ax+b$ où&nbsp;:</p>
<p>$$\begin{aligned}
a&amp;=\dfrac {\text{cov}(x\ ;\, y)}{\text{var}(x)} \\
&amp;=\dfrac {\text{cov}(x\ ;\, y)}{\sigma^2(x)} \\
b&amp;=\bar y-a\bar x
\end{aligned}$$</p></div></div>
<p>Résumons ce qui précède en donnant une méthodologie à suivre, lorsqu’un exercice demande de déterminer la droite d’ajustement par la méthode des moindres carrés.</p>
<div class='blocks a_retenir' markdown='1'><div class='blocks-icon'>
<img src='/content/images/a-retenir.svg' alt='bannière à retenir' />
<p>À retenir</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Méthodologie d’ajustement affine par la méthode des moindres carrés&nbsp;:</b></p>
<p>Soit une série statistique à deux variables $x$ et $y$.</p>
<ul>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">Si nécessaire, représenter le nuage de points $(x_i\ ;\, y_i)$ dans un repère orthogonal.</div>
</li>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Calculer les moyennes $\bar x$ et $\bar y$ des deux variables.</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Placer le cas échéant le point moyen $G\,(\bar x\ ;\, \bar y)$ dans la représentation.</div>
</li>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Calculer les variances $\text{var}(x)$ et $\text{var}(y)$ des deux variables.</div>
</li>

<li class="bullet_list"><div style="" range="4"></div><div class="list_text">Calculer la covariance $\text{cov}(x\ ;\, y)$ des deux variables&nbsp;:</div>
</li>
</ul>
<p>$$\text{cov}(x\ ;\, y)= \dfrac 1n \sum_{i=1}^n (x_i-\bar x)(y_i-\bar y)$$</p>
<ul>

<li class="bullet_list"><div style="" range="5"></div><div class="list_text">Déduire l’équation de la droite d’ajustement de $y$ en $x$ :</div>
</li>
</ul>
<p>$$y=\dfrac {\text{cov}(x\ ;\, y)}{\text{var}(x)}\cdot x + \bar y-a\bar x$$</p>
<ul>

<li class="bullet_list"><div style="" range="6"></div><div class="list_text">Calculer le coefficient de corrélation&nbsp;:</div>
</li>
</ul>
<p>$$r=\dfrac{\text{cov}(x\ ;\, y)}{\sqrt{\text{var}(x)\text{var}(y)}}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">En théorie, un ajustement affine est toujours possible, mais il est indispensable de mesurer sa pertinence&nbsp;; le calcul du coefficient de corrélation est donc important pour pouvoir en juger.</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Plus $r$ est proche en valeur absolue de $1$, plus la corrélation linéaire est forte, et donc plus l’ajustement affine est pertinent.</div>
</li>

<li class="bullet_list"><div style="" range="7"></div><div class="list_text">Si l’ajustement s’avère suffisamment pertinent, alors on peut s’en servir pour effectuer&nbsp;:</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">des <em>inter</em>polations («&nbsp;<em>entre</em>&nbsp;» les données de la série),</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">des <em>extra</em>polations («&nbsp;<em>hors</em>&nbsp;» des données de la série).</div>
</li>
</ul></div></div>
<p>Appliquons cette méthode, toujours à notre exemple.</p>
<div class='blocks exemple' markdown='1'><div class='blocks-icon'>
<img src='/content/images/exemple.svg' alt='bannière exemple' />
<p>Exemple</p>
</div>
<div class='blocks-text' markdown='1'><p>Nous avons déjà représenté le nuage de points (étape&nbsp;1) et calculé les résultats pour les points&nbsp;2,&nbsp;3 et&nbsp;4&nbsp;:</p>
<div markdown="1" class="tab-c texte-centre"><table markdown="1" width="auto%"  style=""><p><tr>
<td style="width:25%; background:#52d2fb; "  ><b>Variable</b></td>
<td style="width:25%; background:#FFE4B5; "  ><b>Moyenne</b></td>
<td style="width:25%; background:#52d2fb; "  ><b>Variance</b></td>
<td style="width:25%; background:#FFE4B5; "  ><b>Covariance</b></td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$x$</td>
<td style="background:#FFEFD5; "  >$\bar x = 65,9$</td>
<td style="background:#abe6ff; "  >$\text{var}(x)=19,89$</td>
<td style="background:#FFEFD5; "  rowspan="2">$\text{cov}(x\ ;\,y)=31,89$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$y$</td>
<td style="background:#FFEFD5; "  >$\bar y=78,9$</td>
<td style="background:#abe6ff; "  >$\text{var}(y)=52,09$</td>
</tr></p></table></div>
<ul>

<li class="bullet_list"><div style="" range="5"></div><div class="list_text">Nous en déduisons les valeurs de $a$ et $b$ de l’équation réduite $y=ax+b$ de la droite des moindres carrés&nbsp;:</div>
</li>
</ul>
<p>$$\begin{aligned}
a&amp;=\dfrac {\text{cov}(x\ ;\, y)}{\text{var}(x)} \\
&amp;=\dfrac {31,89}{19,89} \\
&amp;=\dfrac{1\,063}{663} \\
&amp;\approx 1,603 \\
\\
b&amp;=\bar y-a\bar x \\
&amp;=78,9-\dfrac{1\,063}{663}\times 65,9 \\
&amp;\approx -26,759
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous en déduisons l’équation de la droite d’ajustement, en arrondissant à $10^{-3}$ près&nbsp;:</div>
</li>
</ul>
<p>$$y=1,603\,x-26,759$$</p>
<ul>

<li class="bullet_list"><div style="" range="6"></div><div class="list_text">Nous avons aussi trouvé le coefficient de corrélation entre $x$ et $y$ :</div>
</li>
</ul>
<p>$$r\approx 0,9907$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous l’avons déjà dit, mais reprécisons que l’ajustement affine est ici pertinent&nbsp;; en outre, quand $x$ grandit, $y$ grandit aussi.</div>
</li>

<li class="bullet_list"><div style="" range="7"></div><div class="list_text">Nous allons maintenant nous servir de cette équation pour effectuer quelques prévisions.</div>
</li>
</ul>
<p>Calculons d’abord la charge de rupture prévue par notre modèle pour un objet dont la teneur en carbone a été mesurée à $x^{\prime}=69,2\,\%$.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Il s’agit d’une interpolation.</div>
</li>
</ul>
<p>Il suffit de remplacer, dans l’équation, $x$ par la valeur donnée (pour plus de rigueur, nous utilisons les expressions exactes, et non les arrondis)&nbsp;:</p>
<p>$$\begin{aligned}
y^{\prime} &amp;= \dfrac{1\,063}{663} \times 69,2 + 78,9-\dfrac{1\,063}{663}\times 65,9 \\
&amp;\approx 84,191
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Le modèle prévoit une charge de rupture d’environ $84\ \text{kg}$.</div>
</li>
</ul>
<p>Nous souhaitons maintenant que la charge de rupture de notre objet soit de $y^{\prime\prime}=100\ \text{kg}$. Quelle teneur en carbone doit-il avoir&nbsp;?</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous voyons que $100\ \text{kg}$ est hors de notre domaine d’étude, il s’agit donc d’une extrapolation.</div>
</li>
</ul>
<p>Nous remplaçons cette fois $y$ par la valeur donnée&nbsp;:</p>
<p>$$\begin{aligned}
100 &amp;= \dfrac{1\,063}{663} \times x^{\prime\prime} + 78,9-\dfrac{1\,063}{663}\times 65,9 \\
\Leftrightarrow x^{\prime\prime}&amp;=\dfrac{663}{1\,063}\times \left( 21,1+\dfrac{1\,063}{663}\times 65,9 \right) \\
&amp;=\dfrac{663}{1\,063}\times 21,1+ 65,9 \\
&amp;\approx 79,060
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Avec une teneur de $80\ \%$, nous pouvons supposer que l’objet résistera à une charge de $100\ \text{kg}$.</div>
</li>
</ul>
<p>Remarquons que, si nous avions posé la même question pour une charge de $200\ \text{kg}$, nous aurions trouvé une teneur en carbone d’environ $141\ \%$… Ce qui, en l’occurrence, serait un non-sens physique.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous nous heurtons ici à une des limites de l’extrapolation.</div>
</li>
</ul></div></div>
<p>Ci-dessus, nous avons effectué «&nbsp;manuellement&nbsp;» les calculs complets (à quelques points de suspension près), car il est important de bien comprendre le principe (et aussi parce que des exercices le demandent). À cet effet, nous avons travaillé avec un nombre restreint de données (effectif de $10$).</p>
<p>En pratique, pour pouvoir effectuer des prévisions dignes de confiance et afin d’avoir un modèle mathématique d’ajustement le plus précis possible, nous devons disposer de beaucoup de données.<br />
Bien sûr, dans de tels cas, calculer «&nbsp;manuellement&nbsp;» les moyennes, les variances, la covariance, etc., serait une tâche titanesque (et parfaite pour les erreurs de calcul…).</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous nous servons alors de notre calculatrice ou, mieux encore, d’un tableur pour déterminer directement les indicateurs et même l’équation de la droite des moindres carrés.</div>
</li>
</ul>
<div class='blocks astuce' markdown='1'><div class='blocks-icon'>
<img src='/content/images/astuce.svg' alt='bannière astuce' />
<p>Astuce</p>
</div>
<div class='blocks-text' markdown='1'><p>Servons-nous de notre exemple pour montrer les fonctions concernant les statistiques à deux variables, et ce sur les tableurs les plus utilisés&nbsp;: Calc d’OpenOffice et Microsoft Excel.<br />
Sur une feuille, nous avons au préalable entré les $(x_i\ ;\, y_i)$ sur deux colonnes (nous aurions aussi pu les mettre sur deux lignes)&nbsp;:</p>
<ul>



<li>les valeurs prises par $x$ sont dans les cellules $\green{\text A 1}$ à $\green{\text A 10}$ ;</li>



<li>les valeurs prises par $y$ sont dans les cellules $\purple{\text B 1}$ à $\purple{\text B 10}$.</li>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">Pour calculer les principaux indicateurs&nbsp;:</div>
</li>
</ul>
<div markdown="1" class="tab-c texte-centre"><table markdown="1" width="100%"  style=""><p><tr>
<td style="width:30%; background:#52d2fb; "  ><b>Variable</b></td>
<td style="width:35%; background:#abe6ff; "  >$x$</td>
<td style="width:35%; background:#abe6ff; "  >$y$</td>
</tr>
<tr>
<td style="background:#FFE4B5; "  ><b>Moyenne</b></td>
<td style="background:#FFEFD5; "  >$$\small \text{MOYENNE(\red{A1:A10})}$$</td>
<td style="background:#FFEFD5; "  >$$\small \text{MOYENNE(\purple{B1:B10})}$$</td>
</tr>
<tr>
<td style="background:#52d2fb; "  ><b>Variance</b></td>
<td style="background:#abe6ff; "  >$$\small \text{VAR.P(\red{A1:A10})}$$</td>
<td style="background:#abe6ff; "  >$$\small \text{VAR.P(\purple{B1:B10})}$$</td>
</tr>
<tr>
<td style="background:#FFE4B5; "  ><b>Covariance</b></td>
<td style="background:#FFEFD5; " colspan="2" >$$\small \text{COVARIANCE(\red{A1:A10}\,;\,\purple{B1:B10})}$$</td>
</tr>
<tr>
<td style="background:#52d2fb; "  ><b>Coef. de corrélation</b> $r$</td>
<td style="background:#abe6ff; " colspan="2" >$$\small \text{COEFFICIENT.CORRELATION(\red{A1:A10}\,;\,\purple{B1:B10})}$$</td>
</tr>
<tr>
<td style="background:#FFE4B5; "  rowspan="2"><b>Droite d’ajustement</b>
$$y=ax+b$$</td>
<td style="background:#FFEFD5; " colspan="2" >$$\small \textcolor{#A9A9A9} {a\ :\ }\text{PENTE(\purple{B1:B10}\,;\,\red{A1:A10})}$$</td>
</tr>
<tr>
<td style="background:#FFEFD5; " colspan="2" >$$\small \textcolor{#A9A9A9} {b\ :\ }\text{ORDONNEE.ORIGINE(\purple{B1:B10}\,;\,\red{A1:A10})}$$</td>
</tr></p></table></div>
<ul>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Pour représenter le nuage de points&nbsp;:</div>
</li>

<li class="liste2"><div class="list_text">sélectionner les plages de données&nbsp;;</div>
</li>

<li class="liste2"><div class="list_text">insérer le graphique «&nbsp;Nuages de points&nbsp;»&nbsp;:</div>
</li>

<li class="liste3"><div class="list_text">avec Calc&nbsp;: <span class="color2">Insertion</span> / <span class="color2">Diagramme</span> / <span class="color2">XY (dispersion)</span> / <span class="color2">Points seuls</span>,</div>
</li>

<li class="liste3"><div class="list_text">avec Excel&nbsp;: <span class="color2">Insérer</span> / <span class="color2">Graphique</span> / <span class="color2">XY (nuage de points)</span>.</div>
</li>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Pour tracer la droite de régression, une fois le nuage de points réalisé&nbsp;:</div>
</li>

<li class="liste2"><div class="list_text">avec Calc, le diagramme étant sélectionné (double-clic dessus, si nécessaire)&nbsp;: <span class="color2">Insertion</span> / <span class="color2">Courbe de tendance</span> / <span class="color2">Linéaire</span>, et cocher&nbsp;: <span class="color2">Afficher l’équation</span>&nbsp;;</div>
</li>

<li class="liste2"><div class="list_text">avec Excel&nbsp;: <span class="color2">Clic droit sur un point du graphique</span> / <span class="color2">Ajouter une courbe de tendance</span> / <span class="color2">Linéaire</span>, et cocher&nbsp;: <span class="color2">Afficher l’équation sur le graphique</span>.</div>
</li>
</ul></div></div>
<h2 range="3" markdown="1"><div markdown="1"><p>Ajustement affine par changement de variable </p></div></h2>
<p>Dans certains cas, les points du nuage ne peuvent être considérés comme alignés, car l’approximation serait de manière évidente beaucoup trop grande.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Il ne faut pas pour autant conclure qu’il n’y a pas de corrélation entre les deux variables&nbsp;: il peut y avoir un lien, mais qui n’est pas linéaire.</div>
</li>
</ul>
<p>Parfois, il est toutefois possible d’étudier ce lien non linéaire au moyen d’un ajustement affine, et ce grâce à un <b>changement de variable</b>.<br />
Dans cette dernière partie, nous allons donc montrer cette méthode à travers l’exemple de l’évolution d’une population, que vous avez déjà abordée, ou que vous aborderez bientôt, en <a target="_blank" href="/cours/modeles-demographiques-comprendre-l-evolution-quantitative-des-populations/fiche-de-cours">enseignement scientifique</a> (nous considérerons dans ce cours que le temps est une donnée continue, tandis que, dans le cours d’enseignement scientifique, nous travaillons par palier entier, et donc de manière discrète).</p>
<h3 range="a." markdown="1"><div markdown="1"><p>Un nuage de points non alignés</p></div></h3>
<p>Nous disposons, pour une région, du recensement décennal de la population, sur tout le XX<sup>e</sup>&nbsp;siècle, exprimé en million et arrondi à la dizaine de milliers (la précision est meilleure après les années&nbsp;50)&nbsp;:</p>
<div markdown="1" class="tab-c texte-centre"><table markdown="1" width="auto%"  style=""><p><tr>
<td style="width:50%; background:#52d2fb; "  ><b>Année $x_i$</b></td>
<td style="width:50%; background:#52d2fb; "  ><b>Population $y_i$</b></p>
<p><div markdown='1' class='paragraph' style='width:auto%; margin-top:-25px;margin-right:0px;margin-bottom:-25px;margin-left:0px;'><p>(en million)</p></div></p>
<p></td>
</tr>
<tr>
<td>$0$</td>
<td>$1,210$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$10$</td>
<td style="background:#abe6ff; "  >$1,840$</td>
</tr>
<tr>
<td>$20$</td>
<td>$1,810$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$30$</td>
<td style="background:#abe6ff; "  >$2,890$</td>
</tr>
<tr>
<td>$40$</td>
<td>$4,430$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$50$</td>
<td style="background:#abe6ff; "  >$4,730$</td>
</tr>
<tr>
<td>$60$</td>
<td>$6,542$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$70$</td>
<td style="background:#abe6ff; "  >$9,552$</td>
</tr>
<tr>
<td>$80$</td>
<td>$14,264$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$90$</td>
<td style="background:#abe6ff; "  >$21,252$</td>
</tr>
<tr>
<td>$100$</td>
<td>$22,035$</td>
</tr></p></table></div>
<ul>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Servons-nous d’un tableur pour représenter le nuage de points et tracer la droite d’ajustement de $y$ en $x$.</div>
</li>
</ul>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img05.png?1649860283513" alt="Droite d’ajustement de y en x" /> 
<span>Droite d’ajustement de y en x
 </span></p></div></div>
<ul>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Nous voyons que les points du nuage peuvent difficilement être considérés comme alignés. De plus, les distances entre les points et la droite semblent assez grandes.</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Donnons néanmoins le coefficient de corrélation, obtenu avec le tableur&nbsp;:</div>
</li>
</ul>
<p>$$r\approx 0,923$$</p>
<p>Celui-ci n’est pas très éloigné de $1$, mais nous ne pouvons pas dire non plus qu’il est «&nbsp;presque égal&nbsp;» à $1$.</p>
<ul>

<li class="bullet_list"><div style="" range="C"></div><div class="list_text">Un ajustement affine par la méthode des moindres carrés ne semblent pas bien adapté.</div>
</li>
</ul>
<p>Nous pouvons tout de même faire une première remarque.<br />
Nous constatons une diminution de la population entre les années&nbsp;10 et&nbsp;20, et une stagnation entre les années&nbsp;40 et&nbsp;50.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Des événements historiques peuvent expliquer ces points.</div>
</li>
</ul>
<h3 range="b." markdown="1"><div markdown="1"><p>Changement de variable</p></div></h3>
<p>Si les points ne sont pas alignés, nous reconnaissons quand même l’«&nbsp;allure&nbsp;» caractéristique de la courbe représentative de la fonction exponentielle.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Nous allons donc considérer une nouvelle variable $z$ définie, pour tout $i \in \lbrace1,\,…,\,10\rbrace$, par&nbsp;:</div>
</li>
</ul>
<p>$$z_i=\ln{(y_i)}$$</p>
<p>Si l’évolution de la population suit effectivement une croissance exponentielle, nous pourrons alors mettre en évidence une corrélation linéaire entre $x$ et $z$.<br />
Notons que, dans cet exemple, nous nous intéressons aux effectifs d’une population, donc la variable $y$ prendra des valeurs strictement positives, et nous pouvons donc travailler directement avec la fonction logarithme népérien.</p>
<ul>

<li class="bullet_list"><div style="" range="1"></div><div class="list_text">Complétons notre tableau de données avec les valeurs de $z$, arrondies à $10^{-3}$ près&nbsp;:</div>
</li>
</ul>
<div markdown="1" class="tab-c texte-centre"><table markdown="1" width="auto%"  style=""><p><tr>
<td style="width:32%; background:#52d2fb; "  ><b>Année $x_i$</b></td>
<td style="width:34%; background:#52d2fb; "  ><b>Population $y_i$</b></p>
<p><div markdown='1' class='paragraph' style='width:auto%; margin-top:-25px;margin-right:0px;margin-bottom:-25px;margin-left:0px;'><p>(en million)</p></div></p>
<p></td>
<td style="width:34%; background:#52d2fb; "  ><b>$z_i=\ln{(y_i)}$</b></td>
</tr>
<tr>
<td>$0$</td>
<td>$1,210$</td>
<td>$0,191$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$10$</td>
<td style="background:#abe6ff; "  >$1,840$</td>
<td style="background:#abe6ff; "  >$0,610$</td>
</tr>
<tr>
<td>$20$</td>
<td>$1,810$</td>
<td>$0,593$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$30$</td>
<td style="background:#abe6ff; "  >$2,890$</td>
<td style="background:#abe6ff; "  >$1,061$</td>
</tr>
<tr>
<td>$40$</td>
<td>$4,430$</td>
<td>$1,488$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$50$</td>
<td style="background:#abe6ff; "  >$4,730$</td>
<td style="background:#abe6ff; "  >$1,554$</td>
</tr>
<tr>
<td>$60$</td>
<td>$6,542$</td>
<td>$1,878$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$70$</td>
<td style="background:#abe6ff; "  >$9,552$</td>
<td style="background:#abe6ff; "  >$2,257$</td>
</tr>
<tr>
<td>$80$</td>
<td>$14,264$</td>
<td>$2,658$</td>
</tr>
<tr>
<td style="background:#abe6ff; "  >$90$</td>
<td style="background:#abe6ff; "  >$21,252$</td>
<td style="background:#abe6ff; "  >$3,056$</td>
</tr>
<tr>
<td>$100$</td>
<td>$22,035$</td>
<td>$3,093$</td>
</tr></p></table></div>
<ul>

<li class="bullet_list"><div style="" range="2"></div><div class="list_text">Représentons le nuage de points $(x_i\ ;\, z_i)$ et traçons la droite d’ajustement de $z$ en $x$ :</div>
</li>
</ul>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img06.png?1649860367687" alt="Droite d’ajustement de z en x" /> 
<span>Droite d’ajustement de z en x
 </span></p></div></div>
<ul>

<li class="bullet_list"><div style="" range="3"></div><div class="list_text">Nous nous rendons compte que le nuage a une allure bien plus «&nbsp;allongée&nbsp;» et que les points sont raisonnablement proches de la droite.</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Pour confirmer cette impression, donnons le coefficient de corrélation de $(x\ ;\, z)$ :</div>
</li>
</ul>
<p>$$r^{\prime}\approx 0,993$$</p>
<p>Le coefficient est cette fois très proche de $1$.</p>
<ul>

<li class="bullet_list"><div style="" range="4"></div><div class="list_text">Nous décidons de modéliser l’évolution de la population par un ajustement réalisé grâce à ce changement de variable&nbsp;: $z=\ln{(y)}$.</div>
</li>
</ul>
<p>L’équation de la droite d’ajustement de $z$ en $x$ est alors&nbsp;:</p>
<p>$$z=0,030x+0,164$$</p>
<p>Nous en déduisons&nbsp;:</p>
<p>$$\begin{aligned}
\ln{(y)}=0,030x+0,164 &amp;\Leftrightarrow \text{e}^{\ln{(y)}}=\text{e}^{0,030x+0,164} \\
&amp;\footnotesize{\textcolor{#A9A9A9}{\text{[par stricte croissance de $\exp$]}}} \\
&amp;\Leftrightarrow y=\text{e}^{0,030x}\times \text{e}^{0,164} \\
&amp;\footnotesize{\textcolor{#A9A9A9}{\text{[car $\exp$ et $\ln$ sont réciproques}}} \\
&amp;\footnotesize{\textcolor{#A9A9A9}{\text{et e$^{a+b}=$e$^a\times$e$^b$]}}}
\end{aligned}$$</p>
<p>En arrondissant à $10^{-3}$ près, nous obtenons finalement&nbsp;:</p>
<p>$$y=1,178\times \text{e}^{0,030x}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">La fonction $f:x\mapsto 1,178\times \text{e}^{0,030x}$ permet d’ajuster le nuage de points $(x_i\ ;\, y_i)$.</div>
</li>
</ul>
<p>Traçons sa courbe représentative&nbsp;:</p>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img07.png?1649860416229" alt="Courbe représentative de la fonction f" /> 
<span>Courbe représentative de la fonction f
 </span></p></div></div>
<ul>

<li class="bullet_list"><div style="" range="5"></div><div class="list_text">Servons-nous de ce modèle pour d’abord faire une interpolation.</div>
</li>
</ul>
<p>Nous cherchons à savoir approximativement en quelle année $x^{\prime}$ la barre des $8$ millions d’habitants a été franchie. Nous décidons de le faire graphiquement.
Pour cela, nous déterminons graphiquement l’abscisse $x^{\prime}$ du point de la courbe représentative de $f$ d’ordonnée $y^{\prime}=8$ :</p>
<div class='image_container' style="width:100%;" markdown='1'><div class='image_zoom' markdown='1'><p><img src="https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img08.png?1649860458007" alt="Courbe représentative de la fonction f et interpolation" /> 
<span>Courbe représentative de la fonction f et interpolation
 </span></p></div></div>
<p>Nous trouvons&nbsp;: $x^{\prime}\approx 64$.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">La barre des $8$ millions d’habitants a été franchie approximativement à l’année $64$.</div>
</li>
</ul>
<p>Remarquons que si nous avions utilisé la modélisation sans changement de variable, nous aurions trouvé environ $49$ ans… ce qui aurait été en contradiction avec les données.</p>
<ul>

<li class="bullet_list"><div style="" range="6"></div><div class="list_text">Effectuons enfin une extrapolation.</div>
</li>
</ul>
<p>Nous nous demandons maintenant quelle sera la population $y^{\prime\prime}$ après $120$ ans.<br />
Nous nous servons bien sûr de la fonction dont nous disposons pour calculer $f(120)$ :</p>
<p>$$\begin{aligned}
y^{\prime\prime}&amp;=1,178\times \text{e}^{0,030\times 120} \\
&amp;\approx43,113
\end{aligned}$$</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Si nous supposons que ce modèle reste valable hors du domaine des données, nous pouvons prévoir une population d’environ $43$ millions de personnes.</div>
</li>
</ul>
<div class='blocks attention' markdown='1'><div class='blocks-icon'>
<img src='/content/images/attention.svg' alt='bannière attention' />
<p>Attention</p>
</div>
<div class='blocks-text' markdown='1'><p>Nous avons précisé que nous supposions le modèle valable hors du domaine de la série statistique, et ceci est toujours indispensable.</p>
<p>Dans notre exemple, nous constatons un infléchissement de la croissance à l’année $100$.<br />
Est-elle simplement conjoncturelle, comme les événements historiques à l’origine de la diminution et du ralentissement que nous avons déjà constatés&nbsp;? Ou cet infléchissement a-t-il des raisons plus profondes et le modèle perd-il sa pertinence pour toute extrapolation&nbsp;?<br />
En effet, en enseignement scientifique, vous avez découvert, ou découvrirez, que la croissance d’une population peut être exponentielle durant des périodes brèves, mais qu’elle se heurte à la limite des ressources disponibles. Ainsi, une population aura tendance à tendre vers un maximum.</p>
<p>Ici, nous n’avons pas assez d’informations pour savoir ce qu’il en est précisément.</p>
<ul>

<li class="arrow_list"><div style="" ></div><div class="list_text">Des données supplémentaires seront à relever au fil du temps, afin d’affiner le modèle.</div>
</li>
</ul></div></div>
<h3 range="c." markdown="1"><div markdown="1"><p>Récapitulatif</p></div></h3>
<p>Voici une méthodologie à suivre lorsqu’un changement de variable s’impose.</p>
<div class='blocks a_retenir' markdown='1'><div class='blocks-icon'>
<img src='/content/images/a-retenir.svg' alt='bannière à retenir' />
<p>À retenir</p>
</div>
<div class='blocks-text' markdown='1'><p><b>Méthodologie&nbsp;:</b></p>
<ul>



<li>En fonction de l’allure du nuage de points, décider d’un changement de variable.</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Généralement, les exercices vous guideront dans ce choix. Mais précisons qu’on fera appel surtout aux fonctions usuelles&nbsp;: logarithme, exponentielle, carré, racine carrée…</div>
</li>



<li>Calculer les nouvelles valeurs déduites du changement de variable.</li>



<li>Représenter le nouveau nuage de points et tracer la droite d’ajustement.</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Calculer le coefficient de corrélation correspondant, afin de confirmer la pertinence du changement de variable.</div>
</li>



<li>À partir de la définition de la nouvelle variable, en déduire la fonction d’ajustement des données initiales.</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">Représenter dans le nuage initial la courbe représentative de cette fonction, si l’on souhaite faire graphiquement des interpolations et des extrapolations.</div>
</li>

<li class="arrow_list"><div style="" ></div><div class="list_text">La définition de cette fonction permet aussi de faire, par le calcul, des interpolations et des extrapolations.</div>
</li>
</ul></div></div>
<p>Notons aussi qu’une calculatrice ou un tableur sont aussi capables de déterminer très rapidement la fonction d’ajustement, selon le modèle que vous choisirez.</p>
<div class='blocks astuce' markdown='1'><div class='blocks-icon'>
<img src='/content/images/astuce.svg' alt='bannière astuce' />
<p>Astuce</p>
</div>
<div class='blocks-text' markdown='1'><p>Dans notre exemple de croissance exponentielle, une fois le nuage représenté dans un tableur&nbsp;:</p>
<ul>

<li class="liste2"><div class="list_text">avec Calc, le diagramme étant sélectionné (double-clic dessus, si nécessaire)&nbsp;: <span class="color2">Insertion</span> / <span class="color2">Courbe de tendance</span> / <span class="color2">Exponentielle</span>, et cocher&nbsp;: <span class="color2">Afficher l’équation</span>&nbsp;;</div>
</li>

<li class="liste2"><div class="list_text">avec Excel&nbsp;: <span class="color2">Clic droit sur un point du graphique</span> / <span class="color2">Ajouter une courbe de tendance</span> / <span class="color2">Exponentielle</span>, et cocher&nbsp;: <span class="color2">Afficher l’équation sur le graphique</span>.</div>
</li>
</ul></div></div>
<p>Enfin, pour conclure ce cours, le long duquel nous avons parlé de lien ou de corrélation entre deux variables, il est indispensable d’ajouter un avertissement.</p>
<div class='blocks attention' markdown='1'><div class='blocks-icon'>
<img src='/content/images/attention.svg' alt='bannière attention' />
<p>Attention</p>
</div>
<div class='blocks-text' markdown='1'><p>Il ne faut pas confondre corrélation entre deux variables et lien de cause à effet&nbsp;: mettre en évidence un lien ne suffit absolument pas à conclure que l’évolution d’une variable est la cause de l’évolution de l’autre. Une étude rigoureuse et la plus exhaustive possible est indispensable pour déterminer un lien de causalité.</p>
<p>Par exemple, il y aurait sans doute une corrélation entre le nombre de ventilateurs achetés et la quantité de glaces consommées, mais l’un n’est évidemment pas la cause de l’autre&nbsp;: la véritable cause est plutôt la température extérieure, qui influe sur les deux variables&nbsp;!<br />
En revanche, ce sont des études approfondies qui montrent que les anomalies météorologiques, comme les canicules, sont sans doute dues au réchauffement climatique, lui-même causé par l’activité humaine…</p></div></div>
<div class='conclusion' markdown='1'><p>Conclusion&nbsp;:</p>
<p>Dans ce cours, nous avons donc ajouté une dimension importante aux statistiques sur lesquelles nous avons travaillé jusqu’ici. En effet, mettre en évidence la corrélation entre deux variables est un aspect fondamental dans l’étude de données, même s’il n’est pas suffisant pour conclure à un lien de cause à effet.<br />
Nous avons ici travaillé sur le lien entre deux variables, mais il peut y avoir bien sûr corrélation entre de multiples variables, compliquant la tâche. Il existe en statistique divers 
Nous avons ici travaillé sur le lien entre deux variables, mais il peut y avoir bien sûr corrélation entre de multiples variables, compliquant la tâche. Il existe en statistique divers outils pour traiter de telles données, que certains découvriront durant leurs études supérieures.</p></div></div></div></div></div></div></div><aside class="main-layout-content_main-layout-content__aside__NEKeZ"><div class="title-module_title__SWuRE title-module_title--l__oBGhO title-module_title--bold__TYhW5 main-layout-content_main-layout-content__aside-title__QY9fM">Faisons connaissance</div><a href="https://www.schoolmouv.fr/membre/inscription"><button class=" wide-button-with-profile-module_wide-button-with-profile__9XQx2 wide-button-module_wide-button__ELrg9"><img width="60" height="60" src="https://images.schoolmouv.fr/assets/profiles/profile-bag.svg" alt="profile picture"/><span class="title-module_title__SWuRE title-module_title--s__kIX2v title-module_title--bold__TYhW5 main-layout-content_main-layout-content__aside-profile__LlFbr"><span class="main-layout-content_main-layout-content__aside-text__5xqAt">Je suis un<!-- --> </span>Elève</span></button></a><a href="https://offres.schoolmouv.fr/plans/"><button class=" wide-button-with-profile-module_wide-button-with-profile__9XQx2 wide-button-module_wide-button__ELrg9"><img width="60" height="60" src="https://images.schoolmouv.fr/assets/profiles/profile-case.svg" alt="profile picture"/><span class="title-module_title__SWuRE title-module_title--s__kIX2v title-module_title--bold__TYhW5 main-layout-content_main-layout-content__aside-profile__LlFbr"><span class="main-layout-content_main-layout-content__aside-text__5xqAt">Je suis un<!-- --> </span>Parent</span></button></a><a href="https://www.schoolmouv.fr/professeur"><button class=" wide-button-with-profile-module_wide-button-with-profile__9XQx2 wide-button-module_wide-button__ELrg9"><img width="60" height="60" src="https://images.schoolmouv.fr/assets/profiles/profile-mug.svg" alt="profile picture"/><span class="title-module_title__SWuRE title-module_title--s__kIX2v title-module_title--bold__TYhW5 main-layout-content_main-layout-content__aside-profile__LlFbr"><span class="main-layout-content_main-layout-content__aside-text__5xqAt">Je suis un<!-- --> </span>Enseignant</span></button></a><div class="main-layout-content_main-layout-content__aside-cta__YEGip">Déjà un compte ? <a href="https://www.schoolmouv.fr/membre/connexion" class=" link-module_xxx__aw2PQ link-module_xxx--large__UT8xp link-module_xxx--default__2faDQ link-module_xxx--global__pTt-C"> <!-- -->Je me connecte !</a></div></aside></div><div></div></div></main><footer class="footer-module_footer__YIYIL footer-module_footer__grid__C-ZCT"><div class="footer-module_footer__grid__C-ZCT"><img width="123" height="39" class="footer-module_footer__logo__XmdNx" src="https://images.schoolmouv.fr/assets/logos/logo_schoolmouv_blanc.svg" alt="Schoolmouv"/><div class="footer-module_footer__top-section__EzSmm"><strong class="footer-module_section__title__vLfT7">La plateforme pédagogique la plus complète</strong><p class="footer-module_section__content__xHwFo">SchoolMouv est la 1ere plateforme de<!-- --> <a class="footer-module_footer__underline-links__V1m2Q" href="https://www.schoolmouv.fr/parent">soutien scolaire en ligne</a>. Retrouvez des milliers de ressources pédagogiques, dont des vidéos captivantes. Tout est conforme au programme de l&#x27;Education Nationale et réalisé avec des enseignants.</p></div><div class="footer-module_footer__top-section__EzSmm"><strong class="footer-module_section__title__vLfT7">Gardez-nous à portée de main</strong><p class="footer-module_section__content__xHwFo">(et c’est moins lourd qu’un cartable !)</p><div class="app-rating-module_app-rating__9Sin1"><span vocab="https://schema.org/" typeof="SoftwareApplication"><span class="app-rating-module_app-rating__item__mgtal"><span property="name" style="display:none">SchoolMouv</span><span property="operatingSystem" style="display:none">Web, iOS, Android</span><span property="applicationCategory" content="Éducation" style="display:none">Éducation</span><img class="app-rating-module_app-rating__logo__SDAfh" width="36" height="36" src="https://images.schoolmouv.fr/assets/logos/app_logo.svg" alt=""/><span class="app-rating-module_app-rating__score__a7oNj"><svg class="note__star" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39798 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25092 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path><mask id="mask0_21_8568" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="13"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39797 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25093 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path></mask><g mask="url(#mask0_21_8568)"><rect x="1" y="0.000976562" width="14" height="13" fill="#FFC600"></rect></g></svg><svg class="note__star" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39798 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25092 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path><mask id="mask0_21_8568" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="13"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39797 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25093 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path></mask><g mask="url(#mask0_21_8568)"><rect x="1" y="0.000976562" width="14" height="13" fill="#FFC600"></rect></g></svg><svg class="note__star" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39798 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25092 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path><mask id="mask0_21_8568" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="13"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39797 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25093 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path></mask><g mask="url(#mask0_21_8568)"><rect x="1" y="0.000976562" width="14" height="13" fill="#FFC600"></rect></g></svg><svg class="note__star" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39798 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25092 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path><mask id="mask0_21_8568" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="13"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39797 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25093 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path></mask><g mask="url(#mask0_21_8568)"><rect x="1" y="0.000976562" width="14" height="13" fill="#FFC600"></rect></g></svg><svg class="note__star" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39798 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25092 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path><mask id="mask0_21_8568" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="13"><path d="M6.19277 1.10499C6.59225 0.558635 7.40775 0.558635 7.80723 1.10499L9.36118 3.23026C9.48461 3.39906 9.65784 3.52492 9.85652 3.59014L12.358 4.41129C13.001 4.62239 13.253 5.39797 12.8569 5.94674L11.3158 8.08138C11.1934 8.25093 11.1272 8.45457 11.1266 8.66368L11.1186 11.2964C11.1166 11.9733 10.4568 12.4526 9.81251 12.2454L7.30614 11.4394C7.10706 11.3754 6.89293 11.3754 6.69386 11.4394L4.18749 12.2454C3.54316 12.4526 2.88341 11.9733 2.88136 11.2964L2.87339 8.66368C2.87276 8.45457 2.80659 8.25093 2.68419 8.08138L1.14314 5.94674C0.746967 5.39797 0.99897 4.62239 1.64203 4.41129L4.14348 3.59014C4.34216 3.52492 4.51539 3.39906 4.63882 3.23026L6.19277 1.10499Z" fill="#BEBEBE"></path></mask><g mask="url(#mask0_21_8568)"><rect x="1" y="0.000976562" width="8" height="13" fill="#FFC600"></rect></g></svg><div property="aggregateRating" typeof="AggregateRating"><span property="ratingValue">4.6</span>/<!-- --> <span property="bestRating">5</span> sur <span property="ratingCount">5361</span> avis</div></span></span></span></div></div><hr class="footer-module_footer__separator__Nr55e"/><div class="footer-module_footer__bottom__CvRsR"><strong class="footer-module_section__title__vLfT7">A propos</strong><ul class="footer-module_footer__bottom-section__cNMRW"><li class="footer-module_section__content__xHwFo"><a href="https://equipe.schoolmouv.fr/">Qui sommes-nous ?</a></li><li class="footer-module_section__content__xHwFo"><a href="https://taleez.com/careers/SchoolMouv ">Nous rejoindre</a></li></ul></div><div class="footer-module_footer__bottom__CvRsR"><strong class="footer-module_section__title__vLfT7">Une question</strong><ul class="footer-module_footer__bottom-section__cNMRW"><li class="footer-module_section__content__xHwFo"><a href="https://contact.schoolmouv.fr">Nous contacter</a></li><li class="footer-module_section__content__xHwFo"><a href="https://aide.schoolmouv.fr/knowledge ">Aide</a></li></ul></div><div class="footer-module_footer__top-section__EzSmm"><strong class="footer-module_section__title__vLfT7">Nos produits</strong><ul class="footer-module_footer__bottom-section__cNMRW footer-module_footer__inner-grid__raRb1"><li><span class="footer-module_section__subtitle__I5jhB">SchoolMouv Orientation</span><ul><li class="footer-module_section__content__xHwFo"><a href="https://orientation.schoolmouv.fr">Guide et conseils d&#x27;orientation post Bac</a></li></ul></li><li><span class="footer-module_section__subtitle__I5jhB">SchoolMouv Le blog</span><ul><li class="footer-module_section__content__xHwFo"><a href="https://leblog.schoolmouv.net/ ">Conseils et accompagnement pour les parents d&#x27;élèves</a></li></ul></li><li><span class="footer-module_section__subtitle__I5jhB">Focus Bac avec SchoolMouv</span><ul><li class="footer-module_section__content__xHwFo"><a href="https://focusbac.schoolmouv.fr/collection/?utm_campaign=SMV_footer_HP">La collection de livres pour rester focus jusqu&#x27;au Bac</a></li></ul></li><li><span class="footer-module_section__subtitle__I5jhB">SchoolMouv Manuels</span><ul><li class="footer-module_section__content__xHwFo"><a href="https://pro.schoolmouv.fr/">Manuels scolaires numériques</a></li></ul></li></ul></div><div class="footer-module_footer__socials__XygMb"><a href="https://www.facebook.com/SchoolMouv"><img height="48" width="48" src="https://images.schoolmouv.fr/assets/socials/facebook.svg" alt=""/></a><a href="https://twitter.com/SchoolMouv"><img height="48" width="48" src="https://images.schoolmouv.fr/assets/socials/twitter.svg" alt=""/></a><a href="https://www.instagram.com/schoolmouv"><img height="48" width="48" src="https://images.schoolmouv.fr/assets/socials/instagram.svg" alt=""/></a><a href="https://www.youtube.com/channel/UCDbismsx1l31qVKCiFRWxYA"><img height="48" width="48" src="https://images.schoolmouv.fr/assets/socials/youtube.svg" alt=""/></a></div><ul class="footer-module_footer__links__ZorwW"><li class="footer__link footer__copyright">© SchoolMouv <!-- -->2023</li><li class="footer-module_footer__link__u6kw8"><a href="https://mentions-legales.schoolmouv.fr/">Mentions légales</a></li><li class="footer-module_footer__link__u6kw8"><a href="https://cgv.schoolmouv.fr">Conditions générales de vente</a></li><li class="footer-module_footer__link__u6kw8"><a href="https://charte-donnees-personnelles.schoolmouv.fr">Charte des données</a></li><li class="footer-module_footer__link__u6kw8"><span><a href='javascript:openAxeptioCookies()'>Gérer mes cookies</a></span></li></ul></div></footer></div></div><script id="__NEXT_DATA__" type="application/json">{"props":{"pageProps":{"sheetTemplateProps":{"resource":{"id":"62a85858a1690001004b27bb","isPublished":true,"resourceType":"Cours","createdAt":"2022-06-14T09:43:52.942Z","sheetDetails":{"id":"62a85858a1690001004b27bb","name":"Statistique à deux variables","slug":"statistique-a-deux-variables-quantitatives"},"siblings":[{"resourceId":"62a85858a1690001004b27bb","sort":null,"slug":null,"typeName":"Cours"},{"resourceId":"6256e017e65efb0100362a41","sort":null,"slug":null,"typeName":"Fiche de révision"},{"resourceId":"5fc9186177960a0100196ef4","sort":null,"slug":null,"typeName":"Quiz"}],"name":"Statistique à deux variables quantitatives","slug":"statistique-a-deux-variables-quantitatives","content":[{"title":"","name":"content","content":"[IN]\nIntroduction\u0026nbsp;:\n\nEn statistique, on cherche à étudier l’effet d’un ou de plusieurs paramètres. Les années précédentes, il était question d’étudier en mathématiques une population avec des séries statistiques à une variable.  \nCependant, dans de nombreux cas, les différents paramètres que l’on étudie pour une même population présentent des liens qu’il est important de pouvoir mettre en évidence, même s’il ne faut pas conclure trop vite à un lien de cause à effet. On parle alors de statistiques à plusieurs variables.\n\nAinsi, dans ce cours, nous allons nous intéresser aux séries statistiques à deux variables. \nDans un premier temps, nous définirons ce qu’est une série statistique à deux variables, comment la représenter et quelles données caractéristiques on peut en déduire.  \nEnsuite, afin de mettre en avant les corrélations entre les deux variables, nous expliquerons comment faire un ajustement affine et comment en déduire la droite des moindres carrées, ce qui permettra d’interpoler ou d’extrapoler.  \nEnfin, nous montrerons comment se ramener à un ajustement linéaire avec un changement de variable.\n[/IN] \n\n## Série statistique à deux variables/1\n\nPour étudier simultanément deux variables statistiques, il est possible de définir une série statistique double ou à deux variables.  \nPour cela, on a pour une population donnée de $n$ individus deux caractères quantitatifs\u0026nbsp;:\n* la variable $x$, pour laquelle les données relevées sont $x_1$, $x_2$, …, $x_n$ ;\n* la variable $y$, pour laquelle les données relevées sont $y_1$, $y_2$, …, $y_n$.\n\nChaque individu aura ainsi un couple de caractères associé $(x_i\\ ;\\, y_i)$ (avec $i\\in \\lbrace 1,\\,2,\\,…,\\,n\\rbrace$). Notons que leurs unités seront souvent différentes (par ex., si on s’intéresse au lien entre la température extérieure et la consommation électrique, ou l’évolution d’une population quelconque en fonction du temps).\n* L’ensemble de ces couples constitue une **série statistique à deux variables**.((fleche))\n\n###Tableau de données et nuage de points/a.\n\nOn représente généralement les couples sous forme de tableau avec une colonne (ou une ligne) pour le caractère $x$ et une colonne (ou une ligne) pour le caractère $y$.\n\n[EX]\nNous nous intéressons à l’éventuel lien entre la teneur en carbone, en pourcent, d’un objet et la charge de rupture, c’est-à-dire la charge, en kilogramme, qui provoquera la rupture de l’objet.\n* $10$ essais ont été faits en laboratoire, et nous obtenons les résultats suivants\u0026nbsp;:((fleche))\n\n[TAB]((auto, c, n, texte-centre))\n[LI]\n| **Teneur en carbone $x_i$**\n\u003c\u003c((auto,-25,0,-25,0))(en $\\%$)\u003e\u003e((0,0,0,#52d2fb))  |\n| **Charge de rupture $y_i$**\n\u003c\u003c((auto,-25,0,-25,0))(en $\\text{kg}$)\u003e\u003e((0,0,0,#52d2fb))  |\n[/LI]\n[LI]\n| $64$ |\n| $77$ |\n[/LI]\n[LI]\n| $68$((0,0,0,#abe6ff)) |\n| $81$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $61$ |\n| $72$ |\n[/LI]\n[LI]\n| $71$((0,0,0,#abe6ff)) |\n| $86$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $66$ |\n| $79$ |\n[/LI]\n[LI]\n| $74$((0,0,0,#abe6ff)) |\n| $93$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $63$ |\n| $74$ |\n[/LI]\n[LI]\n| $70$((0,0,0,#abe6ff)) |\n| $86$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $60$ |\n| $70$ |\n[/LI]\n[LI]\n| $62$((0,0,0,#abe6ff)) |\n| $71$((0,0,0,#abe6ff)) |\n[/LI]\n[/TAB]\n\n[/EX]\n\nLa plupart du temps, nous représentons ces données par un **nuage de points**, qui nous permet de mieux visualiser les données.\n\n[DEF]\n**Nuage de points\u0026nbsp;:**\n\nOn représente une série statistique à deux variables $x$ et $y$ par un nuage de points dans un repère orthogonal $(O\\ ;\\, I,\\,J)$, constitué de points $M_i$ de coordonnées $(x_i\\ ;\\, y_i)$, $x_i$ et $y_i$ étant respectivement les valeurs des variables $x$ et $y$ pour l’individu $i$ ($i$ allant de $1$ à $n$, avec $n$ la taille de la population).\n[/DEF]\n\n[EX]\nDonnons le nuage de points correspondant aux données de notre exemple\u0026nbsp;:\n\n[IMG]((100))\n![Teneur en carbone et charge de rupture](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img01.png?1649859889886) #Teneur en carbone et charge de rupture\n[/IMG]\n\n[/EX]\n\nLe nuage de points que nous venons de représenter montre que les points ne semblent pas répartis au hasard.\n* Les deux variables semblent avoir une corrélation\u0026nbsp;: en effet, quand la teneur en carbone augmente, la charge de rupture paraît augmenter aussi. ((fleche))\n\nMais comment rendre cette corrélation plus évidente et, surtout, comment la quantifier\u0026nbsp;?\n\n### Point moyen/b.\n\nTout d’abord, nous pouvons calculer la moyenne des deux variables, ce qui nous permet de définir le **point moyen**.\n\n[DEF]\n**Point moyen\u0026nbsp;:**\n\nSoit une série statistique à deux variables $x$ et $y$, représentée par un nuage de points dans un repère $(O\\ ;\\, I,\\,J)$.  \nOn définit le point moyen de ce nuage comme le point $G$, de coordonnées $(\\bar{x}\\ ;\\,  \\bar{y})$, où\u0026nbsp;:\n* $\\bar{x}$ est la moyenne arithmétique des valeurs $x_i$ associées à la variable $x$ ;\n* $\\bar{y}$ est la moyenne arithmétique des valeurs $y_i$ associées à la variable $y$.\n[/DEF]\n\nNous savons calculer ces moyennes grâce aux formules suivantes.\n\n[RAP]\nSoit  $x_1$, $x_2$ …, $x_n$ les $n$ valeurs de la variable $x$, et $y_1$, $y_2$, …, $y_n$ les $n$ valeurs de la variable $y$.  \nNous avons alors\u0026nbsp;:\n\n$$\\begin{aligned}\n\\bar{x}\u0026=\\dfrac{x_1+x_2+x_3+…+x_n}{n} \\\\\n\\bar{y}\u0026=\\dfrac{y_1+y_2+y_3+…+y_n}{n}\n\\end{aligned}$$\n[/RAP]\n\n[EX]\nContinuons de filer notre exemple, et calculons les moyennes de $x$ et $y$ :\n\n$$\\begin{aligned}\n\\bar x\u0026=\\dfrac{64+68+61+71+66+74+63+70+60+62}{10} \\\\\n\u0026=\\dfrac {659}{10} \\\\\n\u0026=65,9 \\\\\n\\\\\n\\bar y\u0026=\\dfrac{77+81+72+86+79+93+74+86+70+71}{10} \\\\\n\u0026=\\dfrac {789}{10} \\\\\n\u0026=78,9 \n\\end{aligned}$$\n\n* Le point moyen $G$ a donc pour coordonnées $(65,9\\ ;\\, 78,9)$.((fleche))\n\n[IMG]((100))\n![Point moyen](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img02.png?1649859933962) #Point moyen\n[/IMG]\n\n[/EX]\n\n### Covariance et coefficient de corrélation/c.\n\nDans les classes précédentes, nous avons appris à calculer un indicateur qui permet de mesurer la dispersion des données d’une série statistique autour de sa moyenne.\n* Il s’agit de la **variance**.((fleche))\n\n[RAP]\nSoit une série statistique à une variable $x$, d’effectif $n$ : $(x_1,\\,x_2,\\,…,\\,x_n)$, de moyenne $\\bar x$.  \nLa variance de $x$, que nous notons ici $\\text{var}(x)$, est donnée par la formule\u0026nbsp;:\n\n$$\\begin{aligned}\n\\text{var}(x)\u0026=\\dfrac 1n \\sum_{i=1}^n (x_i-\\bar x)^2 \\\\\n\u0026=\\dfrac 1n\\big((x_1-\\bar x)^2+(x_2-\\bar x)^2+…+(x_n-\\bar x)^2\\big)\n\\end{aligned}$$\n\nRappelons aussi que nous définissons l’écart-type de $x$, noté $\\sigma(x)$, ainsi\u0026nbsp;:\n\n$$\\sigma(x)=\\sqrt{\\text{var}(x)}$$\n[/RAP]\n\nLa **covariance**, elle, est une notion nouvelle, qui vient avec la notion de statistique à deux variables. Comme son nom l’indique, elle mesure la façon dont évoluent conjointement les deux variables considérées.\n\n[DEF]\n**Covariance de $(x\\ ;\\, y)$ :**\n\nSoit une série statistique à deux variables $x$ et $y$, d’effectif $n$ : $\\big((x_1\\ ;\\, y_1),\\,(x_2\\ ;\\, y_2),\\,…,\\,(x_n\\ ;\\, y_n)\\big)$, respectivement de moyennes $\\bar x$ et $\\bar y$.  \nLa covariance de $(x\\ ;\\, y)$, notée ici $\\text{cov}(x\\ ;\\, y)$, est donnée par la formule\u0026nbsp;:\n\n$$\\begin{aligned}\n\\text{cov}(x)\u0026=\\dfrac 1n \\sum_{i=1}^n (x_i-\\bar x)(y_i-\\bar y) \\\\\n\u0026=\\dfrac 1n\\big((x_1-\\bar x)(y_1-\\bar y)+(x_2-\\bar x)(y_2-\\bar y)+…+(x_n-\\bar x)(y_n-\\bar y)\\big)\n\\end{aligned}$$\n[/DEF]\n\nNous le voyons, pour calculer la variance, nous effectuons une somme de produits entre deux grandeurs qui ne sont pas, la plupart du temps, exprimées dans la même unité.\n* Nous allons donc définir un **coefficient de corrélation**, qui sera plus explicite.((fleche))\n\n[DEF]\n**Coefficient de corrélation\u0026nbsp;:**\n\nSoit une série statistique à deux variables\n* $x$, de variance $\\text{var}(x)$ et d’écart-type $\\sigma(x)$,\n* et $y$, de variance $\\text{var}(y)$ et d’écart-type $\\sigma(y)$.\n\nSoit $\\text{cov}(x\\ ;\\, y)$ la covariance de $x$ et $y$.  \nLe coefficient de corrélation $r$, aussi noté $\\rho_{xy}$, est alors défini par\u0026nbsp;:\n\n$$\\begin{aligned}\nr\u0026=\\dfrac{\\text{cov}(x\\ ;\\, y)}{\\sqrt{\\text{var}(x)\\text{var}(y)}} \\\\\n\u0026=\\dfrac{\\text{cov}(x\\ ;\\, y)}{\\sigma(x)\\sigma(y)}\n\\end{aligned}$$\n[/DEF]\n\n[RETENIR]\nCe coefficient indique le lien, linéaire, qui existe entre les variables $x$ et $y$ :\n* il appartient à l’intervalle $[-1\\ ;\\, 1]$ ;\n* plus il est proche des bornes de l’intervalle $-1$ et $1$, plus la corrélation linéaire entre $x$ et $y$ est forte\u0026nbsp;;\n* en revanche, deux variables indépendantes ont un coefficient de corrélation proche de $0$ ;\n* s’il est positif, alors $x$ et $y$ varient «\u0026nbsp;dans le même sens\u0026nbsp;» (plus les valeurs de $x$ grandissent, plus celles de $y$ grandissent)\u0026nbsp;;\n* s’il est négatif, alors $x$ et $y$ varient «\u0026nbsp;en sens contraires\u0026nbsp;» (plus les valeurs de $x$ grandissent, plus celles de $y$ diminuent).\n[/RETENIR]\n\nNous allons maintenant calculer tous ces indicateurs dans le cas de notre exemple et en tirer une première conclusion.\n\n[EX]\n* Rappelons que nous avons trouvé\u0026nbsp;: $\\bar x=65,9$ et $\\bar y=78,9$.((bulle,1))\n* Calculons d’abord les variances et écarts-types de $x$ et $y$ :((bulle,2))\n\n$$\\begin{aligned}\n\\text{var}(x)\u0026=\\dfrac{1}{10}\\times \\big((x_1-\\bar x)^2+ (x_2-\\bar x)^2+…+ (x_9-\\bar x)^2+ (x_{10}-\\bar x)^2 \\big) \\\\\n\u0026=\\dfrac 1{10}\\times \\big((64-65,9)^2+(68-65,9)^2+…+(60-65,9)^2+(62-65,9)^2\\big) \\\\\n\u0026=\\dfrac 1{10}\\times \\big((-1,9)^2+2,1^2+…+(-5,9)^2+(-3,9)^2\\big) \\\\\n\u0026=\\dfrac 1{10}\\times (3,61+4,41+…+34,81+15,21) \\\\\n\u0026=\\dfrac{198,9}{10} \\\\\n\u0026=19,89 \\\\\n\\sigma(x)\u0026=\\sqrt{19,89} \\\\\n\u0026\\approx 4,4598 \\\\\n\\\\\n\\text{var}(y)\u0026=\\dfrac{1}{10}\\times \\big((y_1-\\bar y)^2+ (y_2-\\bar y)^2+…+ (y_9-\\bar y)^2+ (y_{10}-\\bar y)^2 \\big) \\\\\n\u0026=\\dfrac 1{10}\\times \\big((77-78,9)^2+(81-78,9)^2+…+(70-78,9)^2+(71-78,9)^2\\big) \\\\\n\u0026=\\dfrac{520,9}{10} \\\\\n\u0026=52,09 \\\\\n\\sigma(x)\u0026=\\sqrt{19,89} \\\\\n\u0026\\approx 7,2173 \\\\\n\\end{aligned}$$\n\n* Intéressons-nous à la covariance de $x$ et $y$ :((bulle,3))\n\n$$\\begin{aligned}\n\\text{cov}(x\\ ;\\, y)\u0026=\\dfrac 1{10}\\big((x_1-\\bar x) (y_1-\\bar y)+ (x_2-\\bar x) (y_2-\\bar y)+…+ (x_{10}-\\bar x) (y_{10}-\\bar y)\\big) \\\\\n\u0026=\\dfrac 1{10}\\big(3,61+4,41+…+30,81) \\\\\n\u0026=\\dfrac{318,9}{10} \\\\\n\u0026=31,89\n\\end{aligned}$$\n\n* Nous pouvons calculer maintenant le coefficient de corrélation\u0026nbsp;:((bulle,4))\n\n$$\\begin{aligned}\nr\u0026=\\dfrac{\\text{cov}(x\\ ;\\, y)}{\\sqrt{\\text{var}(x)\\text{var}(y)}} \\\\\n\u0026=\\dfrac {31,89}{\\sqrt{19,89\\times 52,09}} \\\\\n\u0026\\approx 0,9907\n\\end{aligned}$$\n\n* Nous pouvons en déduire que $x$ et $y$ ont une corrélation linéaire forte ($r\\approx 1$)\u0026nbsp;; en outre, quand les valeurs prises par $x$ croissent, celles prises par $y$ croissent également ($r\u003e0$).((bulle,C))\n[/EX]\n\n##Ajustement affine/2 \n\nCe qui va suivre a pour but de donner des outils pour effectuer des prévisions pour des valeurs inconnues, que celles-ci soient dans le domaine d’étude ou en dehors.\n* On parle alors d’**interpolation** et d’**extrapolation**.((fleche))\n\n[DEF]\n**Interpolation et extrapolation\u0026nbsp;:**\n\n* Lorsque l’on s’intéresse à des valeurs inconnues mais qui font partie du domaine couvert par les données fournies par l’étude, alors on effectue une interpolation.\n* Si l’on travaille hors de ce domaine, alors on effectue une extrapolation.\n[/DEF]\n\n[EX]\nPour la teneur en carbone de nos objets et la charge de rupture associée\u0026nbsp;:\n* nous pouvons vouloir estimer la charge de rupture d’un objet dont la teneur de carbone est de $69,2\\,\\%$, valeur de $x$ qui est bien comprise entre le minimum ($60\\,\\%$) et le maximum ($74\\,\\%$) de la série,\n* nous effectuerons alors une interpolation\u0026nbsp;;((fleche))\n* nous pouvons aussi avoir besoin d’avoir une approximation de la charge de rupture pour un objet de teneur $50\\,\\%$ ;\n* nous pouvons encore souhaiter connaître la teneur en carbone de l’objet pour que la charge de rupture soit de $100\\ \\text{kg}$ ;\n* dans ces deux derniers cas, nous ferons une extrapolation.((fleche))\n[/EX]\n\n[ATT]\nEffectuer une extrapolation peut être dangereux\u0026nbsp;: en effet, rien ne démontre que le modèle déduit des données fournies reste vrai en dehors de ce domaine.\n* Il conviendra donc toujours de prendre des précautions.((fleche))\n[/ATT]\n\nPar exemple, si un commerçant s’intéresse au chiffre d’affaires qu’il fait en fonction de l’heure de la journée et qu’il ne relève les données qu’entre 17\u0026nbsp;heures et 19\u0026nbsp;heures, alors, ce créneau correspondant à la sortie des bureaux et donc à ses heures de grande fréquentation, il ne pourra extrapoler le chiffre fait lors des horaires «\u0026nbsp;creux\u0026nbsp;», à\u0026nbsp;14\u0026nbsp;h\u0026nbsp;30, par exemple.\n\n### Ajustement affine/a.\n\nLorsqu’un lien linéaire semble apparaître entre deux variables, et afin de pouvoir faire des interpolations et des extrapolations, il est intéressant d’ajuster le nuage de points au moyen d’une droite et de caractériser ainsi la relation affine entre les deux variables.\n* On parle d’**ajustement affine**.((fleche))\n\n[DEF]\n**Ajustement affine\u0026nbsp;:**\n\nLe principe de l’ajustement affine est de tracer, lorsque les points d’un nuage semblent globalement alignés, une droite passant «\u0026nbsp;au plus près\u0026nbsp;» de ces points.\n* Cette droite est alors appelée droite d’ajustement, ou droite de régression.((fleche))\n[/DEF]\n\nRemarquons que «\u0026nbsp;au plus près\u0026nbsp;» est une formulation assez vague. Il existe plusieurs techniques.\n* Nous allons en présenter deux\u0026nbsp;: une à partir de la notion de point moyen, la **méthode de\u0026nbsp;Mayer**, et l’autre, très utilisée, dite **méthode des moindres carrés**.((fleche))\n\n### Méthode de\u0026nbsp;Mayer/b.\n\nCette méthode, aussi appelée méthode des points moyens, consiste tout simplement à relier deux points moyens du nuage. Elle n’est guère fiable, car elle est notamment sensible aux valeurs extrêmes, mais elle a le mérite d’être simple et rapide.\n\n[RETENIR]\n**Méthodologie\u0026nbsp;:**\n\n* On divise le nuage en $2$ groupes de points de même effectif (ou l’un avec un point supplémentaire, si l’effectif est impair).((bulle,1))\n* On calcule le point moyen de ces $2$ groupes.((bulle,2))\n* On relie ces $2$ points moyens pour obtenir la droite d’ajustement.((bulle,3))\n* On peut aussi, si besoin, connaissant les coordonnées de $2$ points, déterminer l’équation de la droite.((bulle,4))\n[/RETENIR]\n\nAppliquons-la rapidement à notre exemple.\n\n[EX]\n* Nous considérons les données par ordre croissant de la teneur en carbone, et scindons donc les points en deux groupes de $5$ couples\u0026nbsp;:((bulle,1))\n\n$$\\begin{aligned}\n\\textcolor{#A9A9A9}{\\text{Groupe 1\\ : }} (60\\ ;\\, 70),\\,(61\\ ;\\, 72),\\,(62,\\,71),\\,(63,\\,74),\\,(64,\\,77) \\\\\n\\textcolor{#A9A9A9}{\\text{Groupe 2\\ : }} (66\\ ;\\, 79),\\,(68\\ ;\\, 81),\\,(70,\\,86),\\,(71,\\,86),\\,(74,\\,93)\n\\end{aligned}$$\n\n* Nous calculons les coordonnées des points moyens $G_1$ et $G_2$, respectivement des groupes\u0026nbsp;1 et\u0026nbsp;2\u0026nbsp;:((bulle,2))\n\n$$\\begin{aligned}\n\\bar x_1\u0026= \\dfrac{60+61+62+63+64}5 \\\\\n\u0026=62 \\\\\n\\bar y_1\u0026= \\dfrac{70+72+71+74+77}5 \\\\\n\u0026=72,8 \\\\\n\\\\\n\\bar x_2\u0026= \\dfrac{66+68+70+71+74}5 \\\\\n\u0026=69,8 \\\\\n\\bar y_2\u0026= \\dfrac{79+81+86+86+93}5 \\\\\n\u0026=85\n\\end{aligned}$$\n\n* Nous obtenons donc $G_1$ de coordonnées $(62\\ ;\\, 72,8)$ et $G_2$ de coordonnées $(69,8\\ ;\\, 85)$.((fleche))\n* Nous représentons ces points et les relions par une droite, qui sera donc notre droite d’ajustement.((bulle,3))\n\n[IMG]((100))\n![Ajustement affine par la méthode de Mayer](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img03.png?1649860008302) #Ajustement affine par la méthode de Mayer\n[/IMG]\n\n* Nous pouvons remarquer que le point moyen $G$ appartient à cette droite.((fleche))\n[/EX]\n\n### Droite des moindres carrés/c.\n\nNous allons maintenant aborder la méthode la plus utilisée pour effectuer un ajustement affine\u0026nbsp;: la **méthode des moindres carrés**.\n\nPour bien la comprendre, considérons un nuage simple de points $M_i\\, (x_i\\ ;\\, y_i)$, représentons aussi une droite, qui passe par le point moyen $G$ et dont l’équation est de la forme $y=ax+b$.\n* Nous considérons en outre les points $P_i$ de la droite, d’abscisse $x_i$ et d’ordonnée $ax_i+b$.((fleche))\n\n[IMG]((100))\n![Ajustement affine par la droite des moindres carrés](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img04.png?1649860096370) #Ajustement affine par la droite des moindres carrés\n[/IMG]\n\nCe qui nous intéresse, c’est la distance entre les points $M_i$ et $P_i$ associés.\n* Dans le schéma ci-dessus, nous avons explicité cette distance entre les points $M_3$ et $P_3$. De la même façon, pour tout $i$ compris entre $1$ et $5$, nous avons\u0026nbsp;:((fleche))\n\n$$M_iP_i=\\vert y_i-(ax_i+b)\\vert$$\n\n[RETENIR]\nSoit un nuage de $n$ points, qui représente une série statistique à deux variables.  \nDéterminer la **droite des moindres carrés** consiste à trouver la droite qui minimise le carré des distances $M_iP_i$ ($i\\in \\lbrace 1,\\,…,\\,n\\rbrace$).\n* Il s’agit donc de déterminer les réels $a$ et $b$ tels que la somme suivante soit minimale\u0026nbsp;:((fleche))\n\n$$\\begin{aligned}\n\\sum_{i=1}^n \\big(y_i-(ax_i+b)\\big)^2 = \\big(y_1-(ax_1+b)\\big)^2+…+\\big(y_n-(ax_n+b)\\big)^2 \\\\\n\\end{aligned}$$\n\n* Nous parlons aussi de **droite d’ajustement de $y$ en $x$**.((fleche))\n[/RETENIR]\n\nPour cela, nous allons admettre la propriété suivante.\n\n[PROP]\nSoit une série statistique à deux variables\u0026nbsp;:\n* $x$, de moyenne $\\bar x$, de variance $\\text{var}(x)$ et d’écart-type $\\sigma(x)$,\n* et $y$, de moyenne $\\bar y$, de variance $\\text{var}(y)$ et d’écart-type $\\sigma(y)$.\n\nSoit $\\text{cov}(x\\ ;\\, y)$ la covariance de $x$ et $y$.  \nLa droite des moindres carrés, ou droite d’ajustement de $y$ en $x$, a pour équation $y=ax+b$ où\u0026nbsp;:\n\n$$\\begin{aligned}\na\u0026=\\dfrac {\\text{cov}(x\\ ;\\, y)}{\\text{var}(x)} \\\\\n\u0026=\\dfrac {\\text{cov}(x\\ ;\\, y)}{\\sigma^2(x)} \\\\\nb\u0026=\\bar y-a\\bar x\n\\end{aligned}$$\n[/PROP]\n\nRésumons ce qui précède en donnant une méthodologie à suivre, lorsqu’un exercice demande de déterminer la droite d’ajustement par la méthode des moindres carrés.\n\n[RETENIR]\n**Méthodologie d’ajustement affine par la méthode des moindres carrés\u0026nbsp;:**\n\nSoit une série statistique à deux variables $x$ et $y$.\n* Si nécessaire, représenter le nuage de points $(x_i\\ ;\\, y_i)$ dans un repère orthogonal.((bulle,1))\n* Calculer les moyennes $\\bar x$ et $\\bar y$ des deux variables.((bulle,2))\n* Placer le cas échéant le point moyen $G\\,(\\bar x\\ ;\\, \\bar y)$ dans la représentation.((fleche))\n* Calculer les variances $\\text{var}(x)$ et $\\text{var}(y)$ des deux variables.((bulle,3))\n* Calculer la covariance $\\text{cov}(x\\ ;\\, y)$ des deux variables\u0026nbsp;:((bulle,4))\n\n$$\\text{cov}(x\\ ;\\, y)= \\dfrac 1n \\sum_{i=1}^n (x_i-\\bar x)(y_i-\\bar y)$$\n\n* Déduire l’équation de la droite d’ajustement de $y$ en $x$ :((bulle,5))\n\n$$y=\\dfrac {\\text{cov}(x\\ ;\\, y)}{\\text{var}(x)}\\cdot x + \\bar y-a\\bar x$$\n\n* Calculer le coefficient de corrélation\u0026nbsp;:((bulle,6))\n\n$$r=\\dfrac{\\text{cov}(x\\ ;\\, y)}{\\sqrt{\\text{var}(x)\\text{var}(y)}}$$\n\n* En théorie, un ajustement affine est toujours possible, mais il est indispensable de mesurer sa pertinence\u0026nbsp;; le calcul du coefficient de corrélation est donc important pour pouvoir en juger.((fleche))\n* Plus $r$ est proche en valeur absolue de $1$, plus la corrélation linéaire est forte, et donc plus l’ajustement affine est pertinent.((fleche))\n* Si l’ajustement s’avère suffisamment pertinent, alors on peut s’en servir pour effectuer\u0026nbsp;:((bulle,7))\n* des __inter__polations («\u0026nbsp;__entre__\u0026nbsp;» les données de la série),((fleche))\n* des __extra__polations («\u0026nbsp;__hors__\u0026nbsp;» des données de la série).((fleche))\n[/RETENIR]\n\nAppliquons cette méthode, toujours à notre exemple.\n\n[EX]\nNous avons déjà représenté le nuage de points (étape\u0026nbsp;1) et calculé les résultats pour les points\u0026nbsp;2,\u0026nbsp;3 et\u0026nbsp;4\u0026nbsp;:\n\n[TAB]((auto, c, n, texte-centre))\n[LI]\n| **Variable**((25,0,0,#52d2fb)) |\n| **Moyenne**((25,0,0,#FFE4B5)) |\n| **Variance**((25,0,0,#52d2fb)) |\n| **Covariance**((25,0,0,#FFE4B5)) |\n[/LI]\n[LI]\n| $x$((0,0,0,#abe6ff)) |\n| $\\bar x = 65,9$((0,0,0,#FFEFD5)) |\n| $\\text{var}(x)=19,89$((0,0,0,#abe6ff)) |\n| $\\text{cov}(x\\ ;\\,y)=31,89$((0,0,2,#FFEFD5)) |\n[/LI]\n[LI]\n| $y$((0,0,0,#abe6ff)) |\n| $\\bar y=78,9$((0,0,0,#FFEFD5)) |\n| $\\text{var}(y)=52,09$((0,0,0,#abe6ff)) |\n[/LI]\n[/TAB]\n\n* Nous en déduisons les valeurs de $a$ et $b$ de l’équation réduite $y=ax+b$ de la droite des moindres carrés\u0026nbsp;:((bulle,5))\n\n$$\\begin{aligned}\na\u0026=\\dfrac {\\text{cov}(x\\ ;\\, y)}{\\text{var}(x)} \\\\\n\u0026=\\dfrac {31,89}{19,89} \\\\\n\u0026=\\dfrac{1\\,063}{663} \\\\\n\u0026\\approx 1,603 \\\\\n\\\\\nb\u0026=\\bar y-a\\bar x \\\\\n\u0026=78,9-\\dfrac{1\\,063}{663}\\times 65,9 \\\\\n\u0026\\approx -26,759\n\\end{aligned}$$\n\n* Nous en déduisons l’équation de la droite d’ajustement, en arrondissant à $10^{-3}$ près\u0026nbsp;:((fleche))\n\n$$y=1,603\\,x-26,759$$\n\n* Nous avons aussi trouvé le coefficient de corrélation entre $x$ et $y$ :((bulle,6))\n\n$$r\\approx 0,9907$$\n\n* Nous l’avons déjà dit, mais reprécisons que l’ajustement affine est ici pertinent\u0026nbsp;; en outre, quand $x$ grandit, $y$ grandit aussi.((fleche))\n* Nous allons maintenant nous servir de cette équation pour effectuer quelques prévisions.((bulle,7))\n\nCalculons d’abord la charge de rupture prévue par notre modèle pour un objet dont la teneur en carbone a été mesurée à $x^{\\prime}=69,2\\,\\%$.\n* Il s’agit d’une interpolation.((fleche))\n\nIl suffit de remplacer, dans l’équation, $x$ par la valeur donnée (pour plus de rigueur, nous utilisons les expressions exactes, et non les arrondis)\u0026nbsp;:\n\n$$\\begin{aligned}\ny^{\\prime} \u0026= \\dfrac{1\\,063}{663} \\times 69,2 + 78,9-\\dfrac{1\\,063}{663}\\times 65,9 \\\\\n\u0026\\approx 84,191\n\\end{aligned}$$\n\n* Le modèle prévoit une charge de rupture d’environ $84\\ \\text{kg}$.((fleche))\n\nNous souhaitons maintenant que la charge de rupture de notre objet soit de $y^{\\prime\\prime}=100\\ \\text{kg}$. Quelle teneur en carbone doit-il avoir\u0026nbsp;?\n* Nous voyons que $100\\ \\text{kg}$ est hors de notre domaine d’étude, il s’agit donc d’une extrapolation.((fleche))\n\nNous remplaçons cette fois $y$ par la valeur donnée\u0026nbsp;:\n\n$$\\begin{aligned}\n100 \u0026= \\dfrac{1\\,063}{663} \\times x^{\\prime\\prime} + 78,9-\\dfrac{1\\,063}{663}\\times 65,9 \\\\\n\\Leftrightarrow x^{\\prime\\prime}\u0026=\\dfrac{663}{1\\,063}\\times \\left( 21,1+\\dfrac{1\\,063}{663}\\times 65,9 \\right) \\\\\n\u0026=\\dfrac{663}{1\\,063}\\times 21,1+ 65,9 \\\\\n\u0026\\approx 79,060\n\\end{aligned}$$\n\n* Avec une teneur de $80\\ \\%$, nous pouvons supposer que l’objet résistera à une charge de $100\\ \\text{kg}$.((fleche))\n\nRemarquons que, si nous avions posé la même question pour une charge de $200\\ \\text{kg}$, nous aurions trouvé une teneur en carbone d’environ $141\\ \\%$... Ce qui, en l’occurrence, serait un non-sens physique.\n* Nous nous heurtons ici à une des limites de l’extrapolation.((fleche))\n[/EX]\n\nCi-dessus, nous avons effectué «\u0026nbsp;manuellement\u0026nbsp;» les calculs complets (à quelques points de suspension près), car il est important de bien comprendre le principe (et aussi parce que des exercices le demandent). À cet effet, nous avons travaillé avec un nombre restreint de données (effectif de $10$).\n\nEn pratique, pour pouvoir effectuer des prévisions dignes de confiance et afin d’avoir un modèle mathématique d’ajustement le plus précis possible, nous devons disposer de beaucoup de données.  \nBien sûr, dans de tels cas, calculer «\u0026nbsp;manuellement\u0026nbsp;» les moyennes, les variances, la covariance, etc., serait une tâche titanesque (et parfaite pour les erreurs de calcul…).\n* Nous nous servons alors de notre calculatrice ou, mieux encore, d’un tableur pour déterminer directement les indicateurs et même l’équation de la droite des moindres carrés.((fleche))\n\n[AST]\nServons-nous de notre exemple pour montrer les fonctions concernant les statistiques à deux variables, et ce sur les tableurs les plus utilisés\u0026nbsp;: Calc d’OpenOffice et Microsoft Excel.  \nSur une feuille, nous avons au préalable entré les $(x_i\\ ;\\, y_i)$ sur deux colonnes (nous aurions aussi pu les mettre sur deux lignes)\u0026nbsp;:\n* les valeurs prises par $x$ sont dans les cellules $\\green{\\text A 1}$ à $\\green{\\text A 10}$ ;\n* les valeurs prises par $y$ sont dans les cellules $\\purple{\\text B 1}$ à $\\purple{\\text B 10}$.\n* Pour calculer les principaux indicateurs\u0026nbsp;:((bulle,1))\n\n[TAB]((100, c, n, texte-centre))\n[LI]\n| **Variable**((30,0,0,#52d2fb)) |\n| $x$((35,0,0,#abe6ff)) |\n| $y$((35,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| **Moyenne**((0,0,0,#FFE4B5)) |\n| $$\\small \\text{MOYENNE(\\red{A1:A10})}$$((0,0,0,#FFEFD5)) |\n| $$\\small \\text{MOYENNE(\\purple{B1:B10})}$$((0,0,0,#FFEFD5)) |\n[/LI]\n[LI]\n| **Variance**((0,0,0,#52d2fb)) |\n| $$\\small \\text{VAR.P(\\red{A1:A10})}$$((0,0,0,#abe6ff)) |\n| $$\\small \\text{VAR.P(\\purple{B1:B10})}$$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| **Covariance**((0,0,0,#FFE4B5)) |\n| $$\\small \\text{COVARIANCE(\\red{A1:A10}\\,;\\,\\purple{B1:B10})}$$((0,2,0,#FFEFD5)) |\n[/LI]\n[LI]\n| **Coef. de corrélation** $r$((0,0,0,#52d2fb)) |\n| $$\\small \\text{COEFFICIENT.CORRELATION(\\red{A1:A10}\\,;\\,\\purple{B1:B10})}$$((0,2,0,#abe6ff)) |\n[/LI]\n[LI]\n| **Droite d’ajustement**\n$$y=ax+b$$((0,0,2,#FFE4B5))  |\n| $$\\small \\textcolor{#A9A9A9} {a\\ :\\ }\\text{PENTE(\\purple{B1:B10}\\,;\\,\\red{A1:A10})}$$((0,2,0,#FFEFD5)) |\n[/LI]\n[LI]\n| $$\\small \\textcolor{#A9A9A9} {b\\ :\\ }\\text{ORDONNEE.ORIGINE(\\purple{B1:B10}\\,;\\,\\red{A1:A10})}$$((0,2,0,#FFEFD5)) |\n[/LI]\n[/TAB]\n\n* Pour représenter le nuage de points\u0026nbsp;:((bulle,2))\n* sélectionner les plages de données\u0026nbsp;;((liste2))\n* insérer le graphique «\u0026nbsp;Nuages de points\u0026nbsp;»\u0026nbsp;:((liste2))\n* avec Calc\u0026nbsp;: [2]Insertion[2] / [2]Diagramme[2] / [2]XY (dispersion)[2] / [2]Points seuls[2],((liste3))\n* avec Excel\u0026nbsp;: [2]Insérer[2] / [2]Graphique[2] / [2]XY (nuage de points)[2].((liste3))\n* Pour tracer la droite de régression, une fois le nuage de points réalisé\u0026nbsp;:((bulle,3))\n* avec Calc, le diagramme étant sélectionné (double-clic dessus, si nécessaire)\u0026nbsp;: [2]Insertion[2] / [2]Courbe de tendance[2] / [2]Linéaire[2], et cocher\u0026nbsp;: [2]Afficher l’équation[2]\u0026nbsp;;((liste2))\n* avec Excel\u0026nbsp;: [2]Clic droit sur un point du graphique[2] / [2]Ajouter une courbe de tendance[2] / [2]Linéaire[2], et cocher\u0026nbsp;: [2]Afficher l’équation sur le graphique[2].((liste2))\n[/AST]\n\n## Ajustement affine par changement de variable /3\n\nDans certains cas, les points du nuage ne peuvent être considérés comme alignés, car l’approximation serait de manière évidente beaucoup trop grande.\n* Il ne faut pas pour autant conclure qu’il n’y a pas de corrélation entre les deux variables\u0026nbsp;: il peut y avoir un lien, mais qui n’est pas linéaire.((fleche))\n\nParfois, il est toutefois possible d’étudier ce lien non linéaire au moyen d’un ajustement affine, et ce grâce à un **changement de variable**.  \nDans cette dernière partie, nous allons donc montrer cette méthode à travers l’exemple de l’évolution d’une population, que vous avez déjà abordée, ou que vous aborderez bientôt, en \u003ca target=\"_blank\" href=\"/cours/modeles-demographiques-comprendre-l-evolution-quantitative-des-populations/fiche-de-cours\"\u003eenseignement scientifique\u003c/a\u003e (nous considérerons dans ce cours que le temps est une donnée continue, tandis que, dans le cours d’enseignement scientifique, nous travaillons par palier entier, et donc de manière discrète).\n\n### Un nuage de points non alignés/a.\n\nNous disposons, pour une région, du recensement décennal de la population, sur tout le XX^^e^^\u0026nbsp;siècle, exprimé en million et arrondi à la dizaine de milliers (la précision est meilleure après les années\u0026nbsp;50)\u0026nbsp;:\n\n[TAB]((auto, c, n, texte-centre))\n[LI]\n| **Année $x_i$**((50,0,0,#52d2fb)) |\n| **Population $y_i$**\n\u003c\u003c((auto,-25,0,-25,0))(en million)\u003e\u003e((50,0,0,#52d2fb))  |\n[/LI]\n[LI]\n| $0$ |\n| $1,210$ |\n[/LI]\n[LI]\n| $10$((0,0,0,#abe6ff)) |\n| $1,840$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $20$ |\n| $1,810$ |\n[/LI]\n[LI]\n| $30$((0,0,0,#abe6ff)) |\n| $2,890$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $40$ |\n| $4,430$ |\n[/LI]\n[LI]\n| $50$((0,0,0,#abe6ff)) |\n| $4,730$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $60$ |\n| $6,542$ |\n[/LI]\n[LI]\n| $70$((0,0,0,#abe6ff)) |\n| $9,552$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $80$ |\n| $14,264$ |\n[/LI]\n[LI]\n| $90$((0,0,0,#abe6ff)) |\n| $21,252$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $100$ |\n| $22,035$ |\n[/LI]\n[/TAB]\n\n* Servons-nous d’un tableur pour représenter le nuage de points et tracer la droite d’ajustement de $y$ en $x$.((bulle,2))\n\n[IMG]((100))\n![Droite d’ajustement de y en x](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img05.png?1649860283513) #Droite d’ajustement de y en x\n[/IMG]\n\n* Nous voyons que les points du nuage peuvent difficilement être considérés comme alignés. De plus, les distances entre les points et la droite semblent assez grandes.((bulle,3))\n* Donnons néanmoins le coefficient de corrélation, obtenu avec le tableur\u0026nbsp;:((fleche))\n\n$$r\\approx 0,923$$\n\nCelui-ci n’est pas très éloigné de $1$, mais nous ne pouvons pas dire non plus qu’il est «\u0026nbsp;presque égal\u0026nbsp;» à $1$.\n* Un ajustement affine par la méthode des moindres carrés ne semblent pas bien adapté.((bulle,C))\n\nNous pouvons tout de même faire une première remarque.  \nNous constatons une diminution de la population entre les années\u0026nbsp;10 et\u0026nbsp;20, et une stagnation entre les années\u0026nbsp;40 et\u0026nbsp;50.\n* Des événements historiques peuvent expliquer ces points.((fleche))\n\n### Changement de variable/b.\n\nSi les points ne sont pas alignés, nous reconnaissons quand même l’«\u0026nbsp;allure\u0026nbsp;» caractéristique de la courbe représentative de la fonction exponentielle.\n* Nous allons donc considérer une nouvelle variable $z$ définie, pour tout $i \\in \\lbrace1,\\,…,\\,10\\rbrace$, par\u0026nbsp;:((fleche))\n\n$$z_i=\\ln{(y_i)}$$\n\nSi l’évolution de la population suit effectivement une croissance exponentielle, nous pourrons alors mettre en évidence une corrélation linéaire entre $x$ et $z$.  \nNotons que, dans cet exemple, nous nous intéressons aux effectifs d’une population, donc la variable $y$ prendra des valeurs strictement positives, et nous pouvons donc travailler directement avec la fonction logarithme népérien.\n\n* Complétons notre tableau de données avec les valeurs de $z$, arrondies à $10^{-3}$ près\u0026nbsp;:((bulle,1))\n\n[TAB]((auto, c, n, texte-centre))\n[LI]\n| **Année $x_i$**((32,0,0,#52d2fb)) |\n| **Population $y_i$**\n\u003c\u003c((auto,-25,0,-25,0))(en million)\u003e\u003e((34,0,0,#52d2fb))  |\n| **$z_i=\\ln{(y_i)}$**((34,0,0,#52d2fb)) |\n[/LI]\n[LI]\n| $0$ |\n| $1,210$ |\n| $0,191$ |\n[/LI]\n[LI]\n| $10$((0,0,0,#abe6ff)) |\n| $1,840$((0,0,0,#abe6ff)) |\n| $0,610$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $20$ |\n| $1,810$ |\n| $0,593$ |\n[/LI]\n[LI]\n| $30$((0,0,0,#abe6ff)) |\n| $2,890$((0,0,0,#abe6ff)) |\n| $1,061$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $40$ |\n| $4,430$ |\n| $1,488$ |\n[/LI]\n[LI]\n| $50$((0,0,0,#abe6ff)) |\n| $4,730$((0,0,0,#abe6ff)) |\n| $1,554$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $60$ |\n| $6,542$ |\n| $1,878$ |\n[/LI]\n[LI]\n| $70$((0,0,0,#abe6ff)) |\n| $9,552$((0,0,0,#abe6ff)) |\n| $2,257$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $80$ |\n| $14,264$ |\n| $2,658$ |\n[/LI]\n[LI]\n| $90$((0,0,0,#abe6ff)) |\n| $21,252$((0,0,0,#abe6ff)) |\n| $3,056$((0,0,0,#abe6ff)) |\n[/LI]\n[LI]\n| $100$ |\n| $22,035$ |\n| $3,093$ |\n[/LI]\n[/TAB]\n\n* Représentons le nuage de points $(x_i\\ ;\\, z_i)$ et traçons la droite d’ajustement de $z$ en $x$ :((bulle,2))\n\n[IMG]((100))\n![Droite d’ajustement de z en x](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img06.png?1649860367687) #Droite d’ajustement de z en x\n[/IMG]\n\n* Nous nous rendons compte que le nuage a une allure bien plus «\u0026nbsp;allongée\u0026nbsp;» et que les points sont raisonnablement proches de la droite.((bulle,3))\n* Pour confirmer cette impression, donnons le coefficient de corrélation de $(x\\ ;\\, z)$ :((fleche))\n\n$$r^{\\prime}\\approx 0,993$$\n\nLe coefficient est cette fois très proche de $1$.\n* Nous décidons de modéliser l’évolution de la population par un ajustement réalisé grâce à ce changement de variable\u0026nbsp;: $z=\\ln{(y)}$.((bulle,4))\n\nL’équation de la droite d’ajustement de $z$ en $x$ est alors\u0026nbsp;:\n\n$$z=0,030x+0,164$$\n\nNous en déduisons\u0026nbsp;:\n\n$$\\begin{aligned}\n\\ln{(y)}=0,030x+0,164 \u0026\\Leftrightarrow \\text{e}^{\\ln{(y)}}=\\text{e}^{0,030x+0,164} \\\\\n\u0026\\footnotesize{\\textcolor{#A9A9A9}{\\text{[par stricte croissance de $\\exp$]}}} \\\\\n\u0026\\Leftrightarrow y=\\text{e}^{0,030x}\\times \\text{e}^{0,164} \\\\\n\u0026\\footnotesize{\\textcolor{#A9A9A9}{\\text{[car $\\exp$ et $\\ln$ sont réciproques}}} \\\\\n\u0026\\footnotesize{\\textcolor{#A9A9A9}{\\text{et e$^{a+b}=$e$^a\\times$e$^b$]}}}\n\\end{aligned}$$\n\nEn arrondissant à $10^{-3}$ près, nous obtenons finalement\u0026nbsp;:\n\n$$y=1,178\\times \\text{e}^{0,030x}$$\n\n* La fonction $f:x\\mapsto 1,178\\times \\text{e}^{0,030x}$ permet d’ajuster le nuage de points $(x_i\\ ;\\, y_i)$.((fleche))\n\nTraçons sa courbe représentative\u0026nbsp;:\n\n[IMG]((100))\n![Courbe représentative de la fonction f](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img07.png?1649860416229) #Courbe représentative de la fonction f\n[/IMG]\n\n* Servons-nous de ce modèle pour d’abord faire une interpolation.((bulle,5))\n\nNous cherchons à savoir approximativement en quelle année $x^{\\prime}$ la barre des $8$ millions d’habitants a été franchie. Nous décidons de le faire graphiquement.\nPour cela, nous déterminons graphiquement l’abscisse $x^{\\prime}$ du point de la courbe représentative de $f$ d’ordonnée $y^{\\prime}=8$ :\n\n[IMG]((100))\n![Courbe représentative de la fonction f et interpolation](https://images-schoolmouv-cours.s3.eu-west-1.amazonaws.com/t-fnx-maths-c42-img08.png?1649860458007) #Courbe représentative de la fonction f et interpolation\n[/IMG]\n\nNous trouvons\u0026nbsp;: $x^{\\prime}\\approx 64$.\n* La barre des $8$ millions d’habitants a été franchie approximativement à l’année $64$.((fleche))\n\nRemarquons que si nous avions utilisé la modélisation sans changement de variable, nous aurions trouvé environ $49$ ans… ce qui aurait été en contradiction avec les données.\n\n* Effectuons enfin une extrapolation.((bulle,6))\n\nNous nous demandons maintenant quelle sera la population $y^{\\prime\\prime}$ après $120$ ans.  \nNous nous servons bien sûr de la fonction dont nous disposons pour calculer $f(120)$ :\n\n$$\\begin{aligned}\ny^{\\prime\\prime}\u0026=1,178\\times \\text{e}^{0,030\\times 120} \\\\\n\u0026\\approx43,113\n\\end{aligned}$$\n\n* Si nous supposons que ce modèle reste valable hors du domaine des données, nous pouvons prévoir une population d’environ $43$ millions de personnes.((fleche))\n\n[ATT]\nNous avons précisé que nous supposions le modèle valable hors du domaine de la série statistique, et ceci est toujours indispensable.\n\nDans notre exemple, nous constatons un infléchissement de la croissance à l’année $100$.  \nEst-elle simplement conjoncturelle, comme les événements historiques à l’origine de la diminution et du ralentissement que nous avons déjà constatés\u0026nbsp;? Ou cet infléchissement a-t-il des raisons plus profondes et le modèle perd-il sa pertinence pour toute extrapolation\u0026nbsp;?  \nEn effet, en enseignement scientifique, vous avez découvert, ou découvrirez, que la croissance d’une population peut être exponentielle durant des périodes brèves, mais qu’elle se heurte à la limite des ressources disponibles. Ainsi, une population aura tendance à tendre vers un maximum.\n\nIci, nous n’avons pas assez d’informations pour savoir ce qu’il en est précisément.\n* Des données supplémentaires seront à relever au fil du temps, afin d’affiner le modèle.((fleche)) \n[/ATT]\n\n### Récapitulatif/c.\n\nVoici une méthodologie à suivre lorsqu’un changement de variable s’impose.\n\n[RETENIR]\n**Méthodologie\u0026nbsp;:**\n\n* En fonction de l’allure du nuage de points, décider d’un changement de variable.\n* Généralement, les exercices vous guideront dans ce choix. Mais précisons qu’on fera appel surtout aux fonctions usuelles\u0026nbsp;: logarithme, exponentielle, carré, racine carrée…((fleche))\n* Calculer les nouvelles valeurs déduites du changement de variable.\n* Représenter le nouveau nuage de points et tracer la droite d’ajustement.\n* Calculer le coefficient de corrélation correspondant, afin de confirmer la pertinence du changement de variable.((fleche))\n* À partir de la définition de la nouvelle variable, en déduire la fonction d’ajustement des données initiales.\n* Représenter dans le nuage initial la courbe représentative de cette fonction, si l’on souhaite faire graphiquement des interpolations et des extrapolations.((fleche))\n* La définition de cette fonction permet aussi de faire, par le calcul, des interpolations et des extrapolations.((fleche))\n[/RETENIR]\n\nNotons aussi qu’une calculatrice ou un tableur sont aussi capables de déterminer très rapidement la fonction d’ajustement, selon le modèle que vous choisirez.\n\n[AST]\nDans notre exemple de croissance exponentielle, une fois le nuage représenté dans un tableur\u0026nbsp;:\n* avec Calc, le diagramme étant sélectionné (double-clic dessus, si nécessaire)\u0026nbsp;: [2]Insertion[2] / [2]Courbe de tendance[2] / [2]Exponentielle[2], et cocher\u0026nbsp;: [2]Afficher l’équation[2]\u0026nbsp;;((liste2))\n* avec Excel\u0026nbsp;: [2]Clic droit sur un point du graphique[2] / [2]Ajouter une courbe de tendance[2] / [2]Exponentielle[2], et cocher\u0026nbsp;: [2]Afficher l’équation sur le graphique[2].((liste2))\n[/AST]\n\nEnfin, pour conclure ce cours, le long duquel nous avons parlé de lien ou de corrélation entre deux variables, il est indispensable d’ajouter un avertissement.\n\n[ATT]\nIl ne faut pas confondre corrélation entre deux variables et lien de cause à effet\u0026nbsp;: mettre en évidence un lien ne suffit absolument pas à conclure que l’évolution d’une variable est la cause de l’évolution de l’autre. Une étude rigoureuse et la plus exhaustive possible est indispensable pour déterminer un lien de causalité.\n\nPar exemple, il y aurait sans doute une corrélation entre le nombre de ventilateurs achetés et la quantité de glaces consommées, mais l’un n’est évidemment pas la cause de l’autre\u0026nbsp;: la véritable cause est plutôt la température extérieure, qui influe sur les deux variables\u0026nbsp;!  \nEn revanche, ce sont des études approfondies qui montrent que les anomalies météorologiques, comme les canicules, sont sans doute dues au réchauffement climatique, lui-même causé par l’activité humaine…\n[/ATT]\n\n[C]\nConclusion\u0026nbsp;:\n\nDans ce cours, nous avons donc ajouté une dimension importante aux statistiques sur lesquelles nous avons travaillé jusqu’ici. En effet, mettre en évidence la corrélation entre deux variables est un aspect fondamental dans l’étude de données, même s’il n’est pas suffisant pour conclure à un lien de cause à effet.  \nNous avons ici travaillé sur le lien entre deux variables, mais il peut y avoir bien sûr corrélation entre de multiples variables, compliquant la tâche. Il existe en statistique divers outils pour traiter de telles données, que certains découvriront durant leurs études supérieures.\n[/C]\n\n"}]</body></html>



[notebook](https://notebook.basthon.fr/?kernel=python3&ipynb=eJylkMFqAzEMRH_FKJeW7inNyZB_6L0Ni9bWpiZe2bVl2mXZf68dAmlyCvQiGI0989AChrzPoN8XmEjQoiDoZe3O-17mSKBhwnSy4ZuhgxxKMm232ajCpChndLB2N99BUslCFvSIPtNdmgmW_ibh_rVK-iFTxAXuTSgsoLl430EoEos0wMNdyQOMhtRXccoGJ3WwUWMdrYMp_Y952OPL9oNjcixPw_Nj_IfupvBEicnnSKYpxqkFx1k-A7eDWJejx7m_GG9nQzXHIx8LHq_PYa24PIwhTVibd1fRT45DAr1dfwFL36px)

<div class="centre">
<iframe 
src=
"https://notebook.basthon.fr/?kernel=python3&ipynb=eJylkMFqAzEMRH_FKJeW7inNyZB_6L0Ni9bWpiZe2bVl2mXZf68dAmlyCvQiGI0989AChrzPoN8XmEjQoiDoZe3O-17mSKBhwnSy4ZuhgxxKMm232ajCpChndLB2N99BUslCFvSIPtNdmgmW_ibh_rVK-iFTxAXuTSgsoLl430EoEos0wMNdyQOMhtRXccoGJ3WwUWMdrYMp_Y952OPL9oNjcixPw_Nj_IfupvBEicnnSKYpxqkFx1k-A7eDWJejx7m_GG9nQzXHIx8LHq_PYa24PIwhTVibd1fRT45DAr1dfwFL36px"
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxLK8rPVSgpLSrJSVXIzC3ILypR0OLlyshMSYUIamjyciVnFiUDWYYGBkBOQWpeaQFIND2_JF_D1EAHLpySX56HohxDtS5pyg10THGpNdAxJkG9gY4uWDmSdohASn4e0IcAaadINQ
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>




[notebook](https://notebook.basthon.fr/?kernel=python3&ipynb=eJylkMFqAzEMRH_FKJeW7inNyZB_6L0Ni9bWpiZe2bVl2mXZf68dAmlyCvQiGI0989AChrzPoN8XmEjQoiDoZe3O-17mSKBhwnSy4ZuhgxxKMm232ajCpChndLB2N99BUslCFvSIPtNdmgmW_ibh_rVK-iFTxAXuTSgsoLl430EoEos0wMNdyQOMhtRXccoGJ3WwUWMdrYMp_Y952OPL9oNjcixPw_Nj_IfupvBEicnnSKYpxqkFx1k-A7eDWJejx7m_GG9nQzXHIx8LHq_PYa24PIwhTVibd1fRT45DAr1dfwFL36px)