# Séries statistiques à deux variables.
--------------------------------------

!!! info "Pour la petite histoire"

Si les humains  ont depuis l'Antiquité compté et  recensé, ce n'est qu'au  XIXe siècle  que la  statistique  a pris  son envol  en conquérant  les sciences  sociales  et  naturelles. 

En  Belgique  d'abord  sous  l'impulsion d'[Adolphe <span style="font-variant:small-caps"> Quetelet </span>](https://mathshistory.st-andrews.ac.uk/Biographies/Quetelet/) (1796-1874) et de  son homme moyen  présenté dans _Sur l'homme  et le  développement  de ses  facultés, essai  d'une physique  sociale - 1835_.  Il travailla  beaucoup  sur une  approche statistique du  crime via l'anthropométrie. 

![Adolphe <span style="font-variant:small-caps"> Quetelet </span>](https://mathshistory.st-andrews.ac.uk/Biographies/Quetelet/Quetelet_2.jpeg){: width="15%" ; align="center"}

Quelques années plus  tard, c'est [Francis <span style="font-variant:small-caps">Galton</span>](https://mathshistory.st-andrews.ac.uk/Biographies/Galton/  ) (1822-1911) qui donna son envol à la statistique. Cousin  de  Charles  <span style="font-variant:small-caps"> Darwin </span>,  il  est  un  intellectuel fortuné  typique  de  l'ère  victorienne.  Il fut  à  la  fois  anthropologue, explorateur, géographe,  inventeur, météorologue,  écrivain, proto-généticien, psychométricien  et   statisticien.

![Francis <span style="font-variant:small-caps">Galton</span>](https://mathshistory.st-andrews.ac.uk/Biographies/Galton/Galton_6.jpeg){: width="15%" ; align="center"}.

Il fut, bien malgré lui, l'inspirateur   des  politiques eugénistes tragiquement mises en action  par les Nazis.

<span style="font-variant:small-caps">Galton</span> aurait dû lire <span style="font-variant:small-caps">Rabelais</span>: _"La sagesse  ne peut pas entrer dans un esprit méchant, et science sans conscience n'est que ruine de l'âme."_


## Série statistique à deux variables.

Le "Big Data", ça vous parle ?  Le nombres de données (_datas in english_) s'est considérablement accru avec l'avénement de l'informatique, dans des domaines extrêmement variés. On mesure tout! Ces énormes volumes de données peuvent être utilisés pour résoudre des problèmes que vous n’auriez jamais pu résoudre auparavant. Les données possèdent une valeur intrinsèque. Mais cela ne sert à rien tant que cette valeur n’est pas découverte. Tout aussi important : dans quelle mesure vos données sont-elles véridiques - et dans quelle mesure pouvez-vous vous y fier ?

  * Données issues du site : [National Centers for Environmental Information](https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/)

![nuage_climat](nuage_climat.png)

Que va t'on pouvoir faire de ces données ?

Mais commençons par un exemple plus simple.

  * Exemple simple
  * Calcul de moyennes
  Tout d’abord, nous pouvons calculer la moyenne des deux variables, ce qui nous permet de définir le <b>point moyen</b> qui peut être vu comme un point d'équilibre pour toutes ces données.

    * $\bar{x}$ est la moyenne arithmétique des valeurs $x_i$ associées à la variable $x$ ;
    * $\bar{y}$ est la moyenne arithmétique des valeurs $y_i$ associées à la variable $y$.

  * Utilisation de la calculatrice
  Avec votre calculatrice (_numworks_) entrez les données et déterminez les coordonnées du point moyen.

## Nuage de points.

  * Définition
  
    On représente une série statistique à deux variables $x$ et $y$ par un nuage de points dans un repère orthogonal $(O\ ;\, I,\,J)$, constitué de points $M_i$ de coordonnées $(x_i\ ;\, y_i)$, $x_i$ et $y_i$ étant respectivement les valeurs des variables $x$ et $y$ pour l’individu $i$ ($i$ allant de $1$ à $n$, avec $n$ la taille de la population).
  
  * Point moyen



## Ajustement affine.

   * méthode empirique
   * utiliser le point moyen
   * mesurer l'erreur. Utilisation d'un tableaur
   * Méthode des moindres carrés avec la calculatrice



